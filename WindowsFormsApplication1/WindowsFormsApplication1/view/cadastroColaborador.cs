﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SQLite;
using Infragistics.Excel;

namespace WindowsFormsApplication1
{
    public partial class cadastroColaborador : Form
    {
        string usuarioGlobal;
        
        List<int> flags = new List<int>();
        #region methods
        public cadastroColaborador(string usuario)
        {
            InitializeComponent();
            usuarioGlobal = usuario;
            rbCPF.Checked = true;
            atualizar();
        }

        public void atualizar()
        {
            limparCampos();
            buscarEmpresas();
            carrega_grid();
        }

        public void limparCampos()
        { 
            txtNome.Text = txtPlaca.Text = txtVeiculo.Text = txtCpf.Text = txtMatricula.Text = "";
            this.ultraToolbarsManager1.Tools["Salvar"].SharedProps.Caption = "Salvar";
            lbTitle.Text = "Cadastro Colaborador";
            lbId.Text = "";
        }

        public void buscarEmpresas()
        {
            EmpresaDAO empresaDAO = new EmpresaDAO();
            empresaDAO.buscar();
            cbEmpresa.Items.Add("Selecione");
            while (empresaDAO.Reader.Read())
            {
                cbEmpresa.Items.Add(empresaDAO.Reader["nome"].ToString());
            }
        }

        public void exportar()
        {

            string caminho = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog1.Filter = "Excel Workbook (*.xls)|*.xls|All Files (*.*)|*.*";
            DialogResult dg = saveFileDialog1.ShowDialog(this);
            if (dg == System.Windows.Forms.DialogResult.Cancel) return;
            string filename = saveFileDialog1.FileName;
            Workbook WB = new Workbook();
            WB.Worksheets.Add("Colaborador");
            excelColaborador.Export(gridcadastro, WB.Worksheets["Colaborador"], 0, 0);
            BIFF8Writer.WriteWorkbookToFile(WB, filename);
            System.Diagnostics.Process.Start(filename);
        }

        public void buscarColaborador(String field)
        {
            cbColaborador.DataSource = null;
            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            ColaboradorController control = new ColaboradorController();
            control.select();
            dictionary.Add("Selecione", 0);
            cbColaborador.DisplayMember = "Key";
            cbColaborador.ValueMember = "Value";
            while (control.ColaboradorDAO.Reader.Read())
            {
                dictionary.Add(control.ColaboradorDAO.Reader[field].ToString(), Convert.ToInt16(control.ColaboradorDAO.Reader["id"]));
            }
            dictionary.OrderBy(x => x);
            cbColaborador.DataSource = dictionary.ToList();
            
        }

        public bool salvar()
        {
            ColaboradorController colControl = new ColaboradorController();
            ClassColaborador colaborador = new ClassColaborador();
            if (this.ultraToolbarsManager1.Tools["Salvar"].SharedProps.Caption == "Salvar Alteração")
            {
                colaborador.Id = Convert.ToInt16(lbId.Text);
            }
            colaborador.Nome = txtNome.Text;
            colaborador.Matricula = txtMatricula.Text;
            colaborador.Veiculo = txtVeiculo.Text;
            colaborador.Cpf = txtCpf.Text;
            colaborador.Empresa = cbEmpresa.Text;
            colaborador.Placa = txtPlaca.Text;
            return colControl.insertUpdate(colaborador, this.ultraToolbarsManager1.Tools["Salvar"].SharedProps.Caption); 

        }

        private void carrega_grid()
        {
            ColaboradorController control = new ColaboradorController();
            dscadastro.Rows.Clear();
            control.selectGrid();
            foreach (DataRow grd in control.Grid.Rows)
            {
                object[] novoDados = new object[]{
                    control.Flag,
                    grd[0].ToString(),
                    grd[2].ToString(),//
                    grd[1].ToString(),//
                    grd[3].ToString(),//
                    grd[4].ToString(),//
                    grd[6].ToString(),//
                    grd[5].ToString()//
                }; dscadastro.Rows.Add(novoDados);
            }
        }
        #endregion
        #region events
        private void ultraToolbarsManager1_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            Message message = new Message();
            Cursor.Current = Cursors.WaitCursor;
            switch (e.Tool.Key)
            {
                case "Salvar":
                    {
                        if (txtNome.Text != "" || txtCpf.Text != "" || txtMatricula.Text != "" || cbEmpresa.Text != "Selecione")
                        {
                            if (salvar())
                            {
                                if (this.ultraToolbarsManager1.Tools["Salvar"].SharedProps.Caption == "Salvar")
                                {
                                    message.messageSuccess(this, "Cadastro inserido com sucesso", "Sucesso");
                                }
                                else
                                {
                                    message.messageSuccess(this, "Cadastro alterado com sucesso", "Sucesso");
                                }
                                atualizar(); 
                            }
                            else 
                            {
                                message.messageInformation(this, "Erro ao salvar. Tente novamente!", "Erro");
                                message.messageSuccess(this, "Cadastro inserido com sucesso", "Sucesso");
                            }
                        }
                        else 
                        {
                            message.messageInformation(this, "Favor informar todos os campos obrigatórios.", "Informação");
                        }
                    }
                    break;

                case "Limpar Campos":
                    {
                        limparCampos();
                    }
                    break;

                case "Atualizar":
                    {
                        atualizar();
                    }
                    break;

                case "Deletar":
                    {
                        if (flags.Count() > 0)
                        {
                            if(message.messageConfirmation(this, "Tem certeza que deseja excluir as lihas selecionadas?", "Confirmação" ))
                            {
                                ColaboradorController control = new ColaboradorController();
                                control.deleteForList(flags);
                                if(control.Result)
                                {
                                    message.messageSuccess(this, "Exclusão realizada com sucesso!", "Sucesso");
                                    atualizar();
                                }
                                else
                                {
                                    message.messageInformation(this, "Erro ao exlcuir. Tente novamente.", "Erro");
                                }
                            }
                        }
                        else
                        {
                            message.messageInformation(this, "Não há linhas selecionadas.", "");
                        }
                    }
                    break;

                case "Exportar":
                    {
                        exportar();
                    }
                    break;
            }
        }

        private void gridcadastro_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            if ((bool)e.Cell.Row.Cells["X"].Value)
            {
                
                flags.Remove(Convert.ToInt16(e.Cell.Row.Cells["id"].Value));
            }
            else 
            {
                flags.Add(Convert.ToInt16(e.Cell.Row.Cells["id"].Value));   
            }
        }

        private void gridcadastro_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            lbId.Text = e.Cell.Row.Cells["ID"].Text;
            txtMatricula.Text = e.Cell.Row.Cells["Matrícula"].Text;
            txtCpf.Text = e.Cell.Row.Cells["CPF"].Text;
            txtNome.Text = e.Cell.Row.Cells["Nome"].Text;
            txtPlaca.Text = e.Cell.Row.Cells["Placa"].Text;
            txtVeiculo.Text = e.Cell.Row.Cells["Veículo"].Text;
            cbEmpresa.Text = e.Cell.Row.Cells["Empresa"].Text;

            this.ultraToolbarsManager1.Tools["Salvar"].SharedProps.Caption = "Salvar Alteração";
            lbTitle.Text = "Alterar Colaborador";
        }
        #endregion 

        private void rbCPF_CheckedChanged(object sender, EventArgs e)
        {
            if (rbNome.Checked == true)
            {
                buscarColaborador("nome");
            }
            else
            {
                buscarColaborador("cpf");
            }
        }

        private void cbColaborador_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbColaborador.SelectedText != "Selecione")
            {
                ColaboradorController control = new ColaboradorController();
                control.selectId(Convert.ToInt16(cbColaborador.SelectedValue));
                while(control.ColaboradorDAO.Reader.Read())
                {
                    txtNome.Text = control.ColaboradorDAO.Reader["nome"].ToString();
                    txtMatricula.Text = control.ColaboradorDAO.Reader["matricula"].ToString();
                    txtCpf.Text = control.ColaboradorDAO.Reader["cpf"].ToString();
                    cbEmpresa.Text = control.ColaboradorDAO.Reader["empresa"].ToString();
                    txtPlaca.Text = control.ColaboradorDAO.Reader["placa"].ToString();
                    txtVeiculo.Text = control.ColaboradorDAO.Reader["veiculo"].ToString();
                }
            }
        }

        private void txtCpf_KeyUp(object sender, KeyEventArgs e)
        {
           
            
                if (e.KeyCode != Keys.Back && e.KeyCode != Keys.Space)
                {
                    if (txtCpf.Text.Length == 3) { txtCpf.Text += "."; txtCpf.SelectionStart = txtCpf.Text.Length; }
                    else if (txtCpf.Text.Length == 7) { txtCpf.Text += "."; txtCpf.SelectionStart = txtCpf.Text.Length; }
                    else if (txtCpf.Text.Length == 11) { txtCpf.Text += "-"; txtCpf.SelectionStart = txtCpf.Text.Length; }
                }
            
        }
    }
}
