﻿namespace WindowsFormsApplication1
{
    partial class cadastroColaborador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("X");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("id");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Matrícula");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Nome");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CPF");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Empresa");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Placa");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Veículo");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn9 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("X");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn10 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("id");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn11 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Matrícula");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn12 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Nome");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn13 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CPF");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn14 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Empresa");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn15 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Placa");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn16 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Veículo");
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab2 = new Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon1");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup3 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool1 = new Infragistics.Win.UltraWinToolbars.ButtonTool("cadastroColaborador");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool3 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Histórico");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup4 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup2");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool5 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Salvar");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool6 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Deletar");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool9 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Limpar Campos");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool13 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exportar");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool11 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Atualizar");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool1 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("PopupControlContainerTool1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool2 = new Infragistics.Win.UltraWinToolbars.ButtonTool("cadastroColaborador");
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool4 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Histórico");
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool7 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Salvar");
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool8 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Deletar");
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool10 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Limpar Campos");
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool12 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Atualizar");
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool14 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ButtonTool1");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool2 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("PopupControlContainerTool1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool15 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exportar");
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridcadastro = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dscadastro = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbId = new System.Windows.Forms.Label();
            this.lbTitle = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPlaca = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtVeiculo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbEmpresa = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCpf = new System.Windows.Forms.TextBox();
            this.txtMatricula = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.pnBuscaColaborador = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbNome = new System.Windows.Forms.RadioButton();
            this.rbCPF = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.cbColaborador = new System.Windows.Forms.ComboBox();
            this.Colaborador_Fill_Panel = new Infragistics.Win.Misc.UltraPanel();
            this.Colaboradores = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this._Colaborador_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.ultraToolbarsManager1 = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
            this._Colaborador_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._Colaborador_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._Colaborador_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.excelColaborador = new Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridcadastro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dscadastro)).BeginInit();
            this.panel1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            this.pnBuscaColaborador.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.Colaborador_Fill_Panel.ClientArea.SuspendLayout();
            this.Colaborador_Fill_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Colaboradores)).BeginInit();
            this.Colaboradores.SuspendLayout();
            this.ultraTabSharedControlsPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.gridcadastro);
            this.ultraTabPageControl1.Controls.Add(this.panel1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1308, 499);
            // 
            // gridcadastro
            // 
            this.gridcadastro.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridcadastro.DataMember = null;
            this.gridcadastro.DataSource = this.dscadastro;
            ultraGridColumn1.Header.VisiblePosition = 1;
            ultraGridColumn2.Header.VisiblePosition = 0;
            ultraGridColumn2.Hidden = true;
            ultraGridColumn3.Header.VisiblePosition = 2;
            ultraGridColumn3.Width = 149;
            ultraGridColumn4.Header.VisiblePosition = 4;
            ultraGridColumn4.Width = 394;
            ultraGridColumn5.Header.VisiblePosition = 3;
            ultraGridColumn5.Width = 176;
            ultraGridColumn6.Header.VisiblePosition = 5;
            ultraGridColumn6.Width = 214;
            ultraGridColumn7.Header.VisiblePosition = 7;
            ultraGridColumn7.Width = 195;
            ultraGridColumn8.Header.VisiblePosition = 6;
            ultraGridColumn8.Width = 184;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5,
            ultraGridColumn6,
            ultraGridColumn7,
            ultraGridColumn8});
            this.gridcadastro.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.gridcadastro.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.Yes;
            this.gridcadastro.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.WithinGroup;
            this.gridcadastro.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridcadastro.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.gridcadastro.DisplayLayout.Override.RowFilterAction = Infragistics.Win.UltraWinGrid.RowFilterAction.AppearancesOnly;
            this.gridcadastro.DisplayLayout.Override.RowFilterMode = Infragistics.Win.UltraWinGrid.RowFilterMode.AllRowsInBand;
            this.gridcadastro.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.gridcadastro.ExitEditModeOnLeave = false;
            this.gridcadastro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridcadastro.Location = new System.Drawing.Point(1, 127);
            this.gridcadastro.Name = "gridcadastro";
            this.gridcadastro.Size = new System.Drawing.Size(1303, 369);
            this.gridcadastro.TabIndex = 6;
            this.gridcadastro.Text = "Cadastro";
            this.gridcadastro.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.gridcadastro_CellChange);
            this.gridcadastro.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.gridcadastro_ClickCell);
            // 
            // dscadastro
            // 
            ultraDataColumn9.DataType = typeof(bool);
            ultraDataColumn10.DataType = typeof(short);
            ultraDataColumn10.ReadOnly = Infragistics.Win.DefaultableBoolean.False;
            ultraDataColumn11.ReadOnly = Infragistics.Win.DefaultableBoolean.False;
            ultraDataColumn12.ReadOnly = Infragistics.Win.DefaultableBoolean.False;
            ultraDataColumn13.ReadOnly = Infragistics.Win.DefaultableBoolean.False;
            ultraDataColumn14.ReadOnly = Infragistics.Win.DefaultableBoolean.False;
            ultraDataColumn15.ReadOnly = Infragistics.Win.DefaultableBoolean.False;
            ultraDataColumn16.ReadOnly = Infragistics.Win.DefaultableBoolean.False;
            this.dscadastro.Band.Columns.AddRange(new object[] {
            ultraDataColumn9,
            ultraDataColumn10,
            ultraDataColumn11,
            ultraDataColumn12,
            ultraDataColumn13,
            ultraDataColumn14,
            ultraDataColumn15,
            ultraDataColumn16});
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbId);
            this.panel1.Controls.Add(this.lbTitle);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtPlaca);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtVeiculo);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cbEmpresa);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtCpf);
            this.panel1.Controls.Add(this.txtMatricula);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtNome);
            this.panel1.Location = new System.Drawing.Point(2, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1302, 119);
            this.panel1.TabIndex = 1;
            // 
            // lbId
            // 
            this.lbId.AutoSize = true;
            this.lbId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbId.Location = new System.Drawing.Point(186, 6);
            this.lbId.Name = "lbId";
            this.lbId.Size = new System.Drawing.Size(0, 13);
            this.lbId.TabIndex = 17;
            this.lbId.Visible = false;
            // 
            // lbTitle
            // 
            this.lbTitle.AutoSize = true;
            this.lbTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitle.Location = new System.Drawing.Point(5, 4);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(147, 15);
            this.lbTitle.TabIndex = 16;
            this.lbTitle.Text = "Cadastro Colaborador";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(386, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Placa";
            // 
            // txtPlaca
            // 
            this.txtPlaca.Location = new System.Drawing.Point(426, 80);
            this.txtPlaca.Name = "txtPlaca";
            this.txtPlaca.Size = new System.Drawing.Size(166, 20);
            this.txtPlaca.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(376, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Veículo";
            // 
            // txtVeiculo
            // 
            this.txtVeiculo.Location = new System.Drawing.Point(426, 55);
            this.txtVeiculo.Name = "txtVeiculo";
            this.txtVeiculo.Size = new System.Drawing.Size(166, 20);
            this.txtVeiculo.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(372, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Empresa";
            // 
            // cbEmpresa
            // 
            this.cbEmpresa.FormattingEnabled = true;
            this.cbEmpresa.Location = new System.Drawing.Point(426, 28);
            this.cbEmpresa.Name = "cbEmpresa";
            this.cbEmpresa.Size = new System.Drawing.Size(166, 21);
            this.cbEmpresa.TabIndex = 6;
            this.cbEmpresa.Text = "Selecione";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "CPF";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Matrícula";
            // 
            // txtCpf
            // 
            this.txtCpf.Location = new System.Drawing.Point(92, 80);
            this.txtCpf.MaxLength = 14;
            this.txtCpf.Name = "txtCpf";
            this.txtCpf.Size = new System.Drawing.Size(202, 20);
            this.txtCpf.TabIndex = 3;
            this.txtCpf.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCpf_KeyUp);
            // 
            // txtMatricula
            // 
            this.txtMatricula.Location = new System.Drawing.Point(92, 54);
            this.txtMatricula.Name = "txtMatricula";
            this.txtMatricula.Size = new System.Drawing.Size(202, 20);
            this.txtMatricula.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(92, 28);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(202, 20);
            this.txtNome.TabIndex = 0;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.pnBuscaColaborador);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1308, 499);
            // 
            // pnBuscaColaborador
            // 
            this.pnBuscaColaborador.BackColor = System.Drawing.Color.Transparent;
            this.pnBuscaColaborador.Controls.Add(this.groupBox1);
            this.pnBuscaColaborador.Controls.Add(this.label9);
            this.pnBuscaColaborador.Controls.Add(this.cbColaborador);
            this.pnBuscaColaborador.Location = new System.Drawing.Point(25, 140);
            this.pnBuscaColaborador.Name = "pnBuscaColaborador";
            this.pnBuscaColaborador.Size = new System.Drawing.Size(322, 73);
            this.pnBuscaColaborador.TabIndex = 7;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbNome);
            this.groupBox1.Controls.Add(this.rbCPF);
            this.groupBox1.Location = new System.Drawing.Point(94, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(210, 30);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // rbNome
            // 
            this.rbNome.AutoSize = true;
            this.rbNome.Location = new System.Drawing.Point(132, 9);
            this.rbNome.Name = "rbNome";
            this.rbNome.Size = new System.Drawing.Size(53, 17);
            this.rbNome.TabIndex = 1;
            this.rbNome.TabStop = true;
            this.rbNome.Text = "Nome";
            this.rbNome.UseVisualStyleBackColor = true;
            this.rbNome.CheckedChanged += new System.EventHandler(this.rbCPF_CheckedChanged);
            // 
            // rbCPF
            // 
            this.rbCPF.AutoSize = true;
            this.rbCPF.Location = new System.Drawing.Point(29, 9);
            this.rbCPF.Name = "rbCPF";
            this.rbCPF.Size = new System.Drawing.Size(45, 17);
            this.rbCPF.TabIndex = 0;
            this.rbCPF.TabStop = true;
            this.rbCPF.Text = "CPF";
            this.rbCPF.UseVisualStyleBackColor = true;
            this.rbCPF.CheckedChanged += new System.EventHandler(this.rbCPF_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(7, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 16);
            this.label9.TabIndex = 1;
            this.label9.Text = "Colaborador";
            // 
            // cbColaborador
            // 
            this.cbColaborador.FormattingEnabled = true;
            this.cbColaborador.Location = new System.Drawing.Point(93, 38);
            this.cbColaborador.Name = "cbColaborador";
            this.cbColaborador.Size = new System.Drawing.Size(211, 21);
            this.cbColaborador.TabIndex = 0;
            this.cbColaborador.Text = "Selecione";
            this.cbColaborador.SelectedIndexChanged += new System.EventHandler(this.cbColaborador_SelectedIndexChanged);
            // 
            // Colaborador_Fill_Panel
            // 
            // 
            // Colaborador_Fill_Panel.ClientArea
            // 
            this.Colaborador_Fill_Panel.ClientArea.Controls.Add(this.Colaboradores);
            this.Colaborador_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default;
            this.Colaborador_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Colaborador_Fill_Panel.Location = new System.Drawing.Point(8, 158);
            this.Colaborador_Fill_Panel.Name = "Colaborador_Fill_Panel";
            this.Colaborador_Fill_Panel.Size = new System.Drawing.Size(1322, 528);
            this.Colaborador_Fill_Panel.TabIndex = 0;
            // 
            // Colaboradores
            // 
            this.Colaboradores.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Colaboradores.Controls.Add(this.ultraTabSharedControlsPage1);
            this.Colaboradores.Controls.Add(this.ultraTabPageControl1);
            this.Colaboradores.Controls.Add(this.ultraTabPageControl2);
            this.Colaboradores.Location = new System.Drawing.Point(6, 3);
            this.Colaboradores.Name = "Colaboradores";
            this.Colaboradores.SharedControls.AddRange(new System.Windows.Forms.Control[] {
            this.panel1});
            this.Colaboradores.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.Colaboradores.Size = new System.Drawing.Size(1312, 525);
            this.Colaboradores.TabIndex = 1;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Colaboradores";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "tab1";
            ultraTab2.Visible = false;
            this.Colaboradores.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ultraTabSharedControlsPage1.Controls.Add(this.panel1);
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1308, 499);
            // 
            // _Colaborador_Toolbars_Dock_Area_Left
            // 
            this._Colaborador_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Colaborador_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Colaborador_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
            this._Colaborador_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Colaborador_Toolbars_Dock_Area_Left.InitialResizeAreaExtent = 8;
            this._Colaborador_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 158);
            this._Colaborador_Toolbars_Dock_Area_Left.Name = "_Colaborador_Toolbars_Dock_Area_Left";
            this._Colaborador_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(8, 528);
            this._Colaborador_Toolbars_Dock_Area_Left.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // ultraToolbarsManager1
            // 
            this.ultraToolbarsManager1.DesignerFlags = 1;
            this.ultraToolbarsManager1.DockWithinContainer = this;
            this.ultraToolbarsManager1.DockWithinContainerBaseType = typeof(System.Windows.Forms.Form);
            this.ultraToolbarsManager1.Ribbon.Caption = "";
            ribbonTab2.Caption = "Módulos";
            ribbonGroup3.Caption = "Módulos";
            buttonTool1.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool3.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Normal;
            buttonTool3.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup3.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool1,
            buttonTool3});
            ribbonGroup4.Caption = "Ferramentas";
            buttonTool5.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool6.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool9.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool13.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool11.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            controlContainerTool1.ControlName = "pnBuscaColaborador";
            controlContainerTool1.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            controlContainerTool1.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            controlContainerTool1.InstanceProps.Width = 322;
            ribbonGroup4.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool5,
            buttonTool6,
            buttonTool9,
            buttonTool13,
            buttonTool11,
            controlContainerTool1});
            ribbonTab2.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup3,
            ribbonGroup4});
            this.ultraToolbarsManager1.Ribbon.NonInheritedRibbonTabs.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonTab[] {
            ribbonTab2});
            this.ultraToolbarsManager1.Ribbon.Visible = true;
            appearance11.Image = global::WindowsFormsApplication1.Properties.Resources.folder_32;
            buttonTool2.SharedPropsInternal.AppearancesLarge.Appearance = appearance11;
            buttonTool2.SharedPropsInternal.Caption = "Cadastro";
            appearance12.Image = global::WindowsFormsApplication1.Properties.Resources.Align_Compact;
            buttonTool4.SharedPropsInternal.AppearancesLarge.Appearance = appearance12;
            buttonTool4.SharedPropsInternal.Caption = "Histórico";
            appearance13.Image = global::WindowsFormsApplication1.Properties.Resources.save_32;
            buttonTool7.SharedPropsInternal.AppearancesLarge.Appearance = appearance13;
            buttonTool7.SharedPropsInternal.Caption = "Salvar";
            appearance14.Image = global::WindowsFormsApplication1.Properties.Resources.delete_32;
            buttonTool8.SharedPropsInternal.AppearancesLarge.Appearance = appearance14;
            buttonTool8.SharedPropsInternal.Caption = "Deletar";
            appearance15.Image = global::WindowsFormsApplication1.Properties.Resources.Draw_Eraser;
            buttonTool10.SharedPropsInternal.AppearancesSmall.Appearance = appearance15;
            buttonTool10.SharedPropsInternal.Caption = "Limpar Campos";
            appearance16.Image = global::WindowsFormsApplication1.Properties.Resources.Atualizar_Large;
            buttonTool12.SharedPropsInternal.AppearancesLarge.Appearance = appearance16;
            buttonTool12.SharedPropsInternal.Caption = "Atualizar";
            buttonTool14.SharedPropsInternal.Caption = "ButtonTool1";
            controlContainerTool2.ControlName = "pnBuscaColaborador";
            controlContainerTool2.SharedPropsInternal.AllowMultipleInstances = true;
            controlContainerTool2.SharedPropsInternal.Width = 322;
            appearance17.Image = global::WindowsFormsApplication1.Properties.Resources.Table_Excel;
            buttonTool15.SharedPropsInternal.AppearancesSmall.Appearance = appearance17;
            buttonTool15.SharedPropsInternal.Caption = "Exportar";
            this.ultraToolbarsManager1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool2,
            buttonTool4,
            buttonTool7,
            buttonTool8,
            buttonTool10,
            buttonTool12,
            buttonTool14,
            controlContainerTool2,
            buttonTool15});
            this.ultraToolbarsManager1.ToolClick += new Infragistics.Win.UltraWinToolbars.ToolClickEventHandler(this.ultraToolbarsManager1_ToolClick);
            // 
            // _Colaborador_Toolbars_Dock_Area_Right
            // 
            this._Colaborador_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Colaborador_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Colaborador_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
            this._Colaborador_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Colaborador_Toolbars_Dock_Area_Right.InitialResizeAreaExtent = 8;
            this._Colaborador_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(1330, 158);
            this._Colaborador_Toolbars_Dock_Area_Right.Name = "_Colaborador_Toolbars_Dock_Area_Right";
            this._Colaborador_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(8, 528);
            this._Colaborador_Toolbars_Dock_Area_Right.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _Colaborador_Toolbars_Dock_Area_Top
            // 
            this._Colaborador_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Colaborador_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Colaborador_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
            this._Colaborador_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Colaborador_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._Colaborador_Toolbars_Dock_Area_Top.Name = "_Colaborador_Toolbars_Dock_Area_Top";
            this._Colaborador_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(1338, 158);
            this._Colaborador_Toolbars_Dock_Area_Top.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _Colaborador_Toolbars_Dock_Area_Bottom
            // 
            this._Colaborador_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Colaborador_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Colaborador_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
            this._Colaborador_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Colaborador_Toolbars_Dock_Area_Bottom.InitialResizeAreaExtent = 8;
            this._Colaborador_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 686);
            this._Colaborador_Toolbars_Dock_Area_Bottom.Name = "_Colaborador_Toolbars_Dock_Area_Bottom";
            this._Colaborador_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(1338, 8);
            this._Colaborador_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // cadastroColaborador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1338, 694);
            this.Controls.Add(this.Colaborador_Fill_Panel);
            this.Controls.Add(this._Colaborador_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._Colaborador_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._Colaborador_Toolbars_Dock_Area_Top);
            this.Controls.Add(this._Colaborador_Toolbars_Dock_Area_Bottom);
            this.Name = "cadastroColaborador";
            this.Text = "Colaboradores";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridcadastro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dscadastro)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.pnBuscaColaborador.ResumeLayout(false);
            this.pnBuscaColaborador.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Colaborador_Fill_Panel.ClientArea.ResumeLayout(false);
            this.Colaborador_Fill_Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Colaboradores)).EndInit();
            this.Colaboradores.ResumeLayout(false);
            this.ultraTabSharedControlsPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinToolbars.UltraToolbarsManager ultraToolbarsManager1;
        private Infragistics.Win.Misc.UltraPanel Colaborador_Fill_Panel;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Colaborador_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Colaborador_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Colaborador_Toolbars_Dock_Area_Top;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Colaborador_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl Colaboradores;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private System.Windows.Forms.TextBox txtMatricula;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtCpf;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbEmpresa;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtVeiculo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPlaca;
        private System.Windows.Forms.Label lbTitle;
        private Infragistics.Win.UltraWinGrid.UltraGrid gridcadastro;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource dscadastro;
        private System.Windows.Forms.Label lbId;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private System.Windows.Forms.Panel pnBuscaColaborador;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbNome;
        private System.Windows.Forms.RadioButton rbCPF;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbColaborador;
        private Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter excelColaborador;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}