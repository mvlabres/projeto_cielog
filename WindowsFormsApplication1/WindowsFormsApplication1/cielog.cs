﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Infragistics.Win.UltraWinGrid;
using System.Diagnostics;
using System.Data.SQLite;
using System.IO;
using System.Collections;



namespace WindowsFormsApplication1
{
    public partial class Fgeral : Form
    {
        /*
         variaveis contadoras para reduzir buscas no banco
         */
        int contComboCPF;
        int contComboAcesso;
        int contEmpresa;
        int contComboEmpresaVisitada;

        public Fgeral()
        {
            InitializeComponent();
            ultraToolbarsManager1.Tools["Imprimi"].SharedProps.Enabled = false;
            dtpDataEntrada.Format = DateTimePickerFormat.Custom;
            dtpDataEntrada.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            dtSaida.Format = DateTimePickerFormat.Custom;
            dtSaida.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            cbUsuario.Items.Clear();
            cbusuarioSaida.Items.Clear();
            carregarCombo(1, "select nome from usuario order by nome");
            cbUsuario.Text = "selecione";
            cbusuarioSaida.Text = "";
            cbOperacao.Text = "selecione";
            
            atualizar();
        }
        private Font bold = new Font(FontFamily.GenericSansSerif, 12, FontStyle.Bold);
        private Font regular = new Font(FontFamily.GenericSansSerif, 9, FontStyle.Regular);
        private Font media = new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold);
        private Font pequena = new Font(FontFamily.GenericSansSerif, 7, FontStyle.Regular);
        private Font regularItens = new Font(FontFamily.GenericSansSerif, 6, FontStyle.Regular);
 
        /*
         variaveis contadoras para reduzir buscas no banco
         */


        string tipo_cadastro = "";
        int timer_bkp = 0;
        bool realizar_update = false;
        String strConn = @"Data Source=C:\cielog\cielog.s3db";
        List<string> id = new List<string>();
        List<string> empresa = new List<string>();
        #region metodos

        private void carregarCombo(int combo, string sql)
        {
            if (combo != 3)
            {
                SQLiteConnection con = new SQLiteConnection(strConn);
                con.Open();
                SQLiteCommand comando = new SQLiteCommand(sql, con);
                var reader = comando.ExecuteReader();
                while (reader.Read())
                {
                    if (combo == 1) { 
                        cbUsuario.Items.Add(reader["nome"].ToString());
                        cbusuarioSaida.Items.Add(reader["nome"].ToString());
                    }
                    else if (combo == 2)
                    {
                        if (rbCpf.Checked == true) cbCPF.Items.Add(reader["cpf"].ToString());
                        else if (rbPlaca.Checked == true) cbCPF.Items.Add(reader["placa"].ToString());
                        else cbCPF.Items.Add(reader["nome"].ToString());
                        
                    }
                    else if (combo == 4) 
                    {
                        if (rbSenha.Checked == true) cbAcessoAberto.Items.Add(reader["senha"].ToString());
                        else cbAcessoAberto.Items.Add(reader["motorista"].ToString());
                    }
                    else if (combo == 5)
                    {   
                        cbAlteraEmpresa.Items.Add(reader["nome"].ToString());
                    }
                    else if (combo == 6)
                    {
                        cbEmpresaVisitada.Items.Add(reader["nome"].ToString());
                    }
                }
            }
            else
            {
                
                cbTurno.Items.Add("7:00 AS 19:00 HRS");
                cbTurno.Items.Add("19:00 AS 7:00 HRS");

                if (DateTime.Now.Hour >= 7 && DateTime.Now.Hour < 19) { cbTurno.SelectedIndex = 0; }
                else cbTurno.SelectedIndex = 1;
            }
          
        }
        private void carregar_autocomplete(string sql, int combo)
        {
             SQLiteConnection con = new SQLiteConnection(strConn);
             con.Open();
             SQLiteCommand comando = new SQLiteCommand(sql, con);
             var reader = comando.ExecuteReader();
             while (reader.Read())
             {
                 if (combo == 1) cbCPF.AutoCompleteCustomSource.Add(reader["cpf"].ToString());
                 else 
                 {
                     cbAcessoAberto.AutoCompleteCustomSource.Add(reader["motorista"].ToString());
                 }
             }
        }
        private void marcarTodos()
        {
            foreach (var row in gridAcesso.DisplayLayout.Rows)
            {
                if (!id.Contains(row.Cells["id"].Text)) id.Add(row.Cells["id"].Text);
                row.Cells["X"].Value = true;
            }
        }
        private void desmarcarTodos()
        {
            foreach (var row in gridAcesso.DisplayLayout.Rows)
            {
                if (id.Contains(row.Cells["id"].Text)) id.Remove(row.Cells["id"].Text);
                row.Cells["X"].Value = false;
            }
            cbCPF.Enabled = true;
        }

        private void enabled_fields()
        {
            dtpDataEntrada.Enabled = dtSaida.Enabled = false;
        }

        private void limpar_campos()
        {
            txtId.Text = txtNome.Text = txtNfEntrada.Text = txtCNH.Text = txtSenha.Text = txtcpf.Text = tipo_cadastro =
            txtNfSaida.Text = txtCor.Text = txtPlacaCarreta.Text = txtPlacaCavalo.Text = txtVeiculo.Text = lbStatus.Text =  "";
            cbTipoCaminhao.Text = cbOperacao.Text = cbEmpresaVisitada.Text = cbAlteraEmpresa.Text = cbCPF.Text = "selecione";
            dtpDataEntrada.Value = dtp_vencimentoCNH.Value = DateTime.Now;
            dtSaida.Visible = lbSaida.Visible = picAnularFinal.Visible = false;
            cbCPF.Enabled = dtp_vencimentoCNH.Enabled = cbOperacao.Enabled = cbTipoCaminhao.Enabled = true;
            
            ultraToolbarsManager1.Tools["Imprimi"].SharedProps.Enabled = false;
        }
        private string salvar_atualizacao()
        {
            string msg = "";
            try
            {
                SQLiteConnection con = new SQLiteConnection(strConn);
                con.Open();
                SQLiteCommand cmd = new SQLiteCommand(con);
                cmd.CommandText = "update historico set empresa=@empresa, veiculo = @veiculo,cor = @cor,placa=@placa, placa_carreta=@placa_carreta, nf_entrada=@nf_entrada, nf_saida=@nf_saida, senha=@senha, data_entrada=@data_entrada, data_saida=@data_saida, empresa_visitada = @empresa_visitada, usuario_saida = @usuario_saida, tipo_caminhao = @tipo_caminhao, operacao = @operacao, vencimento_cnh = @vencimento_cnh, numeroCNH = @numeroCNH where id_historico = @id";
                cmd.Parameters.AddWithValue("@empresa", cbAlteraEmpresa.Text);
                cmd.Parameters.AddWithValue("@veiculo", txtVeiculo.Text);
                cmd.Parameters.AddWithValue("@cor", txtCor.Text);
                cmd.Parameters.AddWithValue("@placa", txtPlacaCavalo.Text);
                cmd.Parameters.AddWithValue("@placa_carreta", txtPlacaCarreta.Text);
                cmd.Parameters.AddWithValue("@nf_entrada", txtNfEntrada.Text);
                cmd.Parameters.AddWithValue("@nf_saida", txtNfSaida.Text);
                cmd.Parameters.AddWithValue("@senha", txtSenha.Text);
                cmd.Parameters.AddWithValue("@data_entrada", Convert.ToDateTime(dtpDataEntrada.Value));
                cmd.Parameters.AddWithValue("@empresa_visitada", cbEmpresaVisitada.Text);
                cmd.Parameters.AddWithValue("@usuario_saida", cbusuarioSaida.Text);
                cmd.Parameters.AddWithValue("@numeroCNH", txtCNH.Text);
                if (cbTipoCaminhao.Text == "selecione")
                {
                    cmd.Parameters.AddWithValue("tipo_caminhao", "");
                }
                else
                {
                    cmd.Parameters.AddWithValue("tipo_caminhao", cbTipoCaminhao.Text);
                }
                if (cbOperacao.Text == "selecione")
                {
                    cmd.Parameters.AddWithValue("@operacao", "");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@operacao", cbOperacao.Text);
                }
                if (dtp_vencimentoCNH.Enabled == false)
                {
                    cmd.Parameters.AddWithValue("@vencimento_cnh", null);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@vencimento_cnh", Convert.ToDateTime(dtp_vencimentoCNH.Value.ToString()));
                    
                }
                
                if (dtSaida.Visible == true)
                {
                    cmd.Parameters.AddWithValue("@data_saida", Convert.ToDateTime(dtSaida.Value));
                    msg = "Acesso finalizado com sucesso!";
   
                }
                else
                {
                    cmd.Parameters.AddWithValue("@data_saida", null);
                }
                cmd.Parameters.AddWithValue("@id", txtId.Text.Replace("ID: ", ""));
                var reader = cmd.ExecuteReader();
               
                con.Close();
                atualizar();
                limpar_campos();
                cbCPF.Enabled = true;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            return msg;
        }

        private int verifica_cpf()
        {
            int id = 0;
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(strConn))
                {
                    conn.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "select id_cadastro from cadastro where cpf = @cpf";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@cpf",cbCPF.Text);
                        var reader = cmd.ExecuteReader();
                        //conn.Close();
                        while (reader.Read())
                        {
                            id = Convert.ToInt32(reader["id_cadastro"]);
                        }

                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            return id;
        }

        private void deletar()
        {
            int resultado = -1;
            if (id.Count > 0)
            {
                DialogResult dr = System.Windows.Forms.DialogResult.Yes;
                dr = MessageBox.Show(
                          this,
                          "Tem certeza que deseja excluir " + id.Count.ToString() + " registro(s) selecionado(s)? ",
                          "Confirmação",
                          MessageBoxButtons.YesNo,
                          MessageBoxIcon.Question);
                switch (dr)
                {
                    case DialogResult.Yes:
                        {
                            foreach (string c in id)
                            {
                                try
                                {
                                    using (SQLiteConnection conn = new SQLiteConnection(strConn))
                                    {
                                        conn.Open();
                                        using (SQLiteCommand cmd = new SQLiteCommand(conn))
                                        {
                                            cmd.CommandText = "delete from historico where id_historico = @id_historico";
                                            cmd.Prepare();
                                            cmd.Parameters.AddWithValue("@id_historico", c);
                                            resultado = cmd.ExecuteNonQuery();
                                            conn.Close();
                                        }
                                    }
                                }
                                catch (Exception ex) { MessageBox.Show(ex.Message); }
                            }
                            carregar_grid();
                            id.Clear();
                            mensagem("Registros excluídos com sucesso!");
                        }
                        break;
                    case DialogResult.No: break;
                }
            }
            else mensagem("Não há registros selecionados");
            
        }
        private void atualizar()
        {
            contComboCPF = contComboAcesso = contEmpresa = contComboEmpresaVisitada = 0;
            rbCpf.Checked = true;
            rbSenha.Checked = true;
            cbCPF.Items.Clear();
            cbTipoCaminhao.Items.Clear();
            cbTurno.Items.Clear();
            cbOperacao.Items.Clear();
            cbEmpresaVisitada.Items.Clear();
            cbAcessoAberto.Items.Clear();
            cbAlteraEmpresa.Items.Clear();
            Cursor.Current = Cursors.WaitCursor;
            carregar_grid();
            /*if (rbCpf.Checked == true) carregarCombo(2, "select cpf from cadastro order by cpf");
            else carregarCombo(2, "select nome from cadastro order by nome");*/
           
            carregarCombo(3, "");
            /*if (rbSenha.Checked == true) carregarCombo(4, "select senha from historico where data_saida is null and senha <> '' and senha is not null");
            else
            {
                carregarCombo(4, "select motorista from historico where data_saida is null");
                carregar_autocomplete("select motorista from historico where data_saida is null ", 2);
            }*/
            cbCPF.Text = "selecione";
            /*carregar_autocomplete("select cpf from cadastro order by cpf", 1);*/
            
            /*carregarCombo(5,"select nome from empresa order by nome");*/

            /*carregarCombo(6, "select nome from empresa_visitada order by nome");*/
            Cursor.Current = Cursors.Default;

            cbTipoCaminhao.Items.Add("selecione");
            cbTipoCaminhao.Items.Add("Baú");
            cbTipoCaminhao.Items.Add("Bitrem");
            cbTipoCaminhao.Items.Add("Carreta");
            cbTipoCaminhao.Items.Add("Container");
            cbTipoCaminhao.Items.Add("Rodotrem");
            cbTipoCaminhao.Items.Add("Sider");
            cbTipoCaminhao.Items.Add("Truck");
            cbTipoCaminhao.Items.Add("Outros");

            cbOperacao.Items.Add("Selecione");
            cbOperacao.Items.Add("Carga");
            cbOperacao.Items.Add("Descarga");

            cbTipoCaminhao.SelectedIndex = cbOperacao.SelectedIndex = 0;
        }



        private void imprimir()
        {
            if (cbOperacao.Text != "Selecione")
            {
                if (cbTipoCaminhao.Text != "selecione")
                {
                    if (cbUsuario.Text != "selecione")
                    {
                        if (verificarCampos("motorista"))
                        {
                            if (printDialog1.ShowDialog() == DialogResult.OK)
                            {
                                this.printDocument1.Print();
                                enabled_fields();
                            }
                        }     
                    }
                    else 
                    {
                        mensagem("Favor informar o usuario!");
                        cbUsuario.Focus();
                    }
                }
                else
                {
                    mensagem("Favor informar o tipo de caminhão!");
                    cbTipoCaminhao.Focus();
                }
            }
            else
            {
                mensagem("Favor informar a operação!");
                cbOperacao.Focus();
            }
           
        }
        private void mensagemErro(string msg)
        {
            DialogResult dr = System.Windows.Forms.DialogResult.Yes;
            dr = MessageBox.Show(
                      this,
                      msg,
                      "Erro",
                      MessageBoxButtons.OK,
                      MessageBoxIcon.Stop);
        }
        private void mensagem(string msg)
        {
            DialogResult dr = System.Windows.Forms.DialogResult.Yes;
            dr = MessageBox.Show(
                      this,
                      msg,
                      "Informação",
                      MessageBoxButtons.OK,
                      MessageBoxIcon.Information);  
        }
        private void exportar()
        {
            string caminho = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog1.Filter = "Excel Workbook (*.xls)|*.xls|All Files (*.*)|*.*";
            DialogResult dg = saveFileDialog1.ShowDialog(this);
            if (dg == System.Windows.Forms.DialogResult.Cancel) return;
            string filename = saveFileDialog1.FileName;
            Workbook WB = new Workbook();
            WB.Worksheets.Add("Acessos");
            excelCielo.Export(gridAcesso, WB.Worksheets["Acessos"], 0, 0);
            BIFF8Writer.WriteWorkbookToFile(WB, filename);
            System.Diagnostics.Process.Start(filename);
        }

        private void update()
        {
            int resultado = -1;
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(strConn))
                {
                    conn.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "update cadastro set placa=@placa,placa_carreta=@placa_carreta,veiculo=@veiculo,cor=@cor,data_criacao=@data_criacao, vencimento_cnh = @vencimento_cnh, numeroCNH = @numeroCNH where id_cadastro = @id";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@id", txtId.Text.Replace("ID: ", ""));
                        cmd.Parameters.AddWithValue("@placa", txtPlacaCavalo.Text);
                        cmd.Parameters.AddWithValue("@placa_carreta", txtPlacaCarreta.Text);
                        cmd.Parameters.AddWithValue("@veiculo", txtVeiculo.Text);
                        cmd.Parameters.AddWithValue("@cor", txtCor.Text);
                        cmd.Parameters.AddWithValue("@data_criacao", DateTime.Now);
                        cmd.Parameters.AddWithValue("numeroCNH", txtCNH.Text);
                        if (dtp_vencimentoCNH.Enabled == false)
                        {
                            cmd.Parameters.AddWithValue("@vencimento_cnh", null);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@vencimento_cnh", Convert.ToDateTime(dtp_vencimentoCNH.Value.ToString()));

                        }

                        resultado = cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            realizar_update = false;
        }
        private void carregar_grid()
        {
            DateTime data_compara = DateTime.Now;
            try
            {
                bool marque = false;
                dsacessos.Rows.Clear();
                DataTable acesso = new DataTable();
                String dados = "select * from historico where data_saida is null order by id_historico desc";
                SQLiteConnection con = new SQLiteConnection(strConn);
                SQLiteDataAdapter envia = new SQLiteDataAdapter(dados, strConn);
                envia.Fill(acesso);
                DataTable grid = acesso;
                string cnh = "";
                foreach (DataRow grd in grid.Rows)
                {
                    cnh = grd[18].ToString();
                    object[] novoDados = new object[]{
                    marque,
                    grd[0].ToString(),//id
                    grd[1].ToString(),//turno
                    grd[2].ToString(),//cpf
                    grd[12].ToString(),//moto
                    grd[13].ToString(),//empresa
                    grd[3].ToString(),//data entrada
                    grd[4].ToString(),//data saida
                    grd[5].ToString(),//placa
                    grd[6].ToString(),//carreta
                    grd[7].ToString(),//veiculo
                    grd[8].ToString(),//cor
                    grd[9].ToString(),//nfentrada
                    grd[10].ToString(),//nf saida
                    grd[11].ToString(),//usuario
                    grd[15].ToString(),//empresa visitada
                    grd[14].ToString(),//senha
                    grd[17].ToString(),//tipo de caminhão
                    grd[19].ToString(),//operação
                    cnh,//validade cnh
                    grd[20].ToString()//CNH
                    
                }; dsacessos.Rows.Add(novoDados);
                    cnh = "";
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            foreach (var row in gridAcesso.DisplayLayout.Rows)
            {
                DateTime dataGrid =  Convert.ToDateTime(row.Cells["Data/hora de entrada"].Text);
                TimeSpan diferenca = data_compara - dataGrid;
                int dia = diferenca.Days;
                int hora = diferenca.Hours;
                

                if (hora > 12 && hora < 24) row.Appearance.BackColor = Color.Yellow;
                if (dia >= 1) row.Appearance.BackColor = Color.Red;
                if (row.Cells["Numero CNH"].Text == "") row.Appearance.BackColor = Color.DeepSkyBlue;
                
            }
        }
        #endregion

        #region eventos
        private void gridAcessosAbertos_Click(object sender, EventArgs e)
        {
            dtSaida.Value = DateTime.Now;
            dtSaida.Visible = lbSaida.Visible = true;
            btnInserir.Visible = false;

        }
        private void cbCPF_KeyUp(object sender, KeyEventArgs e)
        {
            if (rbCpf.Checked == true)
            {
                if (e.KeyCode != Keys.Back && e.KeyCode != Keys.Space)
                {
                    if (cbCPF.Text.Length == 3) { cbCPF.Text += "."; cbCPF.SelectionStart = cbCPF.Text.Length; }
                    else if (cbCPF.Text.Length == 7) { cbCPF.Text += "."; cbCPF.SelectionStart = cbCPF.Text.Length; }
                    else if (cbCPF.Text.Length == 11) { cbCPF.Text += "-"; cbCPF.SelectionStart = cbCPF.Text.Length; }
                }
            }
        }

        private Boolean verificarCampos(string tipo_cadastro)
        {
            if (tipo_cadastro == "motorista")
            {
                if (txtCNH.Text == "" || cbAlteraEmpresa.Text == "" || txtVeiculo.Text == "" || txtPlacaCavalo.Text == "" || cbEmpresaVisitada.Text == "")
                {
                    mensagem("Favor informar os campos obrigatórios!");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;

        }
        private void btnInserir_Click(object sender, EventArgs e)
        {
            int cpf = 0; 
            carregarCombo(3, "");
            string msg = "";
            if (lbStatus.Text == "Liberado")
            {
                if (txtNome.Text != "")
                {
                    if (tipo_cadastro == "motorista")
                    {
                        if (dtp_vencimentoCNH.Value < DateTime.Now)
                        {
                            MessageBox.Show(this, "Motorista com CNH vencida", "Atenção");
                            dtp_vencimentoCNH.Focus();
                            return;
                        }
                    }
                    if (cbTurno.Text != "selecione")
                    {
                        if (cbUsuario.Text != "selecione")
                        {
                            if (cbTipoCaminhao.Text != "selecione" || tipo_cadastro == "outros")
                            {
                                if (cbOperacao.Text != "Selecione" || tipo_cadastro == "outros")
                                {
                                    if (verificarCampos(tipo_cadastro))
                                    {
                                        int resultado = -1;
                                        if (realizar_update == true)
                                        {
                                            update();
                                        }
                                        try
                                        {
                                            using (SQLiteConnection conn = new SQLiteConnection(strConn))
                                            {
                                                conn.Open();
                                                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                                                {
                                                    cmd.CommandText = "INSERT INTO historico(cpf,motorista,empresa,placa,placa_carreta,veiculo,cor,nf_entrada,nf_saida,usuario,turno,data_entrada,senha,data_saida,empresa_visitada, tipo_caminhao, operacao, vencimento_cnh, numeroCNH) VALUES (@cpf,@motorista,@empresa,@placa,@placa_carreta,@veiculo,@cor,@nf_entrada,@nf_saida,@usuario,@turno,@data_entrada,@senha, @data_saida,@empresa_visitada, @tipo_caminhao, @operacao, @vencimento_cnh, @numeroCNH)";
                                                    cmd.Prepare();
                                                    cmd.Parameters.AddWithValue("@cpf", txtcpf.Text);
                                                    cmd.Parameters.AddWithValue("@motorista", txtNome.Text);
                                                    cmd.Parameters.AddWithValue("@empresa", cbAlteraEmpresa.Text);
                                                    cmd.Parameters.AddWithValue("@placa", txtPlacaCavalo.Text);
                                                    cmd.Parameters.AddWithValue("@placa_carreta", txtPlacaCarreta.Text);
                                                    cmd.Parameters.AddWithValue("@veiculo", txtVeiculo.Text);
                                                    cmd.Parameters.AddWithValue("@cor", txtCor.Text);
                                                    cmd.Parameters.AddWithValue("@nf_entrada", txtNfEntrada.Text);
                                                    cmd.Parameters.AddWithValue("@nf_saida", txtNfSaida.Text);
                                                    cmd.Parameters.AddWithValue("@usuario", cbUsuario.Text);
                                                    cmd.Parameters.AddWithValue("@turno", cbTurno.Text);
                                                    cmd.Parameters.AddWithValue("@data_entrada", Convert.ToDateTime(dtpDataEntrada.Text));
                                                    cmd.Parameters.AddWithValue("@senha", txtSenha.Text);
                                                    cmd.Parameters.AddWithValue("@data_saida", null);
                                                    cmd.Parameters.AddWithValue("@empresa_visitada", cbEmpresaVisitada.Text);
                                                    if (tipo_cadastro == "motorista")
                                                    {
                                                        cmd.Parameters.AddWithValue("@tipo_caminhao", cbTipoCaminhao.Text);
                                                        cmd.Parameters.AddWithValue("@vencimento_cnh", Convert.ToDateTime(dtp_vencimentoCNH.Text));
                                                        cmd.Parameters.AddWithValue("@operacao", cbOperacao.Text);
                                                        cmd.Parameters.AddWithValue("@numeroCNH", txtCNH.Text);
                                                    }
                                                    else
                                                    {
                                                        cmd.Parameters.AddWithValue("@tipo_caminhao", "");
                                                        cmd.Parameters.AddWithValue("@vencimento_cnh", null);
                                                        cmd.Parameters.AddWithValue("@operacao", null);
                                                        cmd.Parameters.AddWithValue("numeroCNH", null);
                                                    }
                                                    resultado = cmd.ExecuteNonQuery();
                                                    conn.Close();
                                                    carregar_grid();

                                                    mensagem("Acesso inserido com sucesso");

                                                    DialogResult dr = System.Windows.Forms.DialogResult.Yes;
                                                    if (tipo_cadastro == "motorista")
                                                    {
                                                        dr = MessageBox.Show(
                                                                  this,
                                                                  "Deseja imprimir o transbordo de carga e descarga?",
                                                                  Application.ProductName,
                                                                  MessageBoxButtons.YesNo,
                                                                  MessageBoxIcon.Question);
                                                        switch (dr)
                                                        {
                                                            case DialogResult.Yes: imprimir();
                                                                break;
                                                            case DialogResult.No: ;
                                                                break;
                                                        }
                                                    }
                                                    limpar_campos();
                                                    
                                                }
                                            }
                                        }
                                        catch (Exception ex) { MessageBox.Show(ex.Message); }
                                    }
                                }
                                else
                                {
                                    mensagem("Selecione o tipo de operação");
                                    cbOperacao.Focus();
                                }
                            }
                            else { mensagem("Selecione um tipo de caminhão"); cbTipoCaminhao.Focus(); }
                        }
                        else { mensagem("Selecione um usuario"); cbUsuario.Focus(); }
                    }
                    else { mensagem("Selecione um turno"); cbTurno.Focus(); }
                }
                else { mensagem("Acesso inexistente\nSelecione um cpf ou insira um novo cadastro"); cbCPF.Text = "selecione"; }
            }
            else
            { 
                mensagemErro("Cadastro Bloqueado\nMotivo: " + lbMotivo.Text);
                enabled_fields();
            }
        }
        private void gridAcesso_ClickCell(object sender, ClickCellEventArgs e)
        {
            ultraToolbarsManager1.Tools["Imprimi"].SharedProps.Enabled = true;
            if (dsacessos.Rows.Count > 0) cbCPF.Enabled = false;
            else cbCPF.Enabled = true;
            
            if (!e.Cell.Row.Cells["X"].IsActiveCell)
            {
                dtSaida.Visible = lbSaida.Visible = picAnularFinal.Visible = true;
                dtSaida.Enabled = dtp_vencimentoCNH.Enabled = cbTipoCaminhao.Enabled = cbOperacao.Enabled = true;
                txtcpf.Text = e.Cell.Row.Cells["CPF"].Text;
                cbTurno.Text= e.Cell.Row.Cells["Turno"].Text;
                cbCPF.Text = e.Cell.Row.Cells["CPF"].Text;
                txtNome.Text = e.Cell.Row.Cells["Motorista"].Text;
                cbAlteraEmpresa.Text = e.Cell.Row.Cells["Empresa"].Text;
                txtVeiculo.Text = e.Cell.Row.Cells["Veículo"].Text;
                txtCor.Text = e.Cell.Row.Cells["Cor"].Text;
                txtPlacaCavalo.Text = e.Cell.Row.Cells["Placa"].Text;
                txtPlacaCarreta.Text = e.Cell.Row.Cells["Placa carreta"].Text;
                txtNfEntrada.Text = e.Cell.Row.Cells["NF entrada"].Text;
                txtNfSaida.Text = e.Cell.Row.Cells["NF saída"].Text;
                txtSenha.Text = e.Cell.Row.Cells["Senha"].Text;
                dtpDataEntrada.Value = Convert.ToDateTime(e.Cell.Row.Cells["Data/hora de entrada"].Text);
                //cbUsuario.Text = e.Cell.Row.Cells["Usuário"].Text;
                cbTurno.Text = e.Cell.Row.Cells["Turno"].Text;
                txtId.Text = "ID: " + e.Cell.Row.Cells["Id"].Text;
                cbEmpresaVisitada.Text = e.Cell.Row.Cells["Empresa Visitada"].Text;
                if (e.Cell.Row.Cells["Tipo de caminhão"].Text == "")
                {
                    cbTipoCaminhao.Text = "selecione";
                    cbTipoCaminhao.Enabled = false;
                }
                else
                {
                    cbTipoCaminhao.Text = e.Cell.Row.Cells["Tipo de caminhão"].Text;
                    cbTipoCaminhao.Enabled = true;
                }
                cbTipoCaminhao.Text = e.Cell.Row.Cells["Tipo de caminhão"].Text;
                if (e.Cell.Row.Cells["Operação"].Text == "")
                {
                    cbOperacao.Text = "selecione";
                    cbOperacao.Enabled = false;
                }
                else 
                {
                    cbOperacao.Text = e.Cell.Row.Cells["Operação"].Text;
                    cbOperacao.Enabled = true;
                }
                if (e.Cell.Row.Cells["Vencimento CNH"].Text == "")
                {
                    dtp_vencimentoCNH.Value = DateTime.Now;
                    dtp_vencimentoCNH.Enabled = false;
                }
                else 
                {
                    dtp_vencimentoCNH.Value = Convert.ToDateTime(e.Cell.Row.Cells["Vencimento CNH"].Text);
                    txtCNH.Text = e.Cell.Row.Cells["Numero CNH"].Text;
                    dtp_vencimentoCNH.Enabled = true;
                }
                
                dtSaida.Value = DateTime.Now;
                dtSaida.Focus();
                btnInserir.Visible = false;
                enabled_fields();
       
            }

        }
        private void ultraToolbarsManager1_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            switch (e.Tool.Key)
            {
                case "Atualizar":
                    {
                        atualizar();
                    }
                    break;
                case "Cadastro":
                    {
                        if (cbUsuario.Text != "selecione")
                        {
                            cadastro janela = new cadastro(cbUsuario.Text);
                            janela.ShowDialog();
                            atualizar();
                        }
                        else 
                        {
                            mensagem("Informe o usuário!");
                            cbUsuario.Focus();
                        }
                    }
                    break;
                case "Histórico":
                    {
                        Historico janela2 = new Historico();
                        janela2.ShowDialog();
                        atualizar();
                    }
                    break;
                case "Colaborador":
                    {
                        if (cbUsuario.Text != "selecione")
                        {
                            colaborador janela3 = new colaborador(cbUsuario.Text);
                            janela3.ShowDialog();
                            //atualizar();
                        }
                        else
                        {
                            mensagem("Informe o usuário!");
                            cbUsuario.Focus();
                        }
                    }
                    break;
                case "Salvar":
                    {
                        if (txtNome.Text != "")
                        {
                            string msg = "";
                            if (dtpDataEntrada.Text != "")
                            {
                                if (dtSaida.Text != "")
                                {
                                    msg = salvar_atualizacao();

                                }
                                else msg = "Campo Data/hora saída não pode ficar em branco";
                            }
                            else msg = "Campo Data/hora entrada não pode ficar em branco";

                            if (msg != "")
                            {
                                mensagem(msg);
                            }
                        }
                        else mensagem("Não há dados para salvar!");
                        
                    }
                    break;
                case "Deletar":
                    {
                        this.Cursor = Cursors.WaitCursor;
                        deletar();
                        cbCPF.Enabled = true;
                        this.Cursor = Cursors.Default;
                    }
                    break;
                case "Exportar":
                    {
                        if (dsacessos.Rows.Count > 0) exportar();
                        else mensagem("Tabela vazia!");
                        
                    }
                    break;
                case "Novo Usuário":
                    {
                        Usuario janela3 = new Usuario();
                        janela3.ShowDialog();
                        cbUsuario.Items.Clear();
                        carregarCombo(1, "select nome from usuario");
                    }
                    break;
                case "Marcar Todos":
                    {
                        marcarTodos();
                    }
                    break;
                case "Desmarcar Todos":
                    {
                        desmarcarTodos();
                    }
                    break;
                case "Contato Desevolvedor":
                    {

                        ProcessStartInfo sInfo = new ProcessStartInfo("http://www.labsoft.tech");
                        Process.Start(sInfo);
                        /*try
                        {
                            Process.Start("chrome.exe", "http://mvlprogramer.esy.es/Perfil_profissional/");
                        }
                        catch { Process.Start("iexplorer.exe", "http://mvlprogramer.esy.es/Perfil_profissional/"); }*/
                    }
                    break;
                case "Novo Backup":
                    {
                        SalvarBKP();
                        MessageBox.Show("Backup salvo com sucesso!");
                    }
                    break;
                case "imprimi":
                    {
                        imprimir();
                    }
                    break;
            }
        }
        private void btnCarregar_Click(object sender, EventArgs e)
        {
            //limpar_campos();
            btnInserir.Visible = true;
            lbStatus.Visible = true;
            string cnh = null;
            dtp_vencimentoCNH.Enabled = cbOperacao.Enabled = cbTipoCaminhao.Enabled =    true;

            if (dtSaida.Visible == false) dtpDataEntrada.Value = DateTime.Now;
            try
            {
                SQLiteConnection con = new SQLiteConnection(strConn);
                con.Open();

                SQLiteCommand comando = null;
                if (rbCpf.Checked == true) comando = new SQLiteCommand("select * from cadastro where cpf = @busca order by id_cadastro desc", con);
                else if (rbPlaca.Checked == true) comando = new SQLiteCommand("select * from cadastro where placa = @busca order by id_cadastro desc", con);
                else comando = new SQLiteCommand("select * from cadastro where nome = @busca order by id_cadastro desc", con);
                comando.Parameters.AddWithValue("@busca", cbCPF.Text);
                var reader = comando.ExecuteReader();
                while (reader.Read())
                {
                    txtcpf.Text = reader["cpf"].ToString();
                    txtNome.Text = reader["nome"].ToString();
                    cbAlteraEmpresa.Text = reader["empresa"].ToString();
                    txtPlacaCavalo.Text = reader["placa"].ToString();
                    txtPlacaCarreta.Text = reader["placa_carreta"].ToString();
                    txtVeiculo.Text = reader["veiculo"].ToString();
                    txtCor.Text = reader["cor"].ToString();
                    txtId.Text = "ID: " + reader["id_cadastro"].ToString();
                    
                    tipo_cadastro = reader["tipo_cadastro"].ToString();
                    cnh = reader["vencimento_cnh"].ToString();
                    txtCNH.Text = reader["numeroCNH"].ToString();
                    lbStatus.Text = reader["status"].ToString();

                    if (lbStatus.Text == "Bloqueado")
                    {
                        lbStatus.ForeColor = Color.Red;
                        lbMotivo.Text = reader["motivo"].ToString();
                    }
                    else
                    {
                        lbStatus.ForeColor = Color.Green;
                        lbMotivo.Text = "";
                    }
                }
                if (tipo_cadastro == "outros")
                {
                    dtp_vencimentoCNH.Enabled = cbOperacao.Enabled = cbTipoCaminhao.Enabled = false;
                }
                else
                {
                    if (cnh == null)
                    {
                        dtp_vencimentoCNH.Value = DateTime.Now;
                        mensagem("Informe o vencimento da CNH");
                        dtp_vencimentoCNH.Focus();
                    }
                    else
                    {
                        ultraToolbarsManager1.Tools["Imprimi"].SharedProps.Enabled = true;
                        dtp_vencimentoCNH.Value = Convert.ToDateTime(cnh);
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
        private void txtLimparCampos_Click(object sender, EventArgs e)
        {
            limpar_campos();  
        }

        private void gridAcesso_CellChange(object sender, CellEventArgs e)
        {
            if ((bool)e.Cell.Row.Cells["X"].Value)
            {
                e.Cell.Row.Cells["X"].Value = false;
                id.Remove(e.Cell.Row.Cells["id"].Text);
            }
            else
            {
                e.Cell.Row.Cells["X"].Value = true;
                id.Add(e.Cell.Row.Cells["id"].Text);
            }
        }
        private void txtVeiculo_KeyUp(object sender, KeyEventArgs e)
        {
            realizar_update = true;
        }


        private void picAnularFinal_Click(object sender, EventArgs e)
        {
            if (dtSaida.Visible == true)  dtSaida.Visible = lbSaida.Visible = false;
            else dtSaida.Visible = lbSaida.Visible = true; 
            
        }
        private void cbAcessoAberto_SelectedIndexChanged(object sender, EventArgs e)
        {
            string cnh = "";
            btnInserir.Visible = false;
            dtSaida.Visible = true;
            picAnularFinal.Visible = true;
            lbSaida.Visible = cbOperacao.Enabled = cbTipoCaminhao.Enabled = dtp_vencimentoCNH.Enabled = true;
            try
            {
                SQLiteConnection con = new SQLiteConnection(strConn);
                con.Open();
                SQLiteCommand comando = null;
                if (rbSenha.Checked == true) comando = new SQLiteCommand("select * from historico where senha = @busca and data_saida is null", con);
                else comando = new SQLiteCommand("select * from historico where motorista = @busca and data_saida is null", con);
                comando.Parameters.AddWithValue("@busca", cbAcessoAberto.Text);

                var reader = comando.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        txtcpf.Text = reader["cpf"].ToString();
                        txtNome.Text = reader["nome"].ToString();
                        cbAlteraEmpresa.Text = reader["empresa"].ToString();
                        txtPlacaCavalo.Text = reader["placa"].ToString();
                        txtPlacaCarreta.Text = reader["placa_carreta"].ToString();
                        txtVeiculo.Text = reader["veiculo"].ToString();
                        txtCor.Text = reader["cor"].ToString();
                        txtId.Text = "ID: " + reader["id_cadastro"].ToString();

                        tipo_cadastro = reader["tipo_cadastro"].ToString();
                        cnh = reader["vencimento_cnh"].ToString();
                        txtCNH.Text = reader["numeroCNH"].ToString();
                    }
                    if (tipo_cadastro == "outros")
                    {
                        dtp_vencimentoCNH.Enabled = cbOperacao.Enabled = cbTipoCaminhao.Enabled = false;
                    }
                    else
                    {
                        if (cnh == null)
                        {
                            dtp_vencimentoCNH.Value = DateTime.Now;
                            mensagem("Informe o vencimento da CNH");
                            dtp_vencimentoCNH.Focus();
                        }
                        else
                        {
                            ultraToolbarsManager1.Tools["Imprimi"].SharedProps.Enabled = true;
                            dtp_vencimentoCNH.Value = Convert.ToDateTime(cnh);
                            txtCNH.Text = reader["numeroCNH"].ToString();
                        }
                    }   
                }
                else
                {
                    MessageBox.Show("Senha inválida!");
                }

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void rbNome_CheckedChanged(object sender, EventArgs e)
        {
            cbCPF.Items.Clear();
            if (rbCpf.Checked == true) carregarCombo(2, "select cpf from cadastro");
            else if (rbPlaca.Checked) carregarCombo(2, "select placa from cadastro where placa != '' order by placa");
            else carregarCombo(2, "select nome from cadastro order by nome");
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            int offset = 125;

            //print header
            graphics.DrawString("SIGMA TRANSPORTES", bold, Brushes.Black, 40, 0);
            graphics.DrawString(Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy"), regular, Brushes.Black, 100,20);
           
        
            graphics.DrawString("DADOS DA TRANSPORTADORA", media, Brushes.Black, 20, 45);
            graphics.DrawString("Trans.: " + cbAlteraEmpresa.Text, regular, Brushes.Black, 20, 65);          
            graphics.DrawString("Placa do cavalo: " + txtPlacaCavalo.Text, regular, Brushes.Black, 20, 80);
            graphics.DrawString("Placa da carreta: " + txtPlacaCarreta.Text, regular, Brushes.Black, 20, 95);
            graphics.DrawString("Tipo de veículo: " + cbTipoCaminhao.Text, regular, Brushes.Black, 20, 110);
            graphics.DrawString("Operação: " +cbOperacao.Text, regular, Brushes.Black, 20, 125);
            graphics.DrawString("Horário de entrada: " + Convert.ToDateTime(dtpDataEntrada.Text).ToString("HH:mm:ss"), regular, Brushes.Black, 20, 140);
           
           

           
            graphics.DrawString("DADOS DO MOTORISTA", media, Brushes.Black, 20, 170);
            graphics.DrawString("Motorista: " + txtNome.Text, regular, Brushes.Black, 20, 190);
            graphics.DrawString("CNH: " + txtCNH.Text, regular, Brushes.Black, 20, 205);
            graphics.DrawString("Vencimento: " + dtp_vencimentoCNH.Text, regular, Brushes.Black, 20, 220);
            graphics.DrawString("CPF: " + cbCPF.Text, regular, Brushes.Black, 20, 235);


            

            graphics.DrawString("Nota fiscal de entrada: " + txtNfEntrada.Text, regular, Brushes.Black, 20, 270);
            graphics.DrawString("Nota fiscal de saída: " + "____________________", regular, Brushes.Black, 20, 295);



            graphics.DrawString("TRUCK LOG", bold, Brushes.Black, 75, 330);
            graphics.DrawString("(    ) Check In                  (    ) Docking ", regular, Brushes.Black, 20, 350);
            graphics.DrawString("(    ) Load Begin              (    ) Load End ", regular, Brushes.Black, 20, 370);
            graphics.DrawString("(    ) documents S           (    ) Check Out ", regular, Brushes.Black, 20, 390);


            graphics.DrawString("Data de saída: ____/____/____", regular, Brushes.Black, 20, 430);
            graphics.DrawString("Horário de saída: _____:_____", regular, Brushes.Black, 20, 460);

            graphics.DrawString("__________________________", regular, Brushes.Black, 50, 500);
            graphics.DrawString("assinatura ", pequena, Brushes.Black, 110, 520);

            
          

        }

        private void rbSenha_CheckedChanged(object sender, EventArgs e)
        {
            cbAcessoAberto.Items.Clear();
            if (rbSenha.Checked == true) carregarCombo(4, "select senha from historico where data_saida is null and senha <> '' and senha is not null");
            else
            {
                carregarCombo(4, "select motorista from historico where data_saida is null");
                carregar_autocomplete("select motorista from historico where data_saida is null ", 2);
            }
        }
        #endregion     

        #region bkp
        private void SalvarBKP()
        {
            string NomeArquivo = "cielog.s3db";
            string caminho = @"C:\cielog\";
            string diretorio = @"C:\Backup Banco  de dados Cielog\";

            string CopiaArq = System.IO.Path.Combine(caminho, NomeArquivo);
            string Novo = System.IO.Path.Combine(diretorio, NomeArquivo);
            try
            {
                if (!System.IO.Directory.Exists(diretorio))
                {
                    System.IO.Directory.CreateDirectory(diretorio);
                }

                System.IO.File.Copy(CopiaArq, Novo, true);

                /*if (System.IO.Directory.Exists(caminho))
                {
                    //criei um array para o caso de bkp's de mais de 1 arquivo
                    string[] arquivos = System.IO.Directory.GetFiles(caminho);

                    foreach (string arq in arquivos)
                    {
                        NomeArquivo = System.IO.Path.GetFileName(arq);
                        Novo = System.IO.Path.Combine(diretorio, NomeArquivo);
                        System.IO.File.Copy(arq, Novo, true);
                    }
                }*/
            }
            catch { }
        }

        private void timerBKP_Tick(object sender, EventArgs e)
        {

            timer_bkp++;
            if (timer_bkp == 60)
            {
                timer_bkp = 0;
                string hora = DateTime.Now.ToString("hh:mm");
                if (hora == "06:00")
                {
                    timerBKP.Enabled = false;
                    SalvarBKP();
                    timerBKP.Enabled = true;
                }
            }
        }

        #endregion

        private void dtp_vencimentoCNH_ValueChanged(object sender, EventArgs e)
        {
            realizar_update = true;
        }

        //eventos acelerar procesos de busca no banco - iniciar telas
        private void cbCPF_MouseClick(object sender, MouseEventArgs e)
        {
            if (contComboCPF == 0)
            {
                if (rbCpf.Checked == true) carregarCombo(2, "select cpf from cadastro order by cpf");
                else if (rbPlaca.Checked == true) carregarCombo(2, "select placa from cadastro order by placa");
                else carregarCombo(2, "select nome from cadastro order by nome");

                carregar_autocomplete("select cpf from cadastro order by cpf", 1);
                contComboCPF++;
            }
        }


        private void cbAcessoAberto_MouseClick(object sender, MouseEventArgs e)
        {
            if (contComboAcesso == 0)
            {
                if (rbSenha.Checked == true) carregarCombo(4, "select senha from historico where data_saida is null and senha <> '' and senha is not null");
                else
                {
                    carregarCombo(4, "select motorista from historico where data_saida is null");
                    carregar_autocomplete("select motorista from historico where data_saida is null ", 2);
                }
                contComboAcesso++;
            }
        }

        private void cbAlteraEmpresa_MouseClick(object sender, MouseEventArgs e)
        {
            if (contEmpresa == 0)
            {
                carregarCombo(5, "select nome from empresa order by nome");
                contEmpresa++;
            }
        }

        private void cbEmpresaVisitada_MouseClick(object sender, MouseEventArgs e)
        {
            if (contComboEmpresaVisitada == 0)
            {
                carregarCombo(6, "select nome from empresa_visitada order by nome");
                contComboEmpresaVisitada++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("teste");
        }

    }
}
