﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace WindowsFormsApplication1
{
    class ColaboradroDAO
    {
        private Conn conn;
        private SQLiteDataAdapter sqlAdapter;
        private string sql;
        private bool flag;

        public string Sql
        {
            get { return sql; }
            set { sql = value; }
        }


        public SQLiteDataAdapter SqlAdapter
        {
            get { return sqlAdapter; }
            set { sqlAdapter = value; }
        }

        private SQLiteDataReader reader;
        public ClassColaborador classColaborador;

        public SQLiteDataReader Reader
        {
            get { return reader; }
            set { reader = value; }
        }

        public bool insert(ClassColaborador colaborador)
        {
            sql = "insert into colaborador (nome, matricula, cpf, empresa, veiculo, placa) values (@nome, @matricula, @cpf, @empresa, @veiculo, @placa)";
            return this.ConnParameters(colaborador, sql, "insert");
        }

        public bool update(ClassColaborador colaborador)
        {
            sql = "update colaborador set nome = @nome, matricula = @matricula, cpf = @cpf, empresa = @empresa, veiculo = @veiculo, placa = @placa where id = @id";
            return this.ConnParameters(colaborador, sql, "update");
        }

        public bool delete(int id)
        {
            try
            {
                this.conn = new Conn();
                this.Sql = "delete from colaborador where id = @id";
                this.conn.conection_parameter(this.sql);
                this.conn.Cmd.Parameters.AddWithValue("@id", id);
                this.conn.Cmd.ExecuteNonQuery();
                this.conn.desconectar();
                return true;
            }
            catch (Exception e) { return false; }
            
        }
        public void select()
        {
            this.classColaborador = new ClassColaborador();
            this.conn = new Conn();
            this.Sql = "select * from colaborador order by nome";
            this.conn.connection(sql);
            this.reader = this.conn.Cmd.ExecuteReader();
        }

        public void selectGrid()
        {
            this.classColaborador = new ClassColaborador();
            this.conn = new Conn();
            this.Sql = "select * from colaborador order by nome";
            this.conn.connection(sql);
            this.sqlAdapter = new SQLiteDataAdapter(this.conn.Cmd);
            this.conn.desconectar();
        }

        public void selectId(int id)
        {
            this.classColaborador = new ClassColaborador();
            this.conn = new Conn();
            this.Sql = "select * from colaborador where id = @id";
            this.conn.connection(sql);
            this.conn.Cmd.Parameters.AddWithValue("@id", id);
            this.reader = this.conn.Cmd.ExecuteReader();
        }

        //metodos compartilhados

        public bool ConnParameters(ClassColaborador colaborador, string sql, string metodo)
        {
            try
            {
                this.conn = new Conn();
                conn.conection_parameter(sql);
                if (metodo == "update")
                {
                    this.conn.Cmd.Parameters.AddWithValue("@id", colaborador.Id);
                }
                this.conn.Cmd.Parameters.AddWithValue("@nome", colaborador.Nome);
                this.conn.Cmd.Parameters.AddWithValue("@nome", colaborador.Nome);
                this.conn.Cmd.Parameters.AddWithValue("@matricula", colaborador.Matricula);
                this.conn.Cmd.Parameters.AddWithValue("@cpf", colaborador.Cpf);
                this.conn.Cmd.Parameters.AddWithValue("@empresa", colaborador.Empresa);
                this.conn.Cmd.Parameters.AddWithValue("@veiculo", colaborador.Veiculo);
                this.conn.Cmd.Parameters.AddWithValue("@placa", colaborador.Placa);
                this.conn.Cmd.ExecuteNonQuery();
                this.conn.desconectar();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            
        }
    }
}
