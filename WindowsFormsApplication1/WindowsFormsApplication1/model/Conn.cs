﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace WindowsFormsApplication1
{
    class Conn
    {
        private String strConn;
        private SQLiteConnection con;
        private SQLiteCommand cmd;
        private int result;

        public int Result
        {
            get { return result; }
            set { result = value; }
        } 

        public SQLiteCommand Cmd
        {
            get { return cmd; }
            set { cmd = value; }
        }

        public String StrConn
        {
            get { return @"Data Source=C:\cielog\cielog.s3db"; }
            set { strConn = value; }
        }

        public SQLiteConnection Con
        {
            get { return con; }
            set { con = value; }
        }

        public void connection(string sql)
        {
            this.con = new SQLiteConnection(this.StrConn);
            this.con.Open();
            this.cmd = new SQLiteCommand(sql, con);
        }

        public void conection_parameter(string sql)
        {
            this.con = new SQLiteConnection(StrConn);
            this.con.Open();
            this.cmd = new SQLiteCommand(sql, con);
            this.cmd.Prepare();  
        }
        public void desconectar() 
        {
            con.Close();
        }
        
    }
}
