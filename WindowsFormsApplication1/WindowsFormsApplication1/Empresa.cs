﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using System.Data.SQLite;

namespace WindowsFormsApplication1
{
    public partial class empresa : Form
    {
        public empresa(string tipo)
        {
            InitializeComponent();
            tipo_global = tipo;
            carregarQuery();
            carrega_grid();
            
        }

        string id = "";
        String strConn = @"Data Source=C:\cielog\cielog.s3db";//string  para conexão com o bano de dados, declaração global
        string tipo_global;
        string insert, select, delete;
        
        private void carregarQuery()
        {
            if (tipo_global == "visita")
            {
                select = "select * from empresa";
                insert = "INSERT INTO empresa(nome) VALUES (@nome)";
                delete = "delete from empresa where id_empresa = @id";
                 
            }
            else 
            {
                select = "select * from empresa_visitada";
                insert = "INSERT INTO empresa_visitada(nome) VALUES (@nome)";
                delete = "delete from empresa_visitada where id_empresa_visitada = @id";
                label1.Text = "Informe o nome da empresa";
            }
        }
        private void carrega_grid()
        {

            dsEmpresa.Rows.Clear();
            DataTable usuario = new DataTable();
            SQLiteConnection con = new SQLiteConnection(strConn);
            con.Open();
            SQLiteCommand cmd = new SQLiteCommand(con);
            cmd.CommandText = select;
            SQLiteDataAdapter envia = new SQLiteDataAdapter(cmd);
            envia.Fill(usuario);
            DataTable grid = usuario;
            foreach (DataRow grd in grid.Rows)
            {
                object[] novoDados = new object[]{
                    grd[0].ToString(),//id
                    grd[1].ToString(),//nome
                }; dsEmpresa.Rows.Add(novoDados);
            }
        }

        private void pbSalvar_usuario_Click_1(object sender, EventArgs e)
        {
            if (txtempresa.Text != "")
            {
                int resultado = -1;
                try
                {
                    using (SQLiteConnection conn = new SQLiteConnection(strConn))
                    {
                        conn.Open();
                        using (SQLiteCommand cmd = new SQLiteCommand(conn))
                        {
                            cmd.CommandText = insert;
                            cmd.Prepare();
                            cmd.Parameters.AddWithValue("@nome", txtempresa.Text);
                            try
                            {
                                resultado = cmd.ExecuteNonQuery();
                            }
                            catch (SQLiteException ex)
                            {
                                throw ex;
                            }
                            finally
                            {
                                conn.Close();
                            }
                            carrega_grid();
                            DialogResult dr = System.Windows.Forms.DialogResult.Yes;
                            dr = MessageBox.Show(
                                      this,
                                      "Dados inseridos com sucesso!",
                                      "Informação",
                                      MessageBoxButtons.OK,
                                      MessageBoxIcon.Information);
                        }
                    }
                    txtempresa.Clear();
                }
                   
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
            else { MessageBox.Show("Favor informar um nome!"); txtempresa.Focus(); }
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            DialogResult dr = System.Windows.Forms.DialogResult.Yes;
            dr = MessageBox.Show(
                      this,
                      "Tem certeza que deseja excluir o cadastro ID: " + id + "?",
                      "Confirmação",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Question);
            switch (dr)
            {
                case DialogResult.Yes:
                    {
                        int resultado = -1;
                        using (SQLiteConnection conn = new SQLiteConnection(strConn))
                        {
                            conn.Open();
                            using (SQLiteCommand cmd = new SQLiteCommand(conn))
                            {
                                cmd.CommandText = delete;
                                cmd.Prepare();
                                cmd.Parameters.AddWithValue("@id", id);
                                resultado = cmd.ExecuteNonQuery();
                                conn.Close();
                                txtempresa.Text = "";
                                carrega_grid();
                            }
                        }
                    }
                    break;
                case DialogResult.No: break;
            }
        }

        private void gridUsuario_MouseEnterElement_1(object sender, Infragistics.Win.UIElementEventArgs e)
        {
            UltraGridCell cell = e.Element.GetContext(typeof(UltraGridCell)) as UltraGridCell;

            if (cell != null)
            {
                cell.Row.Activate();
            }
        }

        private void gridUsuario_ClickCell_1(object sender, ClickCellEventArgs e)
        {
            id = e.Cell.Row.Cells["Id"].Text;
            txtempresa.Text = e.Cell.Row.Cells["Nome"].Text;
        }

    }
}
