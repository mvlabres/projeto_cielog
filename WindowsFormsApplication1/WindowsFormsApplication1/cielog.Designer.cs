﻿namespace WindowsFormsApplication1
{
    partial class Fgeral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("X");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Id");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Turno");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CPF");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Motorista");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Empresa");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Data/hora de entrada");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Data/hora de saída");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn9 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Placa");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn10 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Placa carreta");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Veículo");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Cor");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NF entrada");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NF saída");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Usuário");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Empresa Visitada");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Senha");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn18 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Tipo de caminhão");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn19 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Operação");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn20 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Vencimento CNH");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn21 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Numero CNH");
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn1 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("X");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn2 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Id");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn3 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Turno");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn4 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CPF");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn5 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Motorista");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn6 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Empresa");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn7 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Data/hora de entrada");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn8 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Data/hora de saída");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn9 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Placa");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn10 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Placa carreta");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn11 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Veículo");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn12 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Cor");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn13 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("NF entrada");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn14 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("NF saída");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn15 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Usuário");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn16 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Empresa Visitada");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn17 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Senha");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn18 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Tipo de caminhão");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn19 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Operação");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn20 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Vencimento CNH");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn21 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Numero CNH");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn22 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CPF");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn23 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Nome");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn24 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Empresa");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn25 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Placa Cavalo");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn26 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Placa Carreta");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn27 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Tipo de caminhão");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn28 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Data/hora entrada");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn29 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Data/hora saida");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn30 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Usuario");
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab1 = new Infragistics.Win.UltraWinToolbars.RibbonTab("Módulos");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup1 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("Módulos");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool1 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool1 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Cadastro");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool32 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Colaborador");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool3 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Histórico");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup2 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool22 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Atualizar");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool5 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Salvar");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool10 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Deletar");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool8 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exportar");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool24 = new Infragistics.Win.UltraWinToolbars.ButtonTool("imprimi");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool3 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool2");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool14 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Novo Usuário");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool17 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Marcar Todos");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool20 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Desmarcar Todos");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup3 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("Desenvolvedor");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool26 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Novo Backup");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool12 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Contato Desevolvedor");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool2 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Cadastro");
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.LabelTool labelTool2 = new Infragistics.Win.UltraWinToolbars.LabelTool("logo");
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.LabelTool labelTool3 = new Infragistics.Win.UltraWinToolbars.LabelTool("LabelTool1");
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.LabelTool labelTool5 = new Infragistics.Win.UltraWinToolbars.LabelTool("LabelTool2");
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool4 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Histórico");
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool6 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Gerenciador");
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool2 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool7 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Salvar");
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool9 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exportar");
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool11 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Deletar");
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool13 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Imprimir");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool4 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool2");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool15 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Novo Usuário");
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool18 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ButtonTool1");
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool19 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Marcar Todos");
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool21 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Desmarcar Todos");
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool16 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Contato Desevolvedor");
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool23 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Atualizar");
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool25 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Nova Empresa");
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool27 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Salvar Backup");
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool29 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Salvar Backup Banco");
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool31 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Salvar Backup Banco de Dados");
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool28 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Novo Backup");
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool30 = new Infragistics.Win.UltraWinToolbars.ButtonTool("imprimi");
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool33 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Colaborador");
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fgeral));
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.lbMotivo = new System.Windows.Forms.Label();
            this.lbStatus = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rbPlaca = new System.Windows.Forms.RadioButton();
            this.rbNome = new System.Windows.Forms.RadioButton();
            this.rbCpf = new System.Windows.Forms.RadioButton();
            this.rbNome_Acesso = new System.Windows.Forms.RadioButton();
            this.rbSenha = new System.Windows.Forms.RadioButton();
            this.lbAcessoAberto = new System.Windows.Forms.Label();
            this.cbAcessoAberto = new System.Windows.Forms.ComboBox();
            this.txtLimparCampos = new System.Windows.Forms.Button();
            this.txtId = new System.Windows.Forms.TextBox();
            this.gridAcesso = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dsacessos = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.txtCNH = new System.Windows.Forms.TextBox();
            this.dtp_vencimentoCNH = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.cbOperacao = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cbTipoCaminhao = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbusuarioSaida = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtcpf = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbEmpresaVisitada = new System.Windows.Forms.ComboBox();
            this.cbAlteraEmpresa = new System.Windows.Forms.ComboBox();
            this.picAnularFinal = new System.Windows.Forms.PictureBox();
            this.txtNfSaida = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtNfEntrada = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCor = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtVeiculo = new System.Windows.Forms.TextBox();
            this.txtPlacaCarreta = new System.Windows.Forms.TextBox();
            this.txtPlacaCavalo = new System.Windows.Forms.TextBox();
            this.btnInserir = new System.Windows.Forms.Button();
            this.lbSaida = new System.Windows.Forms.Label();
            this.dtSaida = new System.Windows.Forms.DateTimePicker();
            this.dtpDataEntrada = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbCPF = new System.Windows.Forms.ComboBox();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.cbTurno = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbUsuario = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dsAcessosAbertos = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.Fgeral_Fill_Panel = new Infragistics.Win.Misc.UltraPanel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this._Fgeral_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.ultraToolbarsManager1 = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
            this._Fgeral_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._Fgeral_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._Fgeral_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.timerBKP = new System.Windows.Forms.Timer(this.components);
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.excelCielo = new Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter(this.components);
            this.ultraTabPageControl2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAcesso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsacessos)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAnularFinal)).BeginInit();
            this.ultraTabPageControl1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsAcessosAbertos)).BeginInit();
            this.Fgeral_Fill_Panel.ClientArea.SuspendLayout();
            this.Fgeral_Fill_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.lbMotivo);
            this.ultraTabPageControl2.Controls.Add(this.lbStatus);
            this.ultraTabPageControl2.Controls.Add(this.panel3);
            this.ultraTabPageControl2.Controls.Add(this.rbNome_Acesso);
            this.ultraTabPageControl2.Controls.Add(this.rbSenha);
            this.ultraTabPageControl2.Controls.Add(this.lbAcessoAberto);
            this.ultraTabPageControl2.Controls.Add(this.cbAcessoAberto);
            this.ultraTabPageControl2.Controls.Add(this.txtLimparCampos);
            this.ultraTabPageControl2.Controls.Add(this.txtId);
            this.ultraTabPageControl2.Controls.Add(this.gridAcesso);
            this.ultraTabPageControl2.Controls.Add(this.panel1);
            this.ultraTabPageControl2.Controls.Add(this.label1);
            this.ultraTabPageControl2.Controls.Add(this.cbCPF);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1306, 754);
            // 
            // lbMotivo
            // 
            this.lbMotivo.AutoSize = true;
            this.lbMotivo.Location = new System.Drawing.Point(915, 28);
            this.lbMotivo.Name = "lbMotivo";
            this.lbMotivo.Size = new System.Drawing.Size(0, 13);
            this.lbMotivo.TabIndex = 16;
            // 
            // lbStatus
            // 
            this.lbStatus.AutoSize = true;
            this.lbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatus.Location = new System.Drawing.Point(732, 34);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(0, 15);
            this.lbStatus.TabIndex = 15;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.rbPlaca);
            this.panel3.Controls.Add(this.rbNome);
            this.panel3.Controls.Add(this.rbCpf);
            this.panel3.Location = new System.Drawing.Point(120, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(178, 27);
            this.panel3.TabIndex = 14;
            // 
            // rbPlaca
            // 
            this.rbPlaca.AccessibleName = "Insere";
            this.rbPlaca.AutoSize = true;
            this.rbPlaca.Location = new System.Drawing.Point(122, 5);
            this.rbPlaca.Name = "rbPlaca";
            this.rbPlaca.Size = new System.Drawing.Size(52, 17);
            this.rbPlaca.TabIndex = 14;
            this.rbPlaca.Text = "Placa";
            this.rbPlaca.UseVisualStyleBackColor = true;
            this.rbPlaca.CheckedChanged += new System.EventHandler(this.rbNome_CheckedChanged);
            // 
            // rbNome
            // 
            this.rbNome.AccessibleName = "Insere";
            this.rbNome.AutoSize = true;
            this.rbNome.Location = new System.Drawing.Point(64, 5);
            this.rbNome.Name = "rbNome";
            this.rbNome.Size = new System.Drawing.Size(53, 17);
            this.rbNome.TabIndex = 13;
            this.rbNome.Text = "Nome";
            this.rbNome.UseVisualStyleBackColor = true;
            this.rbNome.CheckedChanged += new System.EventHandler(this.rbNome_CheckedChanged);
            // 
            // rbCpf
            // 
            this.rbCpf.AccessibleName = "Insere";
            this.rbCpf.AutoSize = true;
            this.rbCpf.Location = new System.Drawing.Point(7, 5);
            this.rbCpf.Name = "rbCpf";
            this.rbCpf.Size = new System.Drawing.Size(45, 17);
            this.rbCpf.TabIndex = 12;
            this.rbCpf.Text = "CPF";
            this.rbCpf.UseVisualStyleBackColor = true;
            this.rbCpf.CheckedChanged += new System.EventHandler(this.rbNome_CheckedChanged);
            // 
            // rbNome_Acesso
            // 
            this.rbNome_Acesso.AccessibleName = "Localiza";
            this.rbNome_Acesso.AutoSize = true;
            this.rbNome_Acesso.Location = new System.Drawing.Point(661, 14);
            this.rbNome_Acesso.Name = "rbNome_Acesso";
            this.rbNome_Acesso.Size = new System.Drawing.Size(53, 17);
            this.rbNome_Acesso.TabIndex = 13;
            this.rbNome_Acesso.Text = "Nome";
            this.rbNome_Acesso.UseVisualStyleBackColor = true;
            // 
            // rbSenha
            // 
            this.rbSenha.AccessibleName = "Localiza";
            this.rbSenha.AutoSize = true;
            this.rbSenha.Location = new System.Drawing.Point(599, 14);
            this.rbSenha.Name = "rbSenha";
            this.rbSenha.Size = new System.Drawing.Size(56, 17);
            this.rbSenha.TabIndex = 12;
            this.rbSenha.Text = "Senha";
            this.rbSenha.UseVisualStyleBackColor = true;
            this.rbSenha.CheckedChanged += new System.EventHandler(this.rbSenha_CheckedChanged);
            // 
            // lbAcessoAberto
            // 
            this.lbAcessoAberto.AutoSize = true;
            this.lbAcessoAberto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAcessoAberto.Location = new System.Drawing.Point(466, 15);
            this.lbAcessoAberto.Name = "lbAcessoAberto";
            this.lbAcessoAberto.Size = new System.Drawing.Size(114, 16);
            this.lbAcessoAberto.TabIndex = 9;
            this.lbAcessoAberto.Text = "Localizar Acesso:";
            // 
            // cbAcessoAberto
            // 
            this.cbAcessoAberto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbAcessoAberto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.cbAcessoAberto.FormattingEnabled = true;
            this.cbAcessoAberto.Location = new System.Drawing.Point(467, 32);
            this.cbAcessoAberto.MaxLength = 49;
            this.cbAcessoAberto.Name = "cbAcessoAberto";
            this.cbAcessoAberto.Size = new System.Drawing.Size(236, 21);
            this.cbAcessoAberto.TabIndex = 8;
            this.cbAcessoAberto.SelectedIndexChanged += new System.EventHandler(this.cbAcessoAberto_SelectedIndexChanged);
            this.cbAcessoAberto.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbAcessoAberto_MouseClick);
            // 
            // txtLimparCampos
            // 
            this.txtLimparCampos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLimparCampos.BackColor = System.Drawing.Color.Gray;
            this.txtLimparCampos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtLimparCampos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLimparCampos.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtLimparCampos.Location = new System.Drawing.Point(1172, 26);
            this.txtLimparCampos.Name = "txtLimparCampos";
            this.txtLimparCampos.Size = new System.Drawing.Size(131, 27);
            this.txtLimparCampos.TabIndex = 7;
            this.txtLimparCampos.Text = "Limpar Campos";
            this.txtLimparCampos.UseVisualStyleBackColor = false;
            this.txtLimparCampos.Click += new System.EventHandler(this.txtLimparCampos_Click);
            // 
            // txtId
            // 
            this.txtId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtId.Location = new System.Drawing.Point(311, 32);
            this.txtId.Multiline = true;
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(78, 18);
            this.txtId.TabIndex = 6;
            // 
            // gridAcesso
            // 
            this.gridAcesso.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridAcesso.DataMember = null;
            this.gridAcesso.DataSource = this.dsacessos;
            ultraGridColumn1.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
            ultraGridColumn1.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn1.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn1.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn1.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn1.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn2.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn2.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn2.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn2.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn2.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn2.Header.VisiblePosition = 1;
            ultraGridColumn3.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn3.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn3.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn3.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn3.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn3.Header.VisiblePosition = 2;
            ultraGridColumn4.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn4.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn4.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn4.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn4.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn4.Header.VisiblePosition = 3;
            ultraGridColumn5.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn5.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn5.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn5.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn5.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn5.Header.VisiblePosition = 4;
            ultraGridColumn6.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn6.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn6.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn6.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn6.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn6.Header.VisiblePosition = 7;
            ultraGridColumn7.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn7.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn7.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn7.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn7.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn7.Header.VisiblePosition = 8;
            ultraGridColumn8.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn8.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn8.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn8.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn8.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn8.Header.VisiblePosition = 9;
            ultraGridColumn9.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn9.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn9.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn9.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn9.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn9.Header.VisiblePosition = 10;
            ultraGridColumn10.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn10.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn10.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn10.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn10.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn10.Header.VisiblePosition = 11;
            ultraGridColumn11.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn11.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn11.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn11.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn11.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn11.Header.VisiblePosition = 12;
            ultraGridColumn12.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn12.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn12.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn12.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn12.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn12.Header.VisiblePosition = 15;
            ultraGridColumn13.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn13.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn13.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn13.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn13.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn13.Header.VisiblePosition = 16;
            ultraGridColumn14.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn14.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn14.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn14.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn14.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn14.Header.VisiblePosition = 17;
            ultraGridColumn15.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn15.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn15.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn15.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn15.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn15.Header.VisiblePosition = 18;
            ultraGridColumn16.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn16.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn16.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn16.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn16.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn16.Header.VisiblePosition = 19;
            ultraGridColumn17.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn17.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn17.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn17.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn17.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn17.Header.VisiblePosition = 20;
            ultraGridColumn18.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn18.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn18.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn18.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn18.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn18.Header.VisiblePosition = 13;
            ultraGridColumn19.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn19.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn19.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn19.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn19.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn19.Header.VisiblePosition = 14;
            ultraGridColumn20.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn20.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn20.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn20.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn20.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn20.Header.VisiblePosition = 6;
            ultraGridColumn21.FilterClearButtonVisible = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn21.FilterComparisonType = Infragistics.Win.UltraWinGrid.FilterComparisonType.CaseInsensitive;
            ultraGridColumn21.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            ultraGridColumn21.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            ultraGridColumn21.FilterOperatorDropDownItems = Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals;
            ultraGridColumn21.Header.VisiblePosition = 5;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5,
            ultraGridColumn6,
            ultraGridColumn7,
            ultraGridColumn8,
            ultraGridColumn9,
            ultraGridColumn10,
            ultraGridColumn11,
            ultraGridColumn12,
            ultraGridColumn13,
            ultraGridColumn14,
            ultraGridColumn15,
            ultraGridColumn16,
            ultraGridColumn17,
            ultraGridColumn18,
            ultraGridColumn19,
            ultraGridColumn20,
            ultraGridColumn21});
            this.gridAcesso.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.gridAcesso.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            appearance38.TextVAlignAsString = "Top";
            this.gridAcesso.DisplayLayout.Override.FilterCellAppearanceActive = appearance38;
            this.gridAcesso.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.gridAcesso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridAcesso.Location = new System.Drawing.Point(6, 251);
            this.gridAcesso.Name = "gridAcesso";
            this.gridAcesso.Size = new System.Drawing.Size(1297, 281);
            this.gridAcesso.TabIndex = 4;
            this.gridAcesso.Text = "Acessos";
            this.gridAcesso.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.gridAcesso.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.gridAcesso_CellChange);
            this.gridAcesso.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.gridAcesso_ClickCell);
            this.gridAcesso.Click += new System.EventHandler(this.gridAcessosAbertos_Click);
            // 
            // dsacessos
            // 
            ultraDataColumn1.DataType = typeof(bool);
            this.dsacessos.Band.Columns.AddRange(new object[] {
            ultraDataColumn1,
            ultraDataColumn2,
            ultraDataColumn3,
            ultraDataColumn4,
            ultraDataColumn5,
            ultraDataColumn6,
            ultraDataColumn7,
            ultraDataColumn8,
            ultraDataColumn9,
            ultraDataColumn10,
            ultraDataColumn11,
            ultraDataColumn12,
            ultraDataColumn13,
            ultraDataColumn14,
            ultraDataColumn15,
            ultraDataColumn16,
            ultraDataColumn17,
            ultraDataColumn18,
            ultraDataColumn19,
            ultraDataColumn20,
            ultraDataColumn21});
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label46);
            this.panel1.Controls.Add(this.txtCNH);
            this.panel1.Controls.Add(this.dtp_vencimentoCNH);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.cbOperacao);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.cbTipoCaminhao);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.cbusuarioSaida);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.txtcpf);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.cbEmpresaVisitada);
            this.panel1.Controls.Add(this.cbAlteraEmpresa);
            this.panel1.Controls.Add(this.picAnularFinal);
            this.panel1.Controls.Add(this.txtNfSaida);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txtNfEntrada);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtCor);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txtVeiculo);
            this.panel1.Controls.Add(this.txtPlacaCarreta);
            this.panel1.Controls.Add(this.txtPlacaCavalo);
            this.panel1.Controls.Add(this.btnInserir);
            this.panel1.Controls.Add(this.lbSaida);
            this.panel1.Controls.Add(this.dtSaida);
            this.panel1.Controls.Add(this.dtpDataEntrada);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtSenha);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtNome);
            this.panel1.Location = new System.Drawing.Point(9, 59);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1294, 197);
            this.panel1.TabIndex = 2;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(1276, 67);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(19, 25);
            this.label27.TabIndex = 56;
            this.label27.Text = "*";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(1053, 15);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(19, 25);
            this.label26.TabIndex = 55;
            this.label26.Text = "*";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(697, 74);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(19, 25);
            this.label25.TabIndex = 54;
            this.label25.Text = "*";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(697, 16);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(19, 25);
            this.label24.TabIndex = 53;
            this.label24.Text = "*";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(335, 75);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(19, 25);
            this.label23.TabIndex = 52;
            this.label23.Text = "*";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(335, 133);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(19, 25);
            this.label22.TabIndex = 51;
            this.label22.Text = "*";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(23, 172);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(111, 16);
            this.label20.TabIndex = 50;
            this.label20.Text = ": Campos obrigatórios";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(11, 172);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(19, 25);
            this.label21.TabIndex = 49;
            this.label21.Text = "*";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(80, 80);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(37, 16);
            this.label46.TabIndex = 47;
            this.label46.Text = "CNH";
            // 
            // txtCNH
            // 
            this.txtCNH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCNH.Location = new System.Drawing.Point(131, 75);
            this.txtCNH.MaxLength = 19;
            this.txtCNH.Multiline = true;
            this.txtCNH.Name = "txtCNH";
            this.txtCNH.Size = new System.Drawing.Size(198, 23);
            this.txtCNH.TabIndex = 45;
            this.txtCNH.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtVeiculo_KeyUp);
            // 
            // dtp_vencimentoCNH
            // 
            this.dtp_vencimentoCNH.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_vencimentoCNH.Location = new System.Drawing.Point(131, 103);
            this.dtp_vencimentoCNH.Name = "dtp_vencimentoCNH";
            this.dtp_vencimentoCNH.Size = new System.Drawing.Size(198, 20);
            this.dtp_vencimentoCNH.TabIndex = 44;
            this.dtp_vencimentoCNH.ValueChanged += new System.EventHandler(this.dtp_vencimentoCNH_ValueChanged);
            this.dtp_vencimentoCNH.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtVeiculo_KeyUp);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(7, 106);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(111, 16);
            this.label19.TabIndex = 43;
            this.label19.Text = "Vencimento CNH";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(1116, 50);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(69, 16);
            this.label18.TabIndex = 41;
            this.label18.Text = "Operação";
            // 
            // cbOperacao
            // 
            this.cbOperacao.FormattingEnabled = true;
            this.cbOperacao.Location = new System.Drawing.Point(1119, 68);
            this.cbOperacao.Name = "cbOperacao";
            this.cbOperacao.Size = new System.Drawing.Size(154, 21);
            this.cbOperacao.TabIndex = 40;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(372, 133);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(117, 16);
            this.label17.TabIndex = 39;
            this.label17.Text = "Tipo de caminhão";
            // 
            // cbTipoCaminhao
            // 
            this.cbTipoCaminhao.FormattingEnabled = true;
            this.cbTipoCaminhao.Location = new System.Drawing.Point(495, 129);
            this.cbTipoCaminhao.Name = "cbTipoCaminhao";
            this.cbTipoCaminhao.Size = new System.Drawing.Size(198, 21);
            this.cbTipoCaminhao.TabIndex = 38;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(1116, 10);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 16);
            this.label16.TabIndex = 37;
            this.label16.Text = "Usuário saída";
            // 
            // cbusuarioSaida
            // 
            this.cbusuarioSaida.FormattingEnabled = true;
            this.cbusuarioSaida.Location = new System.Drawing.Point(1119, 26);
            this.cbusuarioSaida.Name = "cbusuarioSaida";
            this.cbusuarioSaida.Size = new System.Drawing.Size(154, 21);
            this.cbusuarioSaida.TabIndex = 36;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(84, 49);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 16);
            this.label15.TabIndex = 35;
            this.label15.Text = "CPF";
            // 
            // txtcpf
            // 
            this.txtcpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcpf.Location = new System.Drawing.Point(131, 47);
            this.txtcpf.Multiline = true;
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.ReadOnly = true;
            this.txtcpf.Size = new System.Drawing.Size(234, 23);
            this.txtcpf.TabIndex = 34;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(726, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 16);
            this.label10.TabIndex = 33;
            this.label10.Text = "Empresa Visitada";
            // 
            // cbEmpresaVisitada
            // 
            this.cbEmpresaVisitada.FormattingEnabled = true;
            this.cbEmpresaVisitada.Location = new System.Drawing.Point(854, 17);
            this.cbEmpresaVisitada.Name = "cbEmpresaVisitada";
            this.cbEmpresaVisitada.Size = new System.Drawing.Size(198, 21);
            this.cbEmpresaVisitada.TabIndex = 32;
            this.cbEmpresaVisitada.SelectedIndexChanged += new System.EventHandler(this.dtp_vencimentoCNH_ValueChanged);
            this.cbEmpresaVisitada.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbEmpresaVisitada_MouseClick);
            // 
            // cbAlteraEmpresa
            // 
            this.cbAlteraEmpresa.FormattingEnabled = true;
            this.cbAlteraEmpresa.Location = new System.Drawing.Point(131, 129);
            this.cbAlteraEmpresa.Name = "cbAlteraEmpresa";
            this.cbAlteraEmpresa.Size = new System.Drawing.Size(198, 21);
            this.cbAlteraEmpresa.TabIndex = 31;
            this.cbAlteraEmpresa.SelectedIndexChanged += new System.EventHandler(this.dtp_vencimentoCNH_ValueChanged);
            this.cbAlteraEmpresa.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbAlteraEmpresa_MouseClick);
            // 
            // picAnularFinal
            // 
            this.picAnularFinal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picAnularFinal.Image = global::WindowsFormsApplication1.Properties.Resources.block_16;
            this.picAnularFinal.Location = new System.Drawing.Point(1058, 128);
            this.picAnularFinal.Name = "picAnularFinal";
            this.picAnularFinal.Size = new System.Drawing.Size(23, 26);
            this.picAnularFinal.TabIndex = 28;
            this.picAnularFinal.TabStop = false;
            this.picAnularFinal.Visible = false;
            this.picAnularFinal.Click += new System.EventHandler(this.picAnularFinal_Click);
            // 
            // txtNfSaida
            // 
            this.txtNfSaida.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNfSaida.Location = new System.Drawing.Point(854, 73);
            this.txtNfSaida.MaxLength = 50;
            this.txtNfSaida.Multiline = true;
            this.txtNfSaida.Name = "txtNfSaida";
            this.txtNfSaida.Size = new System.Drawing.Size(198, 23);
            this.txtNfSaida.TabIndex = 6;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(776, 77);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 16);
            this.label14.TabIndex = 27;
            this.label14.Text = "NF Saída";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(765, 49);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 16);
            this.label13.TabIndex = 26;
            this.label13.Text = "NF Entrada";
            // 
            // txtNfEntrada
            // 
            this.txtNfEntrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNfEntrada.Location = new System.Drawing.Point(854, 43);
            this.txtNfEntrada.MaxLength = 50;
            this.txtNfEntrada.Multiline = true;
            this.txtNfEntrada.Name = "txtNfEntrada";
            this.txtNfEntrada.Size = new System.Drawing.Size(198, 23);
            this.txtNfEntrada.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(460, 48);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 16);
            this.label12.TabIndex = 24;
            this.label12.Text = "Cor";
            // 
            // txtCor
            // 
            this.txtCor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCor.Location = new System.Drawing.Point(495, 44);
            this.txtCor.MaxLength = 50;
            this.txtCor.Multiline = true;
            this.txtCor.Name = "txtCor";
            this.txtCor.Size = new System.Drawing.Size(198, 23);
            this.txtCor.TabIndex = 2;
            this.txtCor.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtVeiculo_KeyUp);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(438, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 16);
            this.label11.TabIndex = 22;
            this.label11.Text = "Veículo";
            // 
            // txtVeiculo
            // 
            this.txtVeiculo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVeiculo.Location = new System.Drawing.Point(495, 15);
            this.txtVeiculo.MaxLength = 50;
            this.txtVeiculo.Multiline = true;
            this.txtVeiculo.Name = "txtVeiculo";
            this.txtVeiculo.Size = new System.Drawing.Size(198, 23);
            this.txtVeiculo.TabIndex = 1;
            this.txtVeiculo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtVeiculo_KeyUp);
            // 
            // txtPlacaCarreta
            // 
            this.txtPlacaCarreta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlacaCarreta.Location = new System.Drawing.Point(495, 101);
            this.txtPlacaCarreta.MaxLength = 20;
            this.txtPlacaCarreta.Multiline = true;
            this.txtPlacaCarreta.Name = "txtPlacaCarreta";
            this.txtPlacaCarreta.Size = new System.Drawing.Size(198, 23);
            this.txtPlacaCarreta.TabIndex = 4;
            this.txtPlacaCarreta.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtVeiculo_KeyUp);
            // 
            // txtPlacaCavalo
            // 
            this.txtPlacaCavalo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlacaCavalo.Location = new System.Drawing.Point(495, 73);
            this.txtPlacaCavalo.MaxLength = 20;
            this.txtPlacaCavalo.Multiline = true;
            this.txtPlacaCavalo.Name = "txtPlacaCavalo";
            this.txtPlacaCavalo.Size = new System.Drawing.Size(198, 23);
            this.txtPlacaCavalo.TabIndex = 3;
            this.txtPlacaCavalo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtVeiculo_KeyUp);
            // 
            // btnInserir
            // 
            this.btnInserir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInserir.BackColor = System.Drawing.SystemColors.GrayText;
            this.btnInserir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInserir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInserir.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnInserir.Image = global::WindowsFormsApplication1.Properties.Resources.Tick;
            this.btnInserir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInserir.Location = new System.Drawing.Point(1190, 155);
            this.btnInserir.Name = "btnInserir";
            this.btnInserir.Size = new System.Drawing.Size(96, 31);
            this.btnInserir.TabIndex = 16;
            this.btnInserir.Text = "Inserir";
            this.btnInserir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInserir.UseVisualStyleBackColor = false;
            this.btnInserir.Click += new System.EventHandler(this.btnInserir_Click);
            // 
            // lbSaida
            // 
            this.lbSaida.AutoSize = true;
            this.lbSaida.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSaida.Location = new System.Drawing.Point(744, 132);
            this.lbSaida.Name = "lbSaida";
            this.lbSaida.Size = new System.Drawing.Size(97, 16);
            this.lbSaida.TabIndex = 15;
            this.lbSaida.Text = "Dta/hora saída";
            this.lbSaida.Visible = false;
            // 
            // dtSaida
            // 
            this.dtSaida.Enabled = false;
            this.dtSaida.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtSaida.Location = new System.Drawing.Point(854, 128);
            this.dtSaida.Name = "dtSaida";
            this.dtSaida.Size = new System.Drawing.Size(198, 22);
            this.dtSaida.TabIndex = 9;
            this.dtSaida.Visible = false;
            // 
            // dtpDataEntrada
            // 
            this.dtpDataEntrada.Enabled = false;
            this.dtpDataEntrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDataEntrada.Location = new System.Drawing.Point(854, 101);
            this.dtpDataEntrada.Name = "dtpDataEntrada";
            this.dtpDataEntrada.Size = new System.Drawing.Size(198, 22);
            this.dtpDataEntrada.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(729, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 16);
            this.label7.TabIndex = 10;
            this.label7.Text = "Dta/Hora entrada";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(382, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 16);
            this.label6.TabIndex = 9;
            this.label6.Text = "Placa do carreta";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(446, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 16);
            this.label5.TabIndex = 7;
            this.label5.Text = "Placa";
            // 
            // txtSenha
            // 
            this.txtSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSenha.Location = new System.Drawing.Point(1119, 111);
            this.txtSenha.MaxLength = 10;
            this.txtSenha.Multiline = true;
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(53, 23);
            this.txtSenha.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1116, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Senha";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(54, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Empresa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(73, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nome";
            // 
            // txtNome
            // 
            this.txtNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.Location = new System.Drawing.Point(131, 18);
            this.txtNome.Multiline = true;
            this.txtNome.Name = "txtNome";
            this.txtNome.ReadOnly = true;
            this.txtNome.Size = new System.Drawing.Size(234, 23);
            this.txtNome.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Informe a busca";
            // 
            // cbCPF
            // 
            this.cbCPF.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbCPF.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.cbCPF.FormattingEnabled = true;
            this.cbCPF.Location = new System.Drawing.Point(9, 32);
            this.cbCPF.MaxLength = 49;
            this.cbCPF.Name = "cbCPF";
            this.cbCPF.Size = new System.Drawing.Size(289, 21);
            this.cbCPF.TabIndex = 0;
            this.cbCPF.SelectedIndexChanged += new System.EventHandler(this.btnCarregar_Click);
            this.cbCPF.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cbCPF_KeyUp);
            this.cbCPF.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbCPF_MouseClick);
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.panel2);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1306, 754);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.cbTurno);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.cbUsuario);
            this.panel2.Location = new System.Drawing.Point(297, 39);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(285, 73);
            this.panel2.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(7, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 16);
            this.label8.TabIndex = 19;
            this.label8.Text = "Turno";
            // 
            // cbTurno
            // 
            this.cbTurno.FormattingEnabled = true;
            this.cbTurno.Location = new System.Drawing.Point(65, 39);
            this.cbTurno.Name = "cbTurno";
            this.cbTurno.Size = new System.Drawing.Size(211, 21);
            this.cbTurno.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(7, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 16);
            this.label9.TabIndex = 1;
            this.label9.Text = "Usuário";
            // 
            // cbUsuario
            // 
            this.cbUsuario.FormattingEnabled = true;
            this.cbUsuario.Location = new System.Drawing.Point(65, 8);
            this.cbUsuario.Name = "cbUsuario";
            this.cbUsuario.Size = new System.Drawing.Size(211, 21);
            this.cbUsuario.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = global::WindowsFormsApplication1.Properties.Resources.proteg1;
            this.pictureBox1.Location = new System.Drawing.Point(27, 39);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(118, 73);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // dsAcessosAbertos
            // 
            this.dsAcessosAbertos.Band.Columns.AddRange(new object[] {
            ultraDataColumn22,
            ultraDataColumn23,
            ultraDataColumn24,
            ultraDataColumn25,
            ultraDataColumn26,
            ultraDataColumn27,
            ultraDataColumn28,
            ultraDataColumn29,
            ultraDataColumn30});
            // 
            // Fgeral_Fill_Panel
            // 
            // 
            // Fgeral_Fill_Panel.ClientArea
            // 
            this.Fgeral_Fill_Panel.ClientArea.Controls.Add(this.ultraTabControl1);
            this.Fgeral_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default;
            this.Fgeral_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Fgeral_Fill_Panel.Location = new System.Drawing.Point(8, 158);
            this.Fgeral_Fill_Panel.Name = "Fgeral_Fill_Panel";
            this.Fgeral_Fill_Panel.Size = new System.Drawing.Size(1338, 567);
            this.Fgeral_Fill_Panel.TabIndex = 0;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Location = new System.Drawing.Point(6, 6);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(1310, 780);
            this.ultraTabControl1.TabIndex = 0;
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Acessos";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "tab1";
            ultraTab1.Visible = false;
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab2,
            ultraTab1});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1306, 754);
            // 
            // _Fgeral_Toolbars_Dock_Area_Left
            // 
            this._Fgeral_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Fgeral_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.Transparent;
            this._Fgeral_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
            this._Fgeral_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.Color.White;
            this._Fgeral_Toolbars_Dock_Area_Left.InitialResizeAreaExtent = 8;
            this._Fgeral_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 158);
            this._Fgeral_Toolbars_Dock_Area_Left.Name = "_Fgeral_Toolbars_Dock_Area_Left";
            this._Fgeral_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(8, 567);
            this._Fgeral_Toolbars_Dock_Area_Left.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // ultraToolbarsManager1
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.BackColor2 = System.Drawing.Color.White;
            appearance17.ForeColor = System.Drawing.Color.White;
            this.ultraToolbarsManager1.Appearance = appearance17;
            this.ultraToolbarsManager1.DesignerFlags = 0;
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.White;
            appearance1.BackColorDisabled = System.Drawing.Color.White;
            appearance1.BackColorDisabled2 = System.Drawing.Color.White;
            appearance1.BackHatchStyle = Infragistics.Win.BackHatchStyle.ForwardDiagonal;
            appearance1.BorderColor = System.Drawing.Color.White;
            appearance1.BorderColor2 = System.Drawing.Color.White;
            appearance1.Image = global::WindowsFormsApplication1.Properties.Resources.Sem_título2;
            appearance1.ImageBackgroundStretchMargins = new Infragistics.Win.ImageBackgroundStretchMargins(5, 60, 0, 0);
            this.ultraToolbarsManager1.DockAreaAppearance = appearance1;
            this.ultraToolbarsManager1.DockWithinContainer = this;
            this.ultraToolbarsManager1.DockWithinContainerBaseType = typeof(System.Windows.Forms.Form);
            this.ultraToolbarsManager1.ImageTransparentColor = System.Drawing.Color.White;
            ribbonTab1.Caption = "Módulos";
            ribbonGroup1.Caption = "Módulos";
            controlContainerTool1.ControlName = "pictureBox1";
            controlContainerTool1.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            controlContainerTool1.InstanceProps.Width = 118;
            buttonTool1.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool32.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool3.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            controlContainerTool1,
            buttonTool1,
            buttonTool32,
            buttonTool3});
            ribbonGroup2.Caption = "Ferramentas";
            buttonTool22.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool5.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool10.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool8.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool24.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            controlContainerTool3.ControlName = "panel2";
            controlContainerTool3.InstanceProps.Width = 285;
            buttonTool14.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup2.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool22,
            buttonTool5,
            buttonTool10,
            buttonTool8,
            buttonTool24,
            controlContainerTool3,
            buttonTool14,
            buttonTool17,
            buttonTool20});
            ribbonGroup3.Caption = "";
            buttonTool26.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool12.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup3.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool26,
            buttonTool12});
            ribbonTab1.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup1,
            ribbonGroup2,
            ribbonGroup3});
            this.ultraToolbarsManager1.Ribbon.NonInheritedRibbonTabs.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonTab[] {
            ribbonTab1});
            this.ultraToolbarsManager1.Ribbon.Visible = true;
            appearance11.Image = global::WindowsFormsApplication1.Properties.Resources.folder_32;
            buttonTool2.SharedPropsInternal.AppearancesSmall.Appearance = appearance11;
            buttonTool2.SharedPropsInternal.Caption = "Cadastro";
            appearance13.Image = global::WindowsFormsApplication1.Properties.Resources.Sem_título2;
            labelTool2.SharedPropsInternal.AppearancesLarge.Appearance = appearance13;
            appearance12.Image = global::WindowsFormsApplication1.Properties.Resources.Sem_título2;
            labelTool2.SharedPropsInternal.AppearancesSmall.Appearance = appearance12;
            labelTool2.SharedPropsInternal.MaxWidth = 10;
            appearance14.Image = global::WindowsFormsApplication1.Properties.Resources.Sem_título2;
            labelTool3.SharedPropsInternal.AppearancesLarge.Appearance = appearance14;
            labelTool3.SharedPropsInternal.MaxWidth = 10;
            labelTool3.SharedPropsInternal.MinWidth = 10;
            labelTool3.SharedPropsInternal.Width = 10;
            appearance15.Image = ((object)(resources.GetObject("appearance15.Image")));
            labelTool5.SharedPropsInternal.AppearancesSmall.Appearance = appearance15;
            appearance18.Image = global::WindowsFormsApplication1.Properties.Resources.Align_Compact;
            buttonTool4.SharedPropsInternal.AppearancesSmall.Appearance = appearance18;
            buttonTool4.SharedPropsInternal.Caption = "Histórico";
            appearance20.Image = global::WindowsFormsApplication1.Properties.Resources.Client_Account_Template;
            buttonTool6.SharedPropsInternal.AppearancesSmall.Appearance = appearance20;
            buttonTool6.SharedPropsInternal.Caption = "Gerenciador";
            controlContainerTool2.ControlName = "pictureBox1";
            controlContainerTool2.SharedPropsInternal.Width = 118;
            appearance19.Image = global::WindowsFormsApplication1.Properties.Resources.save_32;
            buttonTool7.SharedPropsInternal.AppearancesSmall.Appearance = appearance19;
            buttonTool7.SharedPropsInternal.Caption = "Salvar";
            appearance21.Image = global::WindowsFormsApplication1.Properties.Resources.Table_Excel;
            buttonTool9.SharedPropsInternal.AppearancesSmall.Appearance = appearance21;
            buttonTool9.SharedPropsInternal.Caption = "Exportar";
            appearance22.Image = global::WindowsFormsApplication1.Properties.Resources.delete_32;
            buttonTool11.SharedPropsInternal.AppearancesSmall.Appearance = appearance22;
            buttonTool11.SharedPropsInternal.Caption = "Deletar";
            controlContainerTool4.ControlName = "panel2";
            controlContainerTool4.SharedPropsInternal.Width = 285;
            appearance24.Image = global::WindowsFormsApplication1.Properties.Resources.user_322;
            buttonTool15.SharedPropsInternal.AppearancesSmall.Appearance = appearance24;
            buttonTool15.SharedPropsInternal.Caption = "Novo Usuário";
            appearance26.Image = global::WindowsFormsApplication1.Properties.Resources.trash_32;
            buttonTool18.SharedPropsInternal.AppearancesSmall.Appearance = appearance26;
            buttonTool18.SharedPropsInternal.Caption = "Desmarcar Todos";
            appearance25.Image = global::WindowsFormsApplication1.Properties.Resources.tick_322;
            buttonTool19.SharedPropsInternal.AppearancesSmall.Appearance = appearance25;
            buttonTool19.SharedPropsInternal.Caption = "Marcar Todos";
            appearance27.Image = global::WindowsFormsApplication1.Properties.Resources.trash_32;
            buttonTool21.SharedPropsInternal.AppearancesSmall.Appearance = appearance27;
            buttonTool21.SharedPropsInternal.Caption = "Desmarcar Todos";
            appearance28.Image = global::WindowsFormsApplication1.Properties.Resources.Contact_Email;
            buttonTool16.SharedPropsInternal.AppearancesSmall.Appearance = appearance28;
            buttonTool16.SharedPropsInternal.Caption = "Contato Desenvolvedor";
            appearance29.Image = global::WindowsFormsApplication1.Properties.Resources.Atualizar_Large;
            buttonTool23.SharedPropsInternal.AppearancesSmall.Appearance = appearance29;
            buttonTool23.SharedPropsInternal.Caption = "Atualizar";
            appearance30.Image = global::WindowsFormsApplication1.Properties.Resources.Factory;
            buttonTool25.SharedPropsInternal.AppearancesSmall.Appearance = appearance30;
            buttonTool25.SharedPropsInternal.Caption = "Nova Empresa";
            appearance31.Image = global::WindowsFormsApplication1.Properties.Resources.Backup_Manager;
            buttonTool27.SharedPropsInternal.AppearancesSmall.Appearance = appearance31;
            buttonTool27.SharedPropsInternal.Caption = "Salvar Backup";
            appearance32.Image = global::WindowsFormsApplication1.Properties.Resources.Backup_Manager;
            buttonTool29.SharedPropsInternal.AppearancesSmall.Appearance = appearance32;
            buttonTool29.SharedPropsInternal.Caption = "Salvar Backup Banco";
            appearance33.Image = global::WindowsFormsApplication1.Properties.Resources.Backup_Manager;
            buttonTool31.SharedPropsInternal.AppearancesSmall.Appearance = appearance33;
            buttonTool31.SharedPropsInternal.Caption = "Backup Banco de Dados";
            appearance34.Image = global::WindowsFormsApplication1.Properties.Resources.Backup_Manager1;
            buttonTool28.SharedPropsInternal.AppearancesSmall.Appearance = appearance34;
            buttonTool28.SharedPropsInternal.Caption = "Novo Backup";
            appearance35.Image = ((object)(resources.GetObject("appearance35.Image")));
            buttonTool30.SharedPropsInternal.AppearancesSmall.Appearance = appearance35;
            buttonTool30.SharedPropsInternal.Caption = "Imprimir";
            appearance36.Image = global::WindowsFormsApplication1.Properties.Resources.Client_Account_Template;
            buttonTool33.SharedPropsInternal.AppearancesLarge.Appearance = appearance36;
            buttonTool33.SharedPropsInternal.Caption = "Colaboradores";
            this.ultraToolbarsManager1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool2,
            labelTool2,
            labelTool3,
            labelTool5,
            buttonTool4,
            buttonTool6,
            controlContainerTool2,
            buttonTool7,
            buttonTool9,
            buttonTool11,
            buttonTool13,
            controlContainerTool4,
            buttonTool15,
            buttonTool18,
            buttonTool19,
            buttonTool21,
            buttonTool16,
            buttonTool23,
            buttonTool25,
            buttonTool27,
            buttonTool29,
            buttonTool31,
            buttonTool28,
            buttonTool30,
            buttonTool33});
            this.ultraToolbarsManager1.ToolClick += new Infragistics.Win.UltraWinToolbars.ToolClickEventHandler(this.ultraToolbarsManager1_ToolClick);
            // 
            // _Fgeral_Toolbars_Dock_Area_Right
            // 
            this._Fgeral_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Fgeral_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.Transparent;
            this._Fgeral_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
            this._Fgeral_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.Color.White;
            this._Fgeral_Toolbars_Dock_Area_Right.InitialResizeAreaExtent = 8;
            this._Fgeral_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(1346, 158);
            this._Fgeral_Toolbars_Dock_Area_Right.Name = "_Fgeral_Toolbars_Dock_Area_Right";
            this._Fgeral_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(8, 567);
            this._Fgeral_Toolbars_Dock_Area_Right.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _Fgeral_Toolbars_Dock_Area_Top
            // 
            this._Fgeral_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Fgeral_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.Transparent;
            this._Fgeral_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
            this._Fgeral_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.Color.White;
            this._Fgeral_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._Fgeral_Toolbars_Dock_Area_Top.Name = "_Fgeral_Toolbars_Dock_Area_Top";
            this._Fgeral_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(1354, 158);
            this._Fgeral_Toolbars_Dock_Area_Top.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _Fgeral_Toolbars_Dock_Area_Bottom
            // 
            this._Fgeral_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Fgeral_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.Transparent;
            this._Fgeral_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
            this._Fgeral_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.Color.White;
            this._Fgeral_Toolbars_Dock_Area_Bottom.InitialResizeAreaExtent = 8;
            this._Fgeral_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 725);
            this._Fgeral_Toolbars_Dock_Area_Bottom.Name = "_Fgeral_Toolbars_Dock_Area_Bottom";
            this._Fgeral_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(1354, 8);
            this._Fgeral_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // timerBKP
            // 
            this.timerBKP.Enabled = true;
            this.timerBKP.Interval = 1000;
            this.timerBKP.Tick += new System.EventHandler(this.timerBKP_Tick);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // Fgeral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1354, 733);
            this.Controls.Add(this.Fgeral_Fill_Panel);
            this.Controls.Add(this._Fgeral_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._Fgeral_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._Fgeral_Toolbars_Dock_Area_Top);
            this.Controls.Add(this._Fgeral_Toolbars_Dock_Area_Bottom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Fgeral";
            this.Text = "Controle de Acesso";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ultraTabPageControl2.ResumeLayout(false);
            this.ultraTabPageControl2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAcesso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsacessos)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAnularFinal)).EndInit();
            this.ultraTabPageControl1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsAcessosAbertos)).EndInit();
            this.Fgeral_Fill_Panel.ClientArea.ResumeLayout(false);
            this.Fgeral_Fill_Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinToolbars.UltraToolbarsManager ultraToolbarsManager1;
        private Infragistics.Win.Misc.UltraPanel Fgeral_Fill_Panel;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Fgeral_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Fgeral_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Fgeral_Toolbars_Dock_Area_Top;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Fgeral_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbCPF;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.DateTimePicker dtpDataEntrada;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource dsAcessosAbertos;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbUsuario;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label lbSaida;
        private System.Windows.Forms.DateTimePicker dtSaida;
        private System.Windows.Forms.Button btnInserir;
        private Infragistics.Win.UltraWinGrid.UltraGrid gridAcesso;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource dsacessos;
        private System.Windows.Forms.TextBox txtPlacaCarreta;
        private System.Windows.Forms.TextBox txtPlacaCavalo;
        private System.Windows.Forms.TextBox txtNfSaida;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtNfEntrada;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCor;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtVeiculo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbTurno;
        private System.Windows.Forms.Button txtLimparCampos;
        private System.Windows.Forms.PictureBox picAnularFinal;
        private System.Windows.Forms.Timer timerBKP;
        internal System.Windows.Forms.Label lbAcessoAberto;
        private System.Windows.Forms.ComboBox cbAcessoAberto;
        private System.Windows.Forms.ComboBox cbAlteraEmpresa;
        private System.Windows.Forms.RadioButton rbNome_Acesso;
        private System.Windows.Forms.RadioButton rbSenha;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton rbNome;
        private System.Windows.Forms.RadioButton rbCpf;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtcpf;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbEmpresaVisitada;
        private System.Windows.Forms.RadioButton rbPlaca;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbusuarioSaida;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cbTipoCaminhao;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbOperacao;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.DateTimePicker dtp_vencimentoCNH;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txtCNH;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Label lbMotivo;
        private Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter excelCielo;
    }
}

