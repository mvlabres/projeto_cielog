﻿namespace WindowsFormsApplication1
{
    partial class colaborador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn25 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("X");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn26 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("id");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn27 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("idColaborador");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn28 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Matrícula");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn29 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Nome");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn30 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CPF");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn31 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Empresa");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn32 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Placa");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn33 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Veículo");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn34 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Data/hora entrada");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn35 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Data/hora saída");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn36 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Usuário");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn1 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("X");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn2 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("id");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn3 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("idColaborador");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn4 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Matrícula");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn5 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Nome");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn6 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CPF");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn7 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Empresa");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn8 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Placa");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn9 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Veículo");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn10 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Data/hora entrada");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn11 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Data/hora saída");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn12 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Usuário");
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab1 = new Infragistics.Win.UltraWinToolbars.RibbonTab("modulos");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup1 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("Funções");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool3 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Cadastro");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup2 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ferramentas");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool6 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Salvar");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool9 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Excluir");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool11 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exportar");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool1 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Limpar Campos");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool7 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool1");
            Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar1 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("UltraToolbarColaborador");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool2 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ButtonTool1");
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool4 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Cadastro");
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool5 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ButtonTool2");
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool2 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool1");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool4 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool2");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool6 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool3");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool7 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Salvar");
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool8 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Limpar Campos");
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool10 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Excluir");
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool12 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exportar");
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridAcessoColaborador = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dsAcessoColaborador = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbIdColaborador = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbIdAcesso = new System.Windows.Forms.Label();
            this.txtEmpresa = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lbDtaSaida = new System.Windows.Forms.Label();
            this.dtDataHoraSaida = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.dtDataHoraEntrada = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPlaca = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtVeiculo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCpf = new System.Windows.Forms.TextBox();
            this.txtMatricula = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbNome = new System.Windows.Forms.RadioButton();
            this.rbMatricula = new System.Windows.Forms.RadioButton();
            this.btnInserir = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.cbMatricula = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.pnBuscaAcessos = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbFechados = new System.Windows.Forms.RadioButton();
            this.rbAbertos = new System.Windows.Forms.RadioButton();
            this.colaborador_Fill_Panel = new Infragistics.Win.Misc.UltraPanel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this._colaborador_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._colaborador_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._colaborador_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._colaborador_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.excelColaborador = new Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter(this.components);
            this.ultraToolbarsManager1 = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAcessoColaborador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsAcessoColaborador)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            this.pnBuscaAcessos.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.colaborador_Fill_Panel.ClientArea.SuspendLayout();
            this.colaborador_Fill_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.gridAcessoColaborador);
            this.ultraTabPageControl1.Controls.Add(this.panel1);
            this.ultraTabPageControl1.Controls.Add(this.panel2);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1290, 451);
            // 
            // gridAcessoColaborador
            // 
            this.gridAcessoColaborador.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridAcessoColaborador.DataSource = this.dsAcessoColaborador;
            ultraGridColumn25.Header.VisiblePosition = 0;
            ultraGridColumn25.Width = 32;
            ultraGridColumn26.Header.VisiblePosition = 10;
            ultraGridColumn26.Hidden = true;
            ultraGridColumn27.Header.VisiblePosition = 11;
            ultraGridColumn27.Hidden = true;
            ultraGridColumn28.Header.VisiblePosition = 1;
            ultraGridColumn28.Width = 115;
            ultraGridColumn29.Header.VisiblePosition = 3;
            ultraGridColumn29.Width = 286;
            ultraGridColumn30.Header.VisiblePosition = 2;
            ultraGridColumn30.Width = 121;
            ultraGridColumn31.Header.VisiblePosition = 4;
            ultraGridColumn31.Width = 165;
            ultraGridColumn32.Header.VisiblePosition = 6;
            ultraGridColumn32.Width = 136;
            ultraGridColumn33.Header.VisiblePosition = 5;
            ultraGridColumn33.Width = 125;
            ultraGridColumn34.Header.VisiblePosition = 7;
            ultraGridColumn35.Header.VisiblePosition = 8;
            ultraGridColumn35.Width = 104;
            ultraGridColumn36.Header.VisiblePosition = 9;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn25,
            ultraGridColumn26,
            ultraGridColumn27,
            ultraGridColumn28,
            ultraGridColumn29,
            ultraGridColumn30,
            ultraGridColumn31,
            ultraGridColumn32,
            ultraGridColumn33,
            ultraGridColumn34,
            ultraGridColumn35,
            ultraGridColumn36});
            this.gridAcessoColaborador.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.gridAcessoColaborador.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.Yes;
            this.gridAcessoColaborador.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.WithinGroup;
            this.gridAcessoColaborador.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridAcessoColaborador.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.gridAcessoColaborador.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.gridAcessoColaborador.DisplayLayout.Override.FilterEvaluationTrigger = Infragistics.Win.UltraWinGrid.FilterEvaluationTrigger.OnCellValueChange;
            this.gridAcessoColaborador.DisplayLayout.Override.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.Combo;
            this.gridAcessoColaborador.DisplayLayout.Override.RowFilterAction = Infragistics.Win.UltraWinGrid.RowFilterAction.AppearancesOnly;
            this.gridAcessoColaborador.DisplayLayout.Override.RowFilterMode = Infragistics.Win.UltraWinGrid.RowFilterMode.AllRowsInBand;
            this.gridAcessoColaborador.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.gridAcessoColaborador.ExitEditModeOnLeave = false;
            this.gridAcessoColaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridAcessoColaborador.Location = new System.Drawing.Point(3, 220);
            this.gridAcessoColaborador.Name = "gridAcessoColaborador";
            this.gridAcessoColaborador.Size = new System.Drawing.Size(1284, 228);
            this.gridAcessoColaborador.TabIndex = 7;
            this.gridAcessoColaborador.Text = "Cadastro";
            this.gridAcessoColaborador.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.gridAcessoColaborador_CellChange);
            this.gridAcessoColaborador.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.gridAcessoColaborador_ClickCell);
            // 
            // dsAcessoColaborador
            // 
            ultraDataColumn1.DataType = typeof(bool);
            ultraDataColumn2.DataType = typeof(short);
            ultraDataColumn4.ReadOnly = Infragistics.Win.DefaultableBoolean.False;
            ultraDataColumn5.ReadOnly = Infragistics.Win.DefaultableBoolean.False;
            ultraDataColumn6.ReadOnly = Infragistics.Win.DefaultableBoolean.False;
            ultraDataColumn7.ReadOnly = Infragistics.Win.DefaultableBoolean.False;
            ultraDataColumn8.ReadOnly = Infragistics.Win.DefaultableBoolean.False;
            ultraDataColumn9.ReadOnly = Infragistics.Win.DefaultableBoolean.False;
            this.dsAcessoColaborador.Band.Columns.AddRange(new object[] {
            ultraDataColumn1,
            ultraDataColumn2,
            ultraDataColumn3,
            ultraDataColumn4,
            ultraDataColumn5,
            ultraDataColumn6,
            ultraDataColumn7,
            ultraDataColumn8,
            ultraDataColumn9,
            ultraDataColumn10,
            ultraDataColumn11,
            ultraDataColumn12});
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbIdColaborador);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lbIdAcesso);
            this.panel1.Controls.Add(this.txtEmpresa);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.lbDtaSaida);
            this.panel1.Controls.Add(this.dtDataHoraSaida);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.dtDataHoraEntrada);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtPlaca);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtVeiculo);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtCpf);
            this.panel1.Controls.Add(this.txtMatricula);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtNome);
            this.panel1.Location = new System.Drawing.Point(3, 98);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1302, 119);
            this.panel1.TabIndex = 4;
            // 
            // lbIdColaborador
            // 
            this.lbIdColaborador.AutoSize = true;
            this.lbIdColaborador.Location = new System.Drawing.Point(203, 6);
            this.lbIdColaborador.Name = "lbIdColaborador";
            this.lbIdColaborador.Size = new System.Drawing.Size(72, 13);
            this.lbIdColaborador.TabIndex = 24;
            this.lbIdColaborador.Text = "idColaborador";
            this.lbIdColaborador.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(888, 31);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(11, 13);
            this.label15.TabIndex = 23;
            this.label15.Text = "*";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(539, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(11, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "*";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(262, 83);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(11, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "*";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(262, 58);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(11, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(262, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "*";
            // 
            // lbIdAcesso
            // 
            this.lbIdAcesso.AutoSize = true;
            this.lbIdAcesso.Location = new System.Drawing.Point(142, 6);
            this.lbIdAcesso.Name = "lbIdAcesso";
            this.lbIdAcesso.Size = new System.Drawing.Size(50, 13);
            this.lbIdAcesso.TabIndex = 18;
            this.lbIdAcesso.Text = "idAcesso";
            this.lbIdAcesso.Visible = false;
            // 
            // txtEmpresa
            // 
            this.txtEmpresa.Enabled = false;
            this.txtEmpresa.Location = new System.Drawing.Point(391, 28);
            this.txtEmpresa.Name = "txtEmpresa";
            this.txtEmpresa.Size = new System.Drawing.Size(142, 20);
            this.txtEmpresa.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(131, 15);
            this.label9.TabIndex = 16;
            this.label9.Text = "Dados Colaborador";
            // 
            // lbDtaSaida
            // 
            this.lbDtaSaida.AutoSize = true;
            this.lbDtaSaida.Location = new System.Drawing.Point(587, 58);
            this.lbDtaSaida.Name = "lbDtaSaida";
            this.lbDtaSaida.Size = new System.Drawing.Size(101, 13);
            this.lbDtaSaida.TabIndex = 15;
            this.lbDtaSaida.Text = "Data/hora de saída";
            this.lbDtaSaida.Visible = false;
            // 
            // dtDataHoraSaida
            // 
            this.dtDataHoraSaida.Location = new System.Drawing.Point(703, 55);
            this.dtDataHoraSaida.Name = "dtDataHoraSaida";
            this.dtDataHoraSaida.Size = new System.Drawing.Size(179, 20);
            this.dtDataHoraSaida.TabIndex = 14;
            this.dtDataHoraSaida.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(587, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Data/hora de entrada";
            // 
            // dtDataHoraEntrada
            // 
            this.dtDataHoraEntrada.Location = new System.Drawing.Point(703, 28);
            this.dtDataHoraEntrada.Name = "dtDataHoraEntrada";
            this.dtDataHoraEntrada.Size = new System.Drawing.Size(179, 20);
            this.dtDataHoraEntrada.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(351, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Placa";
            // 
            // txtPlaca
            // 
            this.txtPlaca.Location = new System.Drawing.Point(391, 80);
            this.txtPlaca.Name = "txtPlaca";
            this.txtPlaca.Size = new System.Drawing.Size(142, 20);
            this.txtPlaca.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(341, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Veículo";
            // 
            // txtVeiculo
            // 
            this.txtVeiculo.Location = new System.Drawing.Point(391, 55);
            this.txtVeiculo.Name = "txtVeiculo";
            this.txtVeiculo.Size = new System.Drawing.Size(142, 20);
            this.txtVeiculo.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(337, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Empresa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "CPF";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Matrícula";
            // 
            // txtCpf
            // 
            this.txtCpf.Enabled = false;
            this.txtCpf.Location = new System.Drawing.Point(92, 80);
            this.txtCpf.Name = "txtCpf";
            this.txtCpf.Size = new System.Drawing.Size(161, 20);
            this.txtCpf.TabIndex = 3;
            // 
            // txtMatricula
            // 
            this.txtMatricula.Enabled = false;
            this.txtMatricula.Location = new System.Drawing.Point(92, 54);
            this.txtMatricula.Name = "txtMatricula";
            this.txtMatricula.Size = new System.Drawing.Size(161, 20);
            this.txtMatricula.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome";
            // 
            // txtNome
            // 
            this.txtNome.Enabled = false;
            this.txtNome.Location = new System.Drawing.Point(92, 28);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(161, 20);
            this.txtNome.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.btnInserir);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.cbMatricula);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1301, 89);
            this.panel2.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.rbNome);
            this.groupBox1.Controls.Add(this.rbMatricula);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(92, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(262, 30);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            // 
            // rbNome
            // 
            this.rbNome.AutoSize = true;
            this.rbNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNome.Location = new System.Drawing.Point(173, 9);
            this.rbNome.Name = "rbNome";
            this.rbNome.Size = new System.Drawing.Size(53, 17);
            this.rbNome.TabIndex = 1;
            this.rbNome.TabStop = true;
            this.rbNome.Text = "Nome";
            this.rbNome.UseVisualStyleBackColor = true;
            this.rbNome.CheckedChanged += new System.EventHandler(this.rbMatricula_CheckedChanged);
            // 
            // rbMatricula
            // 
            this.rbMatricula.AutoSize = true;
            this.rbMatricula.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbMatricula.Location = new System.Drawing.Point(35, 9);
            this.rbMatricula.Name = "rbMatricula";
            this.rbMatricula.Size = new System.Drawing.Size(70, 17);
            this.rbMatricula.TabIndex = 0;
            this.rbMatricula.TabStop = true;
            this.rbMatricula.Text = "Matrícula";
            this.rbMatricula.UseVisualStyleBackColor = true;
            this.rbMatricula.CheckedChanged += new System.EventHandler(this.rbMatricula_CheckedChanged);
            // 
            // btnInserir
            // 
            this.btnInserir.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnInserir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInserir.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnInserir.Location = new System.Drawing.Point(374, 44);
            this.btnInserir.Name = "btnInserir";
            this.btnInserir.Size = new System.Drawing.Size(168, 33);
            this.btnInserir.TabIndex = 20;
            this.btnInserir.Text = "INSERIR";
            this.btnInserir.UseVisualStyleBackColor = false;
            this.btnInserir.Click += new System.EventHandler(this.btnInserir_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(35, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "Matrícula";
            // 
            // cbMatricula
            // 
            this.cbMatricula.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbMatricula.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.cbMatricula.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMatricula.FormattingEnabled = true;
            this.cbMatricula.Location = new System.Drawing.Point(93, 51);
            this.cbMatricula.Name = "cbMatricula";
            this.cbMatricula.Size = new System.Drawing.Size(261, 21);
            this.cbMatricula.TabIndex = 18;
            this.cbMatricula.Text = "Selecione";
            this.cbMatricula.SelectedIndexChanged += new System.EventHandler(this.cbMatricula_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(5, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 15);
            this.label10.TabIndex = 17;
            this.label10.Text = "Novo Acesso";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.pnBuscaAcessos);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1290, 451);
            // 
            // pnBuscaAcessos
            // 
            this.pnBuscaAcessos.BackColor = System.Drawing.Color.Transparent;
            this.pnBuscaAcessos.Controls.Add(this.label16);
            this.pnBuscaAcessos.Controls.Add(this.groupBox2);
            this.pnBuscaAcessos.Location = new System.Drawing.Point(3, 31);
            this.pnBuscaAcessos.Name = "pnBuscaAcessos";
            this.pnBuscaAcessos.Size = new System.Drawing.Size(224, 73);
            this.pnBuscaAcessos.TabIndex = 8;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(80, 8);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 16);
            this.label16.TabIndex = 4;
            this.label16.Text = "Acessos";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbFechados);
            this.groupBox2.Controls.Add(this.rbAbertos);
            this.groupBox2.Location = new System.Drawing.Point(6, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(210, 30);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            // 
            // rbFechados
            // 
            this.rbFechados.AutoSize = true;
            this.rbFechados.Location = new System.Drawing.Point(132, 9);
            this.rbFechados.Name = "rbFechados";
            this.rbFechados.Size = new System.Drawing.Size(72, 17);
            this.rbFechados.TabIndex = 1;
            this.rbFechados.TabStop = true;
            this.rbFechados.Text = "Fechados";
            this.rbFechados.UseVisualStyleBackColor = true;
            // 
            // rbAbertos
            // 
            this.rbAbertos.AutoSize = true;
            this.rbAbertos.Location = new System.Drawing.Point(29, 9);
            this.rbAbertos.Name = "rbAbertos";
            this.rbAbertos.Size = new System.Drawing.Size(73, 17);
            this.rbAbertos.TabIndex = 0;
            this.rbAbertos.TabStop = true;
            this.rbAbertos.Text = "Em aberto";
            this.rbAbertos.UseVisualStyleBackColor = true;
            this.rbAbertos.CheckedChanged += new System.EventHandler(this.rbAbertos_CheckedChanged);
            // 
            // colaborador_Fill_Panel
            // 
            // 
            // colaborador_Fill_Panel.ClientArea
            // 
            this.colaborador_Fill_Panel.ClientArea.Controls.Add(this.ultraTabControl1);
            this.colaborador_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default;
            this.colaborador_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.colaborador_Fill_Panel.Location = new System.Drawing.Point(8, 158);
            this.colaborador_Fill_Panel.Name = "colaborador_Fill_Panel";
            this.colaborador_Fill_Panel.Size = new System.Drawing.Size(1306, 489);
            this.colaborador_Fill_Panel.TabIndex = 0;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Location = new System.Drawing.Point(6, 6);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(1294, 477);
            this.ultraTabControl1.TabIndex = 0;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Acessos";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "tab1";
            ultraTab2.Visible = false;
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1290, 451);
            // 
            // _colaborador_Toolbars_Dock_Area_Left
            // 
            this._colaborador_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._colaborador_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._colaborador_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
            this._colaborador_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
            this._colaborador_Toolbars_Dock_Area_Left.InitialResizeAreaExtent = 8;
            this._colaborador_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 158);
            this._colaborador_Toolbars_Dock_Area_Left.Name = "_colaborador_Toolbars_Dock_Area_Left";
            this._colaborador_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(8, 489);
            this._colaborador_Toolbars_Dock_Area_Left.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _colaborador_Toolbars_Dock_Area_Right
            // 
            this._colaborador_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._colaborador_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._colaborador_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
            this._colaborador_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
            this._colaborador_Toolbars_Dock_Area_Right.InitialResizeAreaExtent = 8;
            this._colaborador_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(1314, 158);
            this._colaborador_Toolbars_Dock_Area_Right.Name = "_colaborador_Toolbars_Dock_Area_Right";
            this._colaborador_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(8, 489);
            this._colaborador_Toolbars_Dock_Area_Right.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _colaborador_Toolbars_Dock_Area_Top
            // 
            this._colaborador_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._colaborador_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._colaborador_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
            this._colaborador_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
            this._colaborador_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._colaborador_Toolbars_Dock_Area_Top.Name = "_colaborador_Toolbars_Dock_Area_Top";
            this._colaborador_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(1322, 158);
            this._colaborador_Toolbars_Dock_Area_Top.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _colaborador_Toolbars_Dock_Area_Bottom
            // 
            this._colaborador_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._colaborador_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._colaborador_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
            this._colaborador_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
            this._colaborador_Toolbars_Dock_Area_Bottom.InitialResizeAreaExtent = 8;
            this._colaborador_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 647);
            this._colaborador_Toolbars_Dock_Area_Bottom.Name = "_colaborador_Toolbars_Dock_Area_Bottom";
            this._colaborador_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(1322, 8);
            this._colaborador_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // ultraToolbarsManager1
            // 
            this.ultraToolbarsManager1.DesignerFlags = 1;
            this.ultraToolbarsManager1.DockWithinContainer = this;
            this.ultraToolbarsManager1.DockWithinContainerBaseType = typeof(System.Windows.Forms.Form);
            ribbonTab1.Caption = "Módulos";
            ribbonGroup1.Caption = "Funções";
            buttonTool3.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool3});
            ribbonGroup2.Caption = "Ferramentas";
            buttonTool6.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool9.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool11.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool1.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            controlContainerTool7.ControlName = "pnBuscaAcessos";
            controlContainerTool7.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            controlContainerTool7.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            controlContainerTool7.InstanceProps.Width = 224;
            ribbonGroup2.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool6,
            buttonTool9,
            buttonTool11,
            buttonTool1,
            controlContainerTool7});
            ribbonTab1.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup1,
            ribbonGroup2});
            this.ultraToolbarsManager1.Ribbon.NonInheritedRibbonTabs.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonTab[] {
            ribbonTab1});
            this.ultraToolbarsManager1.Ribbon.Visible = true;
            ultraToolbar1.DockedColumn = 0;
            ultraToolbar1.DockedRow = 0;
            ultraToolbar1.Text = "UltraToolbarColaborador";
            this.ultraToolbarsManager1.Toolbars.AddRange(new Infragistics.Win.UltraWinToolbars.UltraToolbar[] {
            ultraToolbar1});
            appearance11.Image = global::WindowsFormsApplication1.Properties.Resources.save_32;
            buttonTool2.SharedPropsInternal.AppearancesSmall.Appearance = appearance11;
            buttonTool2.SharedPropsInternal.Caption = "Salvar";
            appearance12.Image = global::WindowsFormsApplication1.Properties.Resources.Client_Account_Template;
            buttonTool4.SharedPropsInternal.AppearancesLarge.Appearance = appearance12;
            buttonTool4.SharedPropsInternal.Caption = "Cadastro";
            appearance13.Image = global::WindowsFormsApplication1.Properties.Resources.save_32;
            buttonTool5.SharedPropsInternal.AppearancesSmall.Appearance = appearance13;
            buttonTool5.SharedPropsInternal.Caption = "Salvar";
            controlContainerTool2.ControlName = "pnBuscaAcessos";
            controlContainerTool2.SharedPropsInternal.Width = 224;
            controlContainerTool4.SharedPropsInternal.Caption = "ControlContainerTool2";
            controlContainerTool6.SharedPropsInternal.Caption = "ControlContainerTool3";
            appearance14.Image = global::WindowsFormsApplication1.Properties.Resources.save_32;
            buttonTool7.SharedPropsInternal.AppearancesSmall.Appearance = appearance14;
            buttonTool7.SharedPropsInternal.Caption = "Salvar";
            appearance15.Image = global::WindowsFormsApplication1.Properties.Resources.Draw_Eraser;
            buttonTool8.SharedPropsInternal.AppearancesSmall.Appearance = appearance15;
            buttonTool8.SharedPropsInternal.Caption = "Limpar Campos";
            appearance16.Image = global::WindowsFormsApplication1.Properties.Resources.delete_32;
            buttonTool10.SharedPropsInternal.AppearancesSmall.Appearance = appearance16;
            buttonTool10.SharedPropsInternal.Caption = "Excluir";
            appearance17.Image = global::WindowsFormsApplication1.Properties.Resources.Table_Excel;
            buttonTool12.SharedPropsInternal.AppearancesSmall.Appearance = appearance17;
            buttonTool12.SharedPropsInternal.Caption = "Exportar";
            this.ultraToolbarsManager1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool2,
            buttonTool4,
            buttonTool5,
            controlContainerTool2,
            controlContainerTool4,
            controlContainerTool6,
            buttonTool7,
            buttonTool8,
            buttonTool10,
            buttonTool12});
            this.ultraToolbarsManager1.ToolClick += new Infragistics.Win.UltraWinToolbars.ToolClickEventHandler(this.ultraToolbarsManager1_ToolClick);
            // 
            // colaborador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1322, 655);
            this.Controls.Add(this.colaborador_Fill_Panel);
            this.Controls.Add(this._colaborador_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._colaborador_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._colaborador_Toolbars_Dock_Area_Top);
            this.Controls.Add(this._colaborador_Toolbars_Dock_Area_Bottom);
            this.Name = "colaborador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridAcessoColaborador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsAcessoColaborador)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.pnBuscaAcessos.ResumeLayout(false);
            this.pnBuscaAcessos.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.colaborador_Fill_Panel.ClientArea.ResumeLayout(false);
            this.colaborador_Fill_Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinToolbars.UltraToolbarsManager ultraToolbarsManager1;
        private Infragistics.Win.Misc.UltraPanel colaborador_Fill_Panel;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _colaborador_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _colaborador_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _colaborador_Toolbars_Dock_Area_Top;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _colaborador_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbNome;
        private System.Windows.Forms.RadioButton rbMatricula;
        private System.Windows.Forms.Button btnInserir;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbMatricula;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbDtaSaida;
        private System.Windows.Forms.DateTimePicker dtDataHoraSaida;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtDataHoraEntrada;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPlaca;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtVeiculo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCpf;
        private System.Windows.Forms.TextBox txtMatricula;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNome;
        private Infragistics.Win.UltraWinGrid.UltraGrid gridAcessoColaborador;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource dsAcessoColaborador;
        private System.Windows.Forms.TextBox txtEmpresa;
        private System.Windows.Forms.Label lbIdAcesso;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private System.Windows.Forms.Panel pnBuscaAcessos;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbFechados;
        private System.Windows.Forms.RadioButton rbAbertos;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter excelColaborador;
        private System.Windows.Forms.Label lbIdColaborador;
    }
}