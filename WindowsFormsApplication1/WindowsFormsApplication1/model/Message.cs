﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Infragistics.Win.UltraWinGrid;
using System.Diagnostics;
using System.Data.SQLite;
using System.IO;
using System.Collections;

namespace WindowsFormsApplication1
{
    class Message
    {
        public Message() { }
        public bool messageConfirmation(Form view, string message, string name)
        {
            DialogResult dr = System.Windows.Forms.DialogResult.Yes;
            dr = MessageBox.Show(
                view,
                message,
                name,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            switch (dr)
            {
                case DialogResult.Yes:
                    {
                        return true;
                    }
                case DialogResult.No:
                    {
                        return false;
                    }
                default : return true;
            }

        }

        public void messageInformation(Form view, string message, string name)
        {
            DialogResult dr = System.Windows.Forms.DialogResult.OK;
            dr = MessageBox.Show(
                view,
                message,
                name,
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        public void messageSuccess(Form view, string message, string name)
        {
            DialogResult dr = System.Windows.Forms.DialogResult.OK;
            dr = MessageBox.Show(
                view,
                message,
                name,
                MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation);
        }
    }
}
