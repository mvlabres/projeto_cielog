﻿namespace WindowsFormsApplication1
{
    partial class empresa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Id");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Nome");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn1 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Id");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn2 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Nome");
            this.gridUsuario = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dsEmpresa = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pbSalvar_usuario = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtempresa = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsEmpresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSalvar_usuario)).BeginInit();
            this.SuspendLayout();
            // 
            // gridUsuario
            // 
            this.gridUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridUsuario.DataMember = null;
            this.gridUsuario.DataSource = this.dsEmpresa;
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn1.Width = 75;
            ultraGridColumn2.Header.VisiblePosition = 1;
            ultraGridColumn2.Width = 297;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2});
            this.gridUsuario.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.gridUsuario.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.gridUsuario.ExitEditModeOnLeave = false;
            this.gridUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridUsuario.Location = new System.Drawing.Point(4, 66);
            this.gridUsuario.Name = "gridUsuario";
            this.gridUsuario.Size = new System.Drawing.Size(375, 200);
            this.gridUsuario.TabIndex = 10;
            this.gridUsuario.Text = "Empresas";
            this.gridUsuario.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.gridUsuario_ClickCell_1);
            this.gridUsuario.MouseEnterElement += new Infragistics.Win.UIElementEventHandler(this.gridUsuario_MouseEnterElement_1);
            // 
            // dsEmpresa
            // 
            this.dsEmpresa.Band.Columns.AddRange(new object[] {
            ultraDataColumn1,
            ultraDataColumn2});
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WindowsFormsApplication1.Properties.Resources.delete_32;
            this.pictureBox1.Location = new System.Drawing.Point(330, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 33);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // pbSalvar_usuario
            // 
            this.pbSalvar_usuario.Image = global::WindowsFormsApplication1.Properties.Resources.save_32;
            this.pbSalvar_usuario.Location = new System.Drawing.Point(282, 27);
            this.pbSalvar_usuario.Name = "pbSalvar_usuario";
            this.pbSalvar_usuario.Size = new System.Drawing.Size(42, 33);
            this.pbSalvar_usuario.TabIndex = 8;
            this.pbSalvar_usuario.TabStop = false;
            this.pbSalvar_usuario.Click += new System.EventHandler(this.pbSalvar_usuario_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(228, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Informe o nome da empresa visitante";
            // 
            // txtempresa
            // 
            this.txtempresa.Location = new System.Drawing.Point(4, 28);
            this.txtempresa.Multiline = true;
            this.txtempresa.Name = "txtempresa";
            this.txtempresa.Size = new System.Drawing.Size(248, 32);
            this.txtempresa.TabIndex = 6;
            // 
            // Empresa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 275);
            this.Controls.Add(this.gridUsuario);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pbSalvar_usuario);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtempresa);
            this.Name = "Empresa";
            this.Text = "Gerenciar visitantes";
            ((System.ComponentModel.ISupportInitialize)(this.gridUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsEmpresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSalvar_usuario)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid gridUsuario;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pbSalvar_usuario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtempresa;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource dsEmpresa;
    }
}