﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;

namespace WindowsFormsApplication1
{
    class ColaboradorController
    {
        private ColaboradroDAO colaborador;
        private DataTable grid;
        private DataTable cadastro;
        private ColaboradroDAO colaboradorDAO;
        private bool flag;
        private ClassColaborador classColaborador;
        private bool result;

        public bool Result
        {
            get { return result; }
            set { result = value; }
        }

        internal ClassColaborador ClassColaborador
        {
            get { return classColaborador; }
            set { classColaborador = value; }
        }

        public bool Flag
        {
            get { return flag; }
            set { flag = value; }
        }

        internal ColaboradroDAO ColaboradorDAO
        {
            get { return colaboradorDAO; }
            set { colaboradorDAO = value; }
        }

        public DataTable Cadastro
        {
            get { return cadastro; }
            set { cadastro = value; }
        }

        public DataTable Grid
        {
            get { return grid; }
            set { grid = value; }
        }
        public void deleteForList(List<int> idColaborador)
        {
            foreach (var id in idColaborador)
            {
                ColaboradroDAO colaboradorDAO = new ColaboradroDAO();
                this.Result = colaboradorDAO.delete(id);
            }
        }

        public bool insertUpdate(ClassColaborador colaborador, string captionToolbar)
        {
            this.colaborador = new ColaboradroDAO();
            if (captionToolbar == "Salvar")
            {
                return this.colaborador.insert(colaborador);
            }
            else 
            {
                return this.colaborador.update(colaborador);
            }
            
        }

        public void selectGrid()
        {
            this.flag = false;
            this.cadastro = new DataTable();
            this.colaboradorDAO = new ColaboradroDAO();
            colaboradorDAO.selectGrid();
            colaboradorDAO.SqlAdapter.Fill(cadastro);
            this.grid = cadastro;
        }

        public void select()
        {
            this.colaboradorDAO = new ColaboradroDAO();
            this.colaboradorDAO.select();
        }

        public void selectId(int id)
        {
            this.colaboradorDAO = new ColaboradroDAO();
            this.colaboradorDAO.selectId(id);
        }
    }
}
