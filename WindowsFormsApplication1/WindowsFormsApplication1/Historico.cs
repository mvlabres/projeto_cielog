﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Infragistics.Win.UltraWinGrid;
using System.Data.SQLite;

namespace WindowsFormsApplication1
{
    public partial class Historico : Form
    {
        #region construtor

        //variaveis contadoras
        int contComboCPF;
        int contComboEmpresa;


        public Historico()
        {
            InitializeComponent();
            contComboCPF = contComboEmpresa = 0;
            validarCamposFalse();
            campos_datas();
            carregarFiltros();
            //carregarCombos(1, "select cpf from historico order by cpf");
            //carregarCombos(2, "select nome from empresa order by nome");
            carregarCombos(3, "select nome from usuario order by nome");
            carregarGrid(consultaBanco);
        }
        string consultaBanco = "select * from historico where data_entrada >= @dataInicio  and data_entrada <=  @datafim and data_saida is not null order by data_entrada desc";
        string id_edicao = "";
        List<string> id = new List<string>();
        String strConn = @"Data Source=C:\cielog\cielog.s3db";

        #endregion

        #region metodos
        private void carregarCombos(int combo, string sql)
        {
            SQLiteConnection con = new SQLiteConnection(strConn);
            con.Open();
            SQLiteCommand comando = new SQLiteCommand(sql, con);
            var reader = comando.ExecuteReader();
            while (reader.Read())
            {
                if (combo == 1) cbcpf.Items.Add(reader["cpf"].ToString());
                else if (combo == 2)
                {
                    cbEmpresa.Items.Add(reader["nome"].ToString());
                    cbAlteraCpf.Items.Add(reader["nome"].ToString());
                    cbEmpresaVisitada.Items.Add(reader["nome"].ToString());
                }
                else
                {
                    cbusuarioSaida.Items.Add(reader["nome"].ToString());
                }
            } 
        }
        private void Limpar_Campos()
        {
            txtCor.Text = txtNome.Text = txtVeiculo.Text = txtPlacaCarreta.Text = txtPlacaCavalo.Text = 
            txtNfEntrada.Text = txtNfSaida.Text = txtSenha.Text = lbCpf.Text = "";
            dtpDataEntrada.Value = DTPHoraSaida.Value = DateTime.Now;
            cbEmpresaVisitada.Text = cbAlteraCpf.Text = cbusuarioSaida.Text = cbTipo_caminhao.Text = "selecione";
            txtNfEntrada.Enabled = txtNfSaida.Enabled = txtSenha.Enabled = false;
            DTPHoraSaida.Visible = true;
            picAnularFinal.Visible = true;
            lbSaida.Visible = true;
        }

        private void campos_datas()
        {
            dtInicio.Format = DateTimePickerFormat.Custom;
            dtInicio.CustomFormat = "dd/MM/yyyy";
            dtFim.Format = DateTimePickerFormat.Custom;
            dtFim.CustomFormat = "dd/MM/yyyy";
            dtFim.Value = DateTime.Now;
            dtpDataEntrada.Format = DateTimePickerFormat.Custom;
            dtpDataEntrada.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            DTPHoraSaida.Format = DateTimePickerFormat.Custom;
            DTPHoraSaida.CustomFormat = "dd/MM/yyyy hh:mm:ss";
        }

        private void deletar()
        {
            int resultado = -1;
            if (id.Count > 0)
            {
                DialogResult dr = System.Windows.Forms.DialogResult.Yes;
                dr = MessageBox.Show(
                          this,
                          "Tem certeza que deseja excluir " + id.Count.ToString() + " registro(s) selecionado(s)? ",
                          "Confirmação",
                          MessageBoxButtons.YesNo,
                          MessageBoxIcon.Question);
                switch (dr)
                {
                    case DialogResult.Yes:
                        {
                            foreach (string c in id)
                            {
                                using (SQLiteConnection conn = new SQLiteConnection(strConn))
                                {
                                    conn.Open();
                                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                                    {
                                        cmd.CommandText = "delete from historico where id_historico = @id_historico";
                                        cmd.Prepare();
                                        cmd.Parameters.AddWithValue("@id_historico", c);
                                        resultado = cmd.ExecuteNonQuery();
                                        conn.Close();
                                    }
                                }
                            }
                            carregarGrid(consultaBanco);
                            id.Clear();
                            mensagem("Registros excluídos com sucesso!");
                            Limpar_Campos();
                        }
                        break;
                    case DialogResult.No: break;
                }
            }
            else mensagem("Não há registros selecionados");
        }
        private void salvar()
        {
            int resultado = -1;
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(strConn))
                {
                    conn.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "update historico set empresa=@empresa,veiculo=@veiculo,cor=@cor,placa=@placa, placa_carreta=@placa_carreta, nf_entrada=@nf_entrada, nf_saida=@nf_saida, senha=@senha, data_entrada=@data_entrada, data_saida=@data_saida, empresa_visitada = @empresa_visitada, usuario_saida = @usuario_saida,tipo_caminhao = @tipo_caminhao where id_historico = @id";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@data_entrada", Convert.ToDateTime(dtpDataEntrada.Value));
                        if (DTPHoraSaida.Visible == true) cmd.Parameters.AddWithValue("@data_saida", Convert.ToDateTime(DTPHoraSaida.Value));
                        else cmd.Parameters.AddWithValue("@data_saida", null);
                        cmd.Parameters.AddWithValue("@nf_entrada", txtNfEntrada.Text);
                        cmd.Parameters.AddWithValue("@nf_saida", txtNfSaida.Text);
                        cmd.Parameters.AddWithValue("@senha", txtSenha.Text);
                        cmd.Parameters.AddWithValue("@cor", txtCor.Text);
                        cmd.Parameters.AddWithValue("@veiculo", txtVeiculo.Text);
                        cmd.Parameters.AddWithValue("@placa", txtPlacaCavalo.Text);
                        cmd.Parameters.AddWithValue("@placa_carreta", txtPlacaCarreta.Text);
                        cmd.Parameters.AddWithValue("@empresa_visitada", cbEmpresaVisitada.Text);
                        cmd.Parameters.AddWithValue("@empresa", cbAlteraCpf.Text);
                        cmd.Parameters.AddWithValue("@usuario_saida", cbusuarioSaida.Text);
                        cmd.Parameters.AddWithValue("@id", id_edicao);
                        cmd.Parameters.AddWithValue("@tipo_caminhao", cbTipo_caminhao.Text);
                        resultado = cmd.ExecuteNonQuery();
                        conn.Close(); 
                    }
                }
            }
            catch { }
            carregarGrid(consultaBanco);
            mensagem("Alterações salvas com sucesso");
            Limpar_Campos();
            validarCamposFalse();
        }

        private void marcarTodos()
        {
            foreach (var row in gridHistorico.DisplayLayout.Rows)
            {
                if (!id.Contains(row.Cells["id"].Text)) id.Add(row.Cells["id"].Text);
                row.Cells["X"].Value = true;
            }
        }

        private void desmarcarTodos()
        {
            foreach (var row in gridHistorico.DisplayLayout.Rows)
            {
                if (id.Contains(row.Cells["id"].Text)) id.Remove(row.Cells["id"].Text);
                row.Cells["X"].Value = false;
            }
        }
 
        private void exportar()
        {
            string caminho = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog1.Filter = "Excel Workbook (*.xls)|*.xls|All Files (*.*)|*.*";
            DialogResult dg = saveFileDialog1.ShowDialog(this);
            if (dg == System.Windows.Forms.DialogResult.Cancel) return;
            string filename = saveFileDialog1.FileName;
            Workbook WB = new Workbook();
            WB.Worksheets.Add("Histórico");
            excelHistorico.Export(gridHistorico, WB.Worksheets["Histórico"], 0, 0);
            BIFF8Writer.WriteWorkbookToFile(WB, filename);
            System.Diagnostics.Process.Start(filename);
        }
        private void mensagem(string msg)
        {
            DialogResult dr = System.Windows.Forms.DialogResult.Yes;
            dr = MessageBox.Show(
                      this,
                      msg,
                      "Informação",
                      MessageBoxButtons.OK,
                      MessageBoxIcon.Information); 
        }
        private void validarCamposFalse()
        {
            txtNfEntrada.Enabled = txtNfSaida.Enabled = txtSenha.Enabled = dtpDataEntrada.Enabled = picAnularFinal.Enabled = DTPHoraSaida.Enabled =
            txtCor.Enabled = txtNome.Enabled = txtPlacaCavalo.Enabled = txtPlacaCarreta.Enabled = txtVeiculo.Enabled = txtCor.Enabled = cbusuarioSaida.Enabled = cbAlteraCpf.Enabled = cbEmpresaVisitada.Enabled = cbTipo_caminhao.Enabled =  false;
            DTPHoraSaida.Visible = false;
        }

        private void validarCampostrue()
        {
            txtNfEntrada.Enabled = txtNfSaida.Enabled = txtSenha.Enabled = dtpDataEntrada.Enabled = picAnularFinal.Enabled = DTPHoraSaida.Enabled =
            txtCor.Enabled = txtNome.Enabled = txtPlacaCavalo.Enabled = txtPlacaCarreta.Enabled = txtVeiculo.Enabled = txtCor.Enabled = cbusuarioSaida.Enabled = cbAlteraCpf.Enabled = cbEmpresaVisitada.Enabled = cbTipo_caminhao.Enabled = true;
            DTPHoraSaida.Visible =true;

        }

        private void carregarGrid(string dados)
        {
            bool marque = false;
            dshistoricos.Rows.Clear();
            DataTable acesso = new DataTable();
            SQLiteConnection con = new SQLiteConnection(strConn);
            con.Open();
            SQLiteCommand cmd = new SQLiteCommand(con);
            cmd.CommandText = dados;
            cmd.Parameters.AddWithValue("@dataInicio", Convert.ToDateTime(dtInicio.Value));
            cmd.Parameters.AddWithValue("@datafim", Convert.ToDateTime(dtFim.Value));
            SQLiteDataAdapter envia = new SQLiteDataAdapter(cmd);
            envia.Fill(acesso);
            DataTable grid = acesso;
            foreach (DataRow grd in grid.Rows)
            {
                object[] novoDados = new object[]{
                    marque,
                    grd[0].ToString(),//id
                    grd[1].ToString(),//turno
                    grd[2].ToString(),//cpf
                    grd[12].ToString(),//moto
                    grd[13].ToString(),//empresa
                    grd[3].ToString(),//data entrada
                    grd[4].ToString(),//data saida
                    grd[5].ToString(),//placa
                    grd[6].ToString(),//carreta
                    grd[7].ToString(),//veiculo
                    grd[8].ToString(),//cor
                    grd[9].ToString(),//nfentrada
                    grd[10].ToString(),//nf saida
                    grd[11].ToString(),//usuario
                    grd[16].ToString(),//usuario final
                    grd[14].ToString(),//senha
                    grd[15].ToString(),//empresavisitada
                    grd[17].ToString()//tipo de caminhão
 
                }; dshistoricos.Rows.Add(novoDados);
            }
        }
        #endregion

        #region eventos
        private void ultraToolbarsManager1_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            switch (e.Tool.Key)
            {
                case "Salvar":
                    {
                        if (txtNfEntrada.Enabled == true) salvar();
                        else mensagem("Não há dados.");
                    }
                    break;
                case "Exportar":
                    {
                        if (dshistoricos.Rows.Count > 0) exportar();
                        else mensagem("Tabela vazia!");
                    }
                    break;
                case "Deletar":
                    {
                        deletar();
                    }
                    break;
                case "Marcar Todos":
                    {
                        marcarTodos();
                    }
                    break;
                case "Desmarcar Todos":
                    {
                        desmarcarTodos();
                    }
                    break;
            }
        }
        private void carregarFiltros()
        {
            DateTime mes = DateTime.Now;
            dtInicio.Value = Convert.ToDateTime("01/" + mes.ToString("MM/yyyy 00:00:00"));
            cbEmpresas.Checked = cbcpfs.Checked = true;
        }

        private void pbAtualizar_Click(object sender, EventArgs e)
        {
            bool marque = false;
            dshistoricos.Rows.Clear();
            DataTable acesso = new DataTable();
            SQLiteConnection con = new SQLiteConnection(strConn);
            con.Open();
            SQLiteCommand cmd = new SQLiteCommand(con);
            string sql = "";
            if (cbcpf.Enabled != false && cbEmpresa.Enabled != false && cbcpf.Text != "selecione" && cbEmpresa.Text != "selecione")
            {
                cmd.CommandText = "select * from historico where cpf = @cpf and empresa = @empresa and data_saida is not null and data_entrada >= @dataInicio  and data_entrada <=  @datafim  order by data_entrada"; ;
                cmd.Parameters.AddWithValue("@cpf", cbcpf.Text);
                cmd.Parameters.AddWithValue("@empresa", cbEmpresa.Text);
                cmd.Parameters.AddWithValue("@dataInicio", Convert.ToDateTime(dtInicio.Value));
                cmd.Parameters.AddWithValue("@datafim", Convert.ToDateTime(dtFim.Value));
            }
            else if (cbcpf.Enabled != false && cbEmpresa.Enabled == false && cbcpf.Text != "selecione")
            {
                cmd.CommandText = "select * from historico where cpf = @cpf and data_entrada >= @dataInicio and data_saida is not null and data_entrada <=  @datafim  order by data_entrada";
                cmd.Parameters.AddWithValue("@cpf", cbcpf.Text);
            }
            else if (cbcpf.Enabled == false && cbEmpresa.Enabled != false &&  cbEmpresa.Text != "selecione")
            {
                cmd.CommandText = "select * from historico where  empresa = @empresa and data_entrada >= @dataInicio and data_saida is not null and data_entrada <=  @datafim  order by data_entrada";
                cmd.Parameters.AddWithValue("@empresa", cbEmpresa.Text);
            }
            else 
            {
                sql = consultaBanco;
                carregarGrid(sql);
                return;
            }
            cmd.Parameters.AddWithValue("@dataInicio", Convert.ToDateTime(dtInicio.Value));
            cmd.Parameters.AddWithValue("@datafim", Convert.ToDateTime(dtFim.Value));
            SQLiteDataAdapter envia = new SQLiteDataAdapter(cmd);
            envia.Fill(acesso);
            DataTable grid = acesso;
            foreach (DataRow grd in grid.Rows)
            {
                object[] novoDados = new object[]{
                    marque,
                    grd[0].ToString(),//id
                    grd[1].ToString(),//turno
                    grd[2].ToString(),//cpf
                    grd[12].ToString(),//moto
                    grd[13].ToString(),//empresa
                    grd[3].ToString(),//data entrada
                    grd[4].ToString(),//data saida
                    grd[5].ToString(),//placa
                    grd[6].ToString(),//carreta
                    grd[7].ToString(),//veiculo
                    grd[8].ToString(),//cor
                    grd[9].ToString(),//nfentrada
                    grd[10].ToString(),//nf saida
                    grd[11].ToString(),//usuario
                    grd[16].ToString(),//usuario final
                    grd[14].ToString(),//senha
                    grd[15].ToString(),//empresavisitada
                    grd[17].ToString()//tipo de caminhão
                }; dshistoricos.Rows.Add(novoDados);
            }
        }

        private void cbnomes_CheckedChanged(object sender, EventArgs e)
        {
            if (cbEmpresas.Checked == true) cbEmpresa.Enabled = false;
            else cbEmpresa.Enabled = true;
            if (cbcpfs.Checked == true) cbcpf.Enabled = false;
            else cbcpf.Enabled = true;
        }

        private void gridHistorico_CellChange(object sender, CellEventArgs e)
        {
            if ((bool)e.Cell.Row.Cells["X"].Value)
            {
                e.Cell.Row.Cells["X"].Value = false;
                id.Remove(e.Cell.Row.Cells["id"].Text);
            }
            else
            {
                e.Cell.Row.Cells["X"].Value = true;
                id.Add(e.Cell.Row.Cells["id"].Text);
            }
        }
        private void gridHistorico_ClickCell(object sender, ClickCellEventArgs e)
        {
            validarCampostrue();
            id_edicao = e.Cell.Row.Cells["id"].Text;
            lbCpf.Text = "CPF: " + e.Cell.Row.Cells["CPF"].Text;
            txtNome.Text = e.Cell.Row.Cells["Motorista"].Text;
            cbAlteraCpf.Text = e.Cell.Row.Cells["Empresa"].Text;
            cbusuarioSaida.Text = e.Cell.Row.Cells["usuario saída"].Text;
            dtpDataEntrada.Value = Convert.ToDateTime(e.Cell.Row.Cells["Data/hora de entrada"].Text);
            DTPHoraSaida.Value = Convert.ToDateTime(e.Cell.Row.Cells["Data/hora de saída"].Text);
            txtNfEntrada.Text = e.Cell.Row.Cells["NF Entrada"].Text;
            txtNfSaida.Text = e.Cell.Row.Cells["NF Saída"].Text;
            txtSenha.Text = e.Cell.Row.Cells["Senha"].Text;
            txtPlacaCavalo.Text = e.Cell.Row.Cells["Placa"].Text;
            txtPlacaCarreta.Text = e.Cell.Row.Cells["Placa carreta"].Text;
            txtVeiculo.Text = e.Cell.Row.Cells["Veículo"].Text;
            txtCor.Text = e.Cell.Row.Cells["Cor"].Text;
            cbEmpresaVisitada.Text = e.Cell.Row.Cells["Empresa Visitada"].Text;
            cbTipo_caminhao.Text = e.Cell.Row.Cells["Tipo de caminhão"].Text;
        }
        private void picAnularFinal_Click(object sender, EventArgs e)
        {
            if (DTPHoraSaida.Visible == true) DTPHoraSaida.Visible = lbSaida.Visible = false;
            else DTPHoraSaida.Visible = lbSaida.Visible = true; 
        }
        private void btnLimpar_Campos_Click(object sender, EventArgs e)
        {
            Limpar_Campos();
        }
        private void txtLimparCampos_Click(object sender, EventArgs e)
        {
            Limpar_Campos();
        }
        #endregion 

        
        /*eventos de aceleração de busca*/
        private void cbcpfs_MouseClick(object sender, MouseEventArgs e)
        {
            if (contComboCPF == 0)
            {
                carregarCombos(1, "select cpf from historico order by cpf");
                contComboCPF++;
            }
        }

        private void cbEmpresas_MouseClick(object sender, MouseEventArgs e)
        {
            if (contComboEmpresa == 0)
            {
                carregarCombos(2, "select nome from empresa order by nome");
                contComboEmpresa++;
            }
        }
    }
}
