﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1
{
    class ClassColaborador
    {
        private int id;
        private string nome;
        private string matricula;
        private string cpf;
        private string empresa;
        private string veiculo;
        private string placa;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string Matricula
        {
            get { return matricula; }
            set { matricula = value; }
        }

        public string Cpf
        {
            get { return cpf; }
            set { cpf = value; }
        }
        
        public string Empresa
        {
            get { return empresa; }
            set { empresa = value; }
        }
        
        public string Veiculo
        {
            get { return veiculo; }
            set { veiculo = value; }
        }
        
        public string Placa
        {
            get { return placa; }
            set { placa = value; }
        }
    }
}
