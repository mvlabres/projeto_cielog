﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using System.Data.SQLite;
using Infragistics.Win.UltraWinGrid;

namespace WindowsFormsApplication1
{
    public partial class cadastro : Form
    {
        int contCPF, contNome;
        public cadastro(string usuario)
        {
            InitializeComponent();
            contCPF = contNome = 0;
            usuario_logado = usuario;
            dtpCNH.Format = DateTimePickerFormat.Custom;
            dtpCNH.CustomFormat = "dd/MM/yyyy";
            carrega_grid();
            cbFiltraCPF.Text = "Selecione";
            /*if (rbCpf.Checked == true)
            {
                carregarCombo("select cpf from cadastro order by cpf", 1);
                AutoComplete("select cpf from cadastro order by cpf", 1);
            }
            else 
            {
                carregarCombo("select nome from cadastro order by nome", 1);
                AutoComplete("select nome from cadastro order by nome", 1);
            }*/

            carregarCombo("select nome from empresa order by nome", 2);
            AutoComplete("select nome from empresa order by nome", 2);

            //carregar combo de tipo de cacdastro
            cbTipo_cadastro.Text = "Selecione";
            cbTipo_cadastro.Items.Add("motorista");
            cbTipo_cadastro.Items.Add("outros");

            //carregar combo status
            
            cbStatus.Items.Add("Bloqueado");
            cbStatus.Items.Add("Liberado");
            cbStatus.SelectedIndex = 1;

            rbCpf.Checked = true;
            txtnome.Focus();
            id.Clear();
        }
        string usuario_logado = "";
        List<string> id = new List<string>();
        int ID = -1;
        String strConn = @"Data Source=C:\cielog\cielog.s3db";
        #region metodos

        private void carregarCombo(string sql, int combo)
        {
            SQLiteConnection con = new SQLiteConnection(strConn);
            con.Open();
            SQLiteCommand comando = new SQLiteCommand(sql, con);
            var reader = comando.ExecuteReader();
            while (reader.Read())
            {
                if (combo == 1)
                {
                    if (rbCpf.Checked == true) cbFiltraCPF.Items.Add(reader["cpf"].ToString());
                    else cbFiltraCPF.Items.Add(reader["nome"].ToString());
                    
                }
                else ComboEmpresa.Items.Add(reader["nome"].ToString());
            }
        }
    
        private void carrega_grid()
        {
            bool marcar = false;
            dscadastro.Rows.Clear();
            DataTable acesso = new DataTable();
            SQLiteConnection con = new SQLiteConnection(strConn);
            con.Open();
            SQLiteCommand cmd = new SQLiteCommand(con);
            cmd.CommandText = "select * from cadastro order by nome";
            SQLiteDataAdapter envia = new SQLiteDataAdapter(cmd);
            envia.Fill(acesso);
            DataTable grid = acesso;
            string cnh = null;
            foreach (DataRow grd in grid.Rows)
            {
                cnh = grd[9].ToString();
                object[] novoDados = new object[]{
                    marcar,
                    grd[0].ToString(),//id
                    grd[1].ToString(),//cpf
                    grd[2].ToString(),//nome
                    grd[3].ToString(),//empresa
                    grd[4].ToString(),//placa
                    grd[5].ToString(),//carreta
                    grd[6].ToString(),//veiculo
                    grd[7].ToString(),//cor
                    grd[8].ToString(),//data de criação
                    cnh,
                    grd[10].ToString(),//tipo de cadastro
                    grd[11].ToString(),//usuario criação
                    grd[12].ToString(),//usuario alteração
                    grd[13].ToString(),//data alteração
                    grd[14].ToString(),//CNH
                    grd[15].ToString()//status
                }; dscadastro.Rows.Add(novoDados);
            }
        }

        private void salvar()
        {
            int resultado = -1;
            try
            {
                    using (SQLiteConnection conn = new SQLiteConnection(strConn))
                    {
                        string msg = "";
                        conn.Open();
                        using (SQLiteCommand cmd = new SQLiteCommand(conn))
                        {

                            if (ID == -1) cmd.CommandText = "INSERT INTO cadastro(cpf,nome,empresa,placa,placa_carreta,veiculo,cor,data_criacao, vencimento_cnh, tipo_cadastro, usuario_criacao, numeroCNH, status, motivo) VALUES (@cpf,@nome,@empresa,@placa,@placa_carreta,@veiculo,@cor,@data_criacao,@vencimentoCNH, @tipo_cadastro, @usuario, @numeroCNH, @status, @motivo)";
                            else cmd.CommandText = "update cadastro set cpf=@cpf,nome=@nome,empresa=@empresa,placa=@placa,placa_carreta=@placa_carreta,veiculo=@veiculo,cor=@cor, vencimento_cnh = @vencimentoCNH, tipo_cadastro = @tipo_cadastro, usuario_alteracao = @usuario_alteracao, data_alteracao = @data_alteracao, numeroCNH = @numeroCNH, status = @status, motivo = @motivo  where id_cadastro = @id";
                            cmd.Prepare();
                            if (ID > -1)
                            {
                                cmd.Parameters.AddWithValue("@id", ID);
                                cmd.Parameters.AddWithValue("@usuario_alteracao", usuario_logado);
                                cmd.Parameters.AddWithValue("@data_alteracao", DateTime.Now);
                            }
                            else 
                            {
                                cmd.Parameters.AddWithValue("@usuario", usuario_logado);
                                cmd.Parameters.AddWithValue("@data_criacao", DateTime.Now);
                            }
                            cmd.Parameters.AddWithValue("@cpf", txtCPF.Text);
                            cmd.Parameters.AddWithValue("@nome", txtnome.Text.ToUpper());
                            cmd.Parameters.AddWithValue("@empresa", ComboEmpresa.Text);
                            cmd.Parameters.AddWithValue("@placa", txtPlaca.Text);
                            cmd.Parameters.AddWithValue("@placa_carreta", txtPlaca_carreta.Text);
                            cmd.Parameters.AddWithValue("@veiculo", txtVeiculo.Text);
                            cmd.Parameters.AddWithValue("@cor", txtCor.Text);
                            cmd.Parameters.AddWithValue("@numeroCNH", txtCNH.Text);
                            if (cbTipo_cadastro.Text == "outros") cmd.Parameters.AddWithValue("@vencimentoCNH", null);
                            else
                            {
                                cmd.Parameters.AddWithValue("@vencimentoCNH", Convert.ToDateTime(dtpCNH.Text));
                            }
                            cmd.Parameters.AddWithValue("@tipo_cadastro", cbTipo_cadastro.Text);
                            cmd.Parameters.AddWithValue("@status", cbStatus.Text);
                            cmd.Parameters.AddWithValue("@motivo", txtMotivo.Text);
                            resultado = cmd.ExecuteNonQuery();
                            conn.Close();
                            carrega_grid();
                            rbCpf.Checked = true;
                            AutoComplete("select cpf from cadastro order by cpf", 1);
                            dtpCNH.Enabled = true;


                            if (ID == -1) msg = "cadastro salvo com sucesso";
                            else msg = "cadastro alterado com sucesso";

                            mensagem(msg);
                        }
                    }
               
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
        private void mensagem(string msg)
        {
            DialogResult dr = System.Windows.Forms.DialogResult.Yes;
            dr = MessageBox.Show(
                      this,
                      msg,
                      "Informação",
                      MessageBoxButtons.OK,
                      MessageBoxIcon.Information);
        }
        private void AutoComplete(string sql, int combo)
        {
            SQLiteConnection con = new SQLiteConnection(strConn);
            con.Open();
            SQLiteCommand comando = new SQLiteCommand(sql, con);
            var reader = comando.ExecuteReader();
            while (reader.Read())
            {
                if (combo == 1)
                {
                    if (rbCpf.Checked == true) cbFiltraCPF.AutoCompleteCustomSource.Add(reader["cpf"].ToString());
                    else cbFiltraCPF.AutoCompleteCustomSource.Add(reader["nome"].ToString());
                }
                else ComboEmpresa.AutoCompleteCustomSource.Add(reader["nome"].ToString());
               
            }
        }
        private void limpar_campos()
        {
            txtnome.Clear();
            ComboEmpresa.Text = "Selecione";
            txtCPF.Clear();
            txtPlaca.Clear();
            txtPlaca_carreta.Clear();
            txtVeiculo.Clear();
            txtCor.Clear();
            txtnome.Focus();
            dtpCNH.Value = DateTime.Now;
            cbTipo_cadastro.Text = "Selecione";
            cbStatus.Text = "Liberado";
            txtCNH.Text = "";
            ID = -1;
            lbMotivo.Visible = txtMotivo.Visible = false;
        }

        private int verifica_cadastros()
        {
            int id = 0;
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(strConn))
                {
                    conn.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = "select id_cadastro from cadastro where cpf = @cpf";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@cpf", txtCPF.Text);
                        var reader = cmd.ExecuteReader();
                        //conn.Close();
                        while (reader.Read())
                        {
                            id = Convert.ToInt32(reader["id_cadastro"]);
                        }

                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            return id;
        }

        private void marcarTodos()
        {
            foreach (var row in gridcadastro.DisplayLayout.Rows)
            {
                row.Cells["X"].Value = true;
                if (!id.Contains(row.Cells["ID"].Text))
                {
                    id.Add(row.Cells["ID"].Text);
                }
            }
        }
        private void desmarcarTodos()
        {
            foreach (var row in gridcadastro.DisplayLayout.Rows)
            {
                row.Cells["X"].Value = false;
                if (id.Contains(row.Cells["ID"].Text))
                {
                    id.Remove(row.Cells["ID"].Text);
                }
            }
        }

        private void deletar()
        {
            int resultado = -1;
            if (ID > -1)
            {

                DialogResult dr = System.Windows.Forms.DialogResult.Yes;
                dr = MessageBox.Show(
                          this,
                           "Tem certeza que deseja excluir o registro número " + ID.ToString() + "?",
                          "Confirmação",
                          MessageBoxButtons.YesNo,
                          MessageBoxIcon.Question);
                switch (dr)
                {
                    case DialogResult.Yes:
                        {
                            try
                            {
                                using (SQLiteConnection conn = new SQLiteConnection(strConn))
                                {
                                    string msg = "";
                                    conn.Open();
                                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                                    {
                                        cmd.CommandText = "delete from cadastro where id_cadastro = @id_cadastro";
                                        cmd.Prepare();
                                        cmd.Parameters.AddWithValue("@id_cadastro", ID.ToString());
                                        resultado = cmd.ExecuteNonQuery();
                                        conn.Close();
                                    }
                                }
                            }
                            catch (Exception ex) { MessageBox.Show(ex.Message); }
                        }
                        carrega_grid();
                        if (rbCpf.Checked == true) AutoComplete("select cpf from cadastro order by cpf", 1);
                        else AutoComplete("select nome from cadastro order by nome", 1);
                        id.Clear();
                        mensagem("Registro excluído com sucesso!");
                        limpar_campos();
                        break;
                    case DialogResult.No: break;
                }
            }
            else
            {
                if (id.Count > 0)
                {
                    DialogResult dr = System.Windows.Forms.DialogResult.Yes;
                    dr = MessageBox.Show(
                          this,
                          "Tem certeza que deseja excluir " + id.Count.ToString() + " registro(s) selecionado(s)? ",
                          "Confirmação",
                          MessageBoxButtons.YesNo,
                          MessageBoxIcon.Question);
                    switch (dr)
                    {
                        case DialogResult.Yes:
                            {
                                foreach (string c in id)
                                {
                                    try
                                    {
                                        using (SQLiteConnection conn = new SQLiteConnection(strConn))
                                        {
                                            string msg = "";
                                            conn.Open();
                                            using (SQLiteCommand cmd = new SQLiteCommand(conn))
                                            {
                                                if (ID == -1) cmd.CommandText = "delete from cadastro where id_cadastro = @id_cadastro";
                                                cmd.Prepare();
                                                cmd.Parameters.AddWithValue("@id_cadastro", c);
                                                resultado = cmd.ExecuteNonQuery();
                                                conn.Close();
                                            }
                                        }
                                    }
                                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                                }
                                carrega_grid();
                                if (rbCpf.Checked == true) AutoComplete("select cpf from cadastro order by cpf", 1);
                                else AutoComplete("select nome from cadastro order by nome", 1);

                                id.Clear();
                                mensagem("Registros excluídos com sucesso!");
                                limpar_campos();
                            }
                            break;
                        case DialogResult.No: break;
                    }

                }
                else mensagem("Não há registros selecionados");
            }
        }

        private void Exportar()
        {
            string caminho = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

            saveFileDialog1.Filter = "Excel Workbook (*.xls)|*.xls|All Files (*.*)|*.*";

            DialogResult dg = saveFileDialog1.ShowDialog(this);

            if (dg == System.Windows.Forms.DialogResult.Cancel) return;

            string filename = saveFileDialog1.FileName;

            Workbook WB = new Workbook();
            WB.Worksheets.Add("Cadastro");
            excelCadastro.Export(gridcadastro, WB.Worksheets["Cadastro"], 0, 0);


            BIFF8Writer.WriteWorkbookToFile(WB, filename);

            System.Diagnostics.Process.Start(filename);
        }

        #endregion

        #region eventos
        private void ultraToolbarsManager1_ToolClick_1(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            switch (e.Tool.Key)
            {
                case "Salvar":
                    {
                        if (txtCPF.Text != "" && txtnome.Text != "" && ComboEmpresa.Text != "selecione" && ComboEmpresa.Text != "")
                        {
                            int id_verifica = 0;
                            if (ID == -1) id_verifica = verifica_cadastros();

                            if (id_verifica == 0 || ID == 0)
                            {
                                if (cbTipo_cadastro.Text != "Selecione")
                                {
                                    if (cbTipo_cadastro.Text != "")
                                    {
                                        salvar();
                                        ID = -1;
                                        limpar_campos();
                                    }
                                    else
                                    {
                                        mensagem("Informar o tipo de cadastro!");
                                        cbTipo_cadastro.Focus();
                                    }   
                                }
                                else
                                {
                                    mensagem("Informe o  tipo de cadastro");
                                    cbTipo_cadastro.Focus();
                                }
                            }
                            else
                            {
                                mensagem("CPF já está cadastrado!");
                            }
                            
                        }
                        else mensagem("Informar os campos obrigatórios!");
                    }
                    break;
                case "Exportar":
                    {
                        if (dscadastro.Rows.Count > 0) Exportar();
                        else mensagem("Tabela vazia!");
                        
                    }
                    break;
                case "Deletar":
                    {
                        deletar();
                        ID = -1;
                    }
                    break;
                case "Marcar Todos":
                    {
                        marcarTodos();
                    }
                    break;
                case "Desmarcar Todos":
                    {
                        desmarcarTodos();
                    }
                    break;
                case "Gerenciar Empresa":
                    {
                        empresa janela3 = new empresa("empresa");
                        janela3.ShowDialog();
                        ComboEmpresa.Items.Clear();
                        carregarCombo("select nome from empresa order by nome", 2);
                    }
                    break;
                case "Nova Empresa"://empresa visitada
                    {
                        empresa janela3 = new empresa("visita");
                        janela3.ShowDialog();
                        ComboEmpresa.Items.Clear();
                        carregarCombo("select nome from empresa order by nome", 2);
                    }
                    break;
            }
        }
        
        private void gridHistorico_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            if (!e.Cell.Row.Cells["X"].IsActiveCell)
            {
                ID = Convert.ToInt32(e.Cell.Row.Cells["ID"].Text);
                txtnome.Text = e.Cell.Row.Cells["Nome"].Text;
                ComboEmpresa.Text = e.Cell.Row.Cells["Empresa"].Text;
                txtCPF.Text = e.Cell.Row.Cells["cpf"].Text;
                txtPlaca.Text = e.Cell.Row.Cells["Placa"].Text;
                txtPlaca_carreta.Text = e.Cell.Row.Cells["Placa Carreta"].Text;
                txtVeiculo.Text = e.Cell.Row.Cells["Veículo"].Text;
                txtCor.Text = e.Cell.Row.Cells["Cor"].Text;
                if (e.Cell.Row.Cells["Vencimento CNH"].Text == "") dtpCNH.Value = DateTime.Now;
                else dtpCNH.Value = Convert.ToDateTime(e.Cell.Row.Cells["Vencimento CNH"].Text);
                cbTipo_cadastro.Text = e.Cell.Row.Cells["Tipo de cadastro"].Text;
                cbStatus.Text = e.Cell.Row.Cells["Status"].Text;
                txtCNH.Text = e.Cell.Row.Cells["CNH"].Text;
                if (cbStatus.Text == "Bloqueado")
                {
                    lbMotivo.Visible = txtMotivo.Visible = true;
                }
                else
                {
                    lbMotivo.Visible = txtMotivo.Visible = false;
                    txtMotivo.Text = "";
                }
                txtMotivo.Text = e.Cell.Row.Cells["Motivo"].Text;
            }   
        }

        private void btnLimparCampos_Click(object sender, EventArgs e)
        {
            limpar_campos();
        }

        private void txtFiltroCpf_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Back && e.KeyCode != Keys.Space)
            {
                if (cbFiltraCPF.Text.Length == 3) { cbFiltraCPF.Text += "."; cbFiltraCPF.SelectionStart = cbFiltraCPF.Text.Length; }
                else if (cbFiltraCPF.Text.Length == 7) { cbFiltraCPF.Text += "."; cbFiltraCPF.SelectionStart = cbFiltraCPF.Text.Length; }
                else if (cbFiltraCPF.Text.Length == 11) { cbFiltraCPF.Text += "-"; cbFiltraCPF.SelectionStart = cbFiltraCPF.Text.Length; }
            }
        }

        private void txtCPF_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Back && e.KeyCode != Keys.Space)
            {
                if (txtCPF.Text.Length == 3) { txtCPF.Text += "."; txtCPF.SelectionStart = txtCPF.Text.Length; }
                else if (txtCPF.Text.Length == 7) { txtCPF.Text += "."; txtCPF.SelectionStart = txtCPF.Text.Length; }
                else if (txtCPF.Text.Length == 11) { txtCPF.Text += "-"; txtCPF.SelectionStart = txtCPF.Text.Length; }
            }
        }

        private void cbFiltraCPF_SelectedIndexChanged(object sender, EventArgs e)
        {
            SQLiteConnection con = new SQLiteConnection(strConn);
            con.Open();
            SQLiteCommand comando = null;
            if (rbCpf.Checked == true) comando = new SQLiteCommand("select * from cadastro where cpf = @busca order by nome desc", con);
            else comando = new SQLiteCommand("select * from cadastro where nome = @busca order by nome desc", con);
            
            comando.Parameters.AddWithValue("@busca", cbFiltraCPF.Text);
            var reader = comando.ExecuteReader();
            while (reader.Read())
            {
                ID = Convert.ToInt32(reader["id_cadastro"]);
                txtnome.Text = reader["nome"].ToString();
                ComboEmpresa.Text = reader["empresa"].ToString();
                txtPlaca.Text = reader["placa"].ToString();
                txtPlaca_carreta.Text = reader["placa_carreta"].ToString();
                txtVeiculo.Text = reader["veiculo"].ToString();
                txtCor.Text = reader["cor"].ToString();
                txtCPF.Text = reader["cpf"].ToString();
                if (reader["tipo_cadastro"].ToString() == "outros")
                {
                    dtpCNH.Value = DateTime.Now;
                    dtpCNH.Enabled = false;
                }
                else
                {
                    dtpCNH.Value = Convert.ToDateTime(reader["vencimento_cnh"]);
                    txtCNH.Text = reader["numeroCNH"].ToString();
                }

                cbTipo_cadastro.Text = reader["tipo_cadastro"].ToString();
                cbStatus.Text = reader["status"].ToString();
                txtMotivo.Text = reader["motivo"].ToString();
            }
        }
        private void gridcadastro_CellChange(object sender, CellEventArgs e)
        {
            if ((bool)e.Cell.Row.Cells["X"].Value)
            {
                e.Cell.Row.Cells["X"].Value = false;
                id.Remove(e.Cell.Row.Cells["ID"].Text);
            }
            else
            {
                e.Cell.Row.Cells["X"].Value = true;
                id.Add(e.Cell.Row.Cells["ID"].Text);
            }

        }
        private void rbCpf_CheckedChanged(object sender, EventArgs e)
        {
            cbFiltraCPF.Items.Clear();
            if (rbCpf.Checked == true)
            {
                carregarCombo("select cpf from cadastro order by cpf", 1);
                AutoComplete("select cpf from cadastro order by cpf", 1);
            }
            else
            {
                carregarCombo("select nome from cadastro order by nome", 1);
                AutoComplete("select nome from cadastro order by nome", 1);
            }
        }

      
        #endregion   

        private void rbCpf_MouseClick(object sender, MouseEventArgs e)
        {
            if (contCPF == 0)
            {
                if (rbCpf.Checked == true)
                {
                    carregarCombo("select cpf from cadastro order by cpf", 1);
                    AutoComplete("select cpf from cadastro order by cpf", 1);
                }
                else
                {
                    carregarCombo("select nome from cadastro order by nome", 1);
                    AutoComplete("select nome from cadastro order by nome", 1);
                }
                contCPF++;
            }
        }

        private void rbNome_MouseClick(object sender, MouseEventArgs e)
        {
            if (contNome == 0)
            {
                if (rbCpf.Checked == true)
                {
                    carregarCombo("select cpf from cadastro order by cpf", 1);
                    AutoComplete("select cpf from cadastro order by cpf", 1);
                }
                else
                {
                    carregarCombo("select nome from cadastro order by nome", 1);
                    AutoComplete("select nome from cadastro order by nome", 1);
                }
                contNome++;
            }
        }

        private void cbFiltraCPF_MouseClick(object sender, MouseEventArgs e)
        {
            if (contCPF == 0)
            {
                if (rbCpf.Checked == true)
                {
                    carregarCombo("select cpf from cadastro order by cpf", 1);
                    AutoComplete("select cpf from cadastro order by cpf", 1);
                }
                else
                {
                    carregarCombo("select nome from cadastro order by nome", 1);
                    AutoComplete("select nome from cadastro order by nome", 1);
                }
                contCPF++;
            }
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbStatus.Text == "Bloqueado")
            {
                txtMotivo.Visible = lbMotivo.Visible = true;
            }
            else
            {
                txtMotivo.Text = "";
                txtMotivo.Visible = lbMotivo.Visible = false;
            }
        }

    }
}
