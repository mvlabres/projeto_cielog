﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1
{
    class ClassAcessoColaborador
    {
        private int id;
        private int idColaborador;
        private DateTime dataHoraEntrada;
        private DateTime dataHoraSaida;
        private String usuario;
        private String veiculo;
        private String placa;

        public String Placa
        {
            get { return placa; }
            set { placa = value; }
        }

        public String Veiculo
        {
            get { return veiculo; }
            set { veiculo = value; }
        }
        

        public String Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        public DateTime DataHoraSaida
        {
            get { return dataHoraSaida; }
            set { dataHoraSaida = value; }
        }
       

        public DateTime DataHoraEntrada
        {
            get { return dataHoraEntrada; }
            set { dataHoraEntrada = value; }
        }
        

        public int IdColaborador
        {
            get { return idColaborador; }
            set { idColaborador = value; }
        }
       

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        
    }
}
