﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;

namespace WindowsFormsApplication1
{
    public partial class colaborador : Form
    {
        string usuarioGlobal;
        List<int> idDelete;
        Dictionary<String, String> verifyFildsColaborador; 
        public colaborador(string usuario)
        {
            InitializeComponent();
            idDelete = new List<int>();
            usuarioGlobal = usuario;
            refresh();
        }

        #region methods
        private void autocomplete()
        {
            Dictionary<string, Dictionary<string, int>> dictionarys = new Dictionary<string, Dictionary<string, int>>();
            AcessoColaboradorController control = new AcessoColaboradorController();
            dictionarys = control.selectAutoComplete();
            foreach(var dic in dictionarys)
            {
                if (rbMatricula.Checked == true)
                {
                    if (dic.Key == "matricula")
                    {
                        foreach(var dicList in dic.Value)
                        {
                            cbMatricula.AutoCompleteCustomSource.Add(dicList.Key.ToString());
                        }
                    }
                }
                else 
                {
                    if (dic.Key == "nome")
                    {
                        foreach (var dicList in dic.Value)
                        {
                            cbMatricula.AutoCompleteCustomSource.Add(dicList.Key.ToString());
                        }
                    }
                }
            }
        }

        public void exportar()
        {
             string caminho = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
             saveFileDialog1.Filter = "Excel Workbook (*.xls)|*.xls|All Files (*.*)|*.*";
             DialogResult dg = saveFileDialog1.ShowDialog(this);
             if (dg == System.Windows.Forms.DialogResult.Cancel) return;
             string filename = saveFileDialog1.FileName;
             Workbook WB = new Workbook();
             WB.Worksheets.Add("Acessos");
             excelColaborador.Export(gridAcessoColaborador, WB.Worksheets["Acessos"], 0, 0);
             BIFF8Writer.WriteWorkbookToFile(WB, filename);
             System.Diagnostics.Process.Start(filename);
        }

        public void clearFields()
        {
            lbIdAcesso.Text = txtCpf.Text = txtEmpresa.Text = txtMatricula.Text = txtNome.Text = txtPlaca.Text = txtVeiculo.Text = "";
            dtDataHoraEntrada.Value = dtDataHoraSaida.Value = DateTime.Now; 
        }

        public bool save()
        {
            AcessoColaboradorController control = new AcessoColaboradorController();
            ClassAcessoColaborador acessoColaborador = new ClassAcessoColaborador();
            acessoColaborador.Id = Convert.ToInt16(lbIdAcesso.Text);
            acessoColaborador.IdColaborador = Convert.ToInt16(lbIdColaborador.Text);
            acessoColaborador.DataHoraEntrada = Convert.ToDateTime(dtDataHoraEntrada.Text);
            acessoColaborador.DataHoraSaida = Convert.ToDateTime(dtDataHoraSaida.Text);
            acessoColaborador.Usuario = usuarioGlobal;
            return control.update(acessoColaborador);
        }

        public void refresh()
        {
            clearFields();
            rbMatricula.Checked = rbAbertos.Checked = true;
            lbDtaSaida.Visible = dtDataHoraSaida.Visible = false;
            initializeFilds();
            carregarGrid("open");
        }

        public void initializeFilds()
        {
            dtDataHoraSaida.Format = dtDataHoraEntrada.Format = DateTimePickerFormat.Custom;
            dtDataHoraSaida.CustomFormat = dtDataHoraEntrada.CustomFormat = "dd/MM/yyyy HH:mm";

        }

        public void buscarColaborador(string fildDataBase)
        {
            cbMatricula.DataSource = null;
            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            ColaboradroDAO colaboradorDAO = new ColaboradroDAO();
            colaboradorDAO.select();

            dictionary.Add("Selecione", 0);
            cbMatricula.DisplayMember = "Key";
            cbMatricula.ValueMember = "Value";
            while (colaboradorDAO.Reader.Read())
            {
                dictionary.Add(colaboradorDAO.Reader[fildDataBase].ToString(), Convert.ToInt16(colaboradorDAO.Reader["id"]));
            }
            cbMatricula.DataSource = dictionary.ToList();
            autocomplete();
        }

        public bool inserir()
        {
            AcessoColaboradorController control = new AcessoColaboradorController();
            ClassAcessoColaborador acessoColaborador = new ClassAcessoColaborador();
            acessoColaborador.IdColaborador = Convert.ToInt16(lbIdColaborador.Text);
            acessoColaborador.DataHoraEntrada = Convert.ToDateTime(dtDataHoraEntrada.Text);
            acessoColaborador.Usuario = usuarioGlobal;
            acessoColaborador.Veiculo = txtVeiculo.Text;
            acessoColaborador.Placa = txtPlaca.Text;
            return control.insert(acessoColaborador);
        }

        public void carregarGrid(String dateEnd)
        {
            DateTime data_compara = DateTime.Now;
            AcessoColaboradorController control = new AcessoColaboradorController();
            dsAcessoColaborador.Rows.Clear();
            control.select(dateEnd);
            foreach (DataRow grd in control.Grid.Rows)
            {

                object[] novoDados = new object[]{
                    control.Flag,
                    grd[0].ToString(),
                    grd[1].ToString(),//
                    grd[9].ToString(),//
                    grd[8].ToString(),//
                    grd[10].ToString(),//
                    grd[11].ToString(),//
                    grd[6].ToString(),//
                    grd[5].ToString(),//
                    grd[2].ToString(),//
                    grd[3].ToString(),
                    grd[4].ToString()//
                }; dsAcessoColaborador.Rows.Add(novoDados);
            }

            foreach (var row in gridAcessoColaborador.DisplayLayout.Rows)
            {
                DateTime dataGrid = Convert.ToDateTime(row.Cells["Data/hora entrada"].Text);
                TimeSpan diferenca = data_compara - dataGrid;
                int dia = diferenca.Days;
                int hora = diferenca.Hours;


                if (hora > 12 && hora < 24) row.Appearance.BackColor = Color.Yellow;
                if (dia >= 1) row.Appearance.BackColor = Color.Red;
            }
        }

        #endregion
        #region events
        private void ultraToolbarsManager1_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            switch (e.Tool.Key)
            {
                case "Cadastro":
                    {
                        cadastroColaborador janelaCadastro = new cadastroColaborador(usuarioGlobal);
                        janelaCadastro.ShowDialog();
                        refresh();
                    }
                    break;
                case "Salvar":
                    {
                        if(txtNome.Text != "" && dtDataHoraEntrada.Text != "" && dtDataHoraSaida.Text != "")
                        {
                            Message message = new Message();
                            if (save())
                            {
                                refresh();
                                message.messageSuccess(this, "Acesso finalizado com sucesso!", "Sucesso");
                            }
                            else 
                            {
                                message.messageInformation(this, "Erro ao finalizar. Tente mais tarde!", "Informação"); 
                            }
                        }
                    }
                    break;
                case "Limpar Campos":
                    {
                        clearFields();
                    }
                    break;

                case "Excluir":
                    {
                        Message message = new Message();
                        if (idDelete.Count() > 0)
                        {
                            if (message.messageConfirmation(this, "Tem certeza que deseja excluir as lihas selecionadas?", "Confirmação"))
                            {
                                AcessoColaboradorController control = new AcessoColaboradorController();
                                if (control.deleteForList(idDelete))
                                {
                                    if (rbAbertos.Checked == true)
                                    {
                                        carregarGrid("open");
                                    }
                                    else 
                                    {
                                        carregarGrid("close");
                                    }
                                    message.messageSuccess(this, "Exclusão realizada com sucesso!", "Sucesso");
                                    idDelete.Clear();
                                }
                                else
                                {
                                    message.messageInformation(this, "Erro ao exlcuir. Tente novamente.", "Erro");
                                }
                            }
                        }
                        else
                        {
                            message.messageInformation(this, "Não há linhas selecionadas.", "");
                        }
                    }
                    break;
                case "Exportar":
                    {
                        AcessoColaboradorController control = new AcessoColaboradorController();
                        exportar();
                    }
                    break;
            }
        }

        private void rbMatricula_CheckedChanged(object sender, EventArgs e)
        {
            if (rbMatricula.Checked == true)
            {
                buscarColaborador("matricula");
            }
            else
            {
                buscarColaborador("nome");
            }
            carregarGrid("open");
            autocomplete();
        }

        private void cbMatricula_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbDtaSaida.Visible = dtDataHoraSaida.Visible = false;
            ColaboradroDAO colaboradorDAO = new ColaboradroDAO();
            if (Convert.ToInt16(cbMatricula.SelectedValue) != 0)
            {
                colaboradorDAO.selectId(Convert.ToInt16(cbMatricula.SelectedValue));

                while (colaboradorDAO.Reader.Read())
                {
                    lbIdColaborador.Text = colaboradorDAO.Reader["id"].ToString();
                    txtNome.Text = colaboradorDAO.Reader["nome"].ToString();
                    txtMatricula.Text = colaboradorDAO.Reader["matricula"].ToString();
                    txtCpf.Text = colaboradorDAO.Reader["cpf"].ToString();
                    txtEmpresa.Text = colaboradorDAO.Reader["empresa"].ToString();
                    txtVeiculo.Text = colaboradorDAO.Reader["veiculo"].ToString();
                    txtPlaca.Text = colaboradorDAO.Reader["placa"].ToString();

                    verifyFildsColaborador = new Dictionary<string, string>();
                    verifyFildsColaborador.Add("veiculo", colaboradorDAO.Reader["veiculo"].ToString());
                    verifyFildsColaborador.Add("placa", colaboradorDAO.Reader["placa"].ToString());
                }
            }
        }

        private void btnInserir_Click(object sender, EventArgs e)
        {
            Message message = new Message();
            if (dtDataHoraSaida.Visible == false)
            {
                if (txtNome.Text != "" && dtDataHoraEntrada.Text != "")
                {
                    if (inserir())
                    {
                        refresh();
                        message.messageSuccess(this, "Acesso inserido com sucesso!", "Sucesso");   
                    }
                }
                else
                {
                    message.messageInformation(this, "Favor informar os campos obrigatórios!", "Informação");
                }
            }
            else 
            {
                message.messageInformation(this, "Favor limpar os campos antes de inserir novo acesso!", "Informação");
            }
            
        }
        #endregion

        private void gridAcessoColaborador_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            lbDtaSaida.Visible = dtDataHoraSaida.Visible = true;
            lbIdAcesso.Text = e.Cell.Row.Cells["ID"].Text;
            lbIdColaborador.Text = e.Cell.Row.Cells["idColaborador"].Text;
            txtNome.Text = e.Cell.Row.Cells["Nome"].Text;
            txtMatricula.Text = e.Cell.Row.Cells["Matrícula"].Text;
            txtEmpresa.Text = e.Cell.Row.Cells["Empresa"].Text;
            txtPlaca.Text = e.Cell.Row.Cells["Placa"].Text;
            txtVeiculo.Text = e.Cell.Row.Cells["Veículo"].Text;
            txtCpf.Text = e.Cell.Row.Cells["CPF"].Text;
            dtDataHoraEntrada.Text = e.Cell.Row.Cells["Data/hora entrada"].Text;
        }

        private void rbAbertos_CheckedChanged(object sender, EventArgs e)
        {
            if (rbAbertos.Checked == true)
            {
                carregarGrid("open");
            }
            else 
            {
                carregarGrid("close");
            }
        }

        private void gridAcessoColaborador_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            if ((bool)e.Cell.Row.Cells["X"].Value)
            {

                idDelete.Remove(Convert.ToInt16(e.Cell.Row.Cells["ID"].Value));
            }
            else
            {
                idDelete.Add(Convert.ToInt16(e.Cell.Row.Cells["ID"].Value));
            }
        }

       
    }
}
