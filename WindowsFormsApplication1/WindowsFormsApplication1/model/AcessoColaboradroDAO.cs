﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace WindowsFormsApplication1
{
    class AcessoColaboradroDAO
    {
        private Conn conn;
        private SQLiteDataAdapter sqlAdapter;
        private string sql;

        public string Sql
        {
            get { return sql; }
            set { sql = value; }
        }


        public SQLiteDataAdapter SqlAdapter
        {
            get { return sqlAdapter; }
            set { sqlAdapter = value; }
        }

        private SQLiteDataReader reader;
        public ClassColaborador classColaborador;

        public SQLiteDataReader Reader
        {
            get { return reader; }
            set { reader = value; }
        }

        public bool insert(ClassAcessoColaborador acessoColaborador)
        {
            sql = "insert into acesso_colaborador (id_colaborador, dataHora_entrada, usuario, veiculo, placa) values (@id_colaborador, @data_entrada, @usuario, @veiculo, @placa)";
            return this.ConnParameters(acessoColaborador, sql, "insert");
        }

        public bool update(ClassAcessoColaborador acessoColaborador)
        {
            sql = "update acesso_colaborador set id_colaborador=@id_colaborador, dataHora_entrada=@data_entrada, dataHora_saida=@data_saida, usuario=@usuario where id = @id";
            return this.ConnParameters(acessoColaborador, sql, "update");
        }

        public void select(String dateEnd)
        {
            this.conn = new Conn();
            if (dateEnd == "open")
            {
                this.Sql = "select * from acesso_colaborador inner join colaborador  on id_colaborador = colaborador.id where dataHora_saida isNull order by acesso_colaborador.id desc; ";
            }
            else {
                this.Sql = "select * from acesso_colaborador inner join colaborador  on id_colaborador = colaborador.id where acesso_colaborador.dataHora_saida IS NOT NULL order by acesso_colaborador.id desc; ";
            }
            
            this.conn.connection(sql);
            this.sqlAdapter = new SQLiteDataAdapter(this.conn.Cmd);
            this.conn.desconectar();
        }

        public bool ConnParameters(ClassAcessoColaborador acessoColaborador, string sql, string metodo)
        {
            try
            {
                this.conn = new Conn();
                conn.conection_parameter(sql);
                if (metodo == "update")
                {
                    this.conn.Cmd.Parameters.AddWithValue("@id", acessoColaborador.Id);
                    this.conn.Cmd.Parameters.AddWithValue("@data_saida", acessoColaborador.DataHoraSaida);
                }
                this.conn.Cmd.Parameters.AddWithValue("@id_colaborador", acessoColaborador.IdColaborador);
                this.conn.Cmd.Parameters.AddWithValue("@data_entrada", acessoColaborador.DataHoraEntrada);
                this.conn.Cmd.Parameters.AddWithValue("@usuario", acessoColaborador.Usuario);
                this.conn.Cmd.Parameters.AddWithValue("@veiculo", acessoColaborador.Veiculo);
                this.conn.Cmd.Parameters.AddWithValue("@placa", acessoColaborador.Placa);
                this.conn.Cmd.ExecuteNonQuery();
                this.conn.desconectar();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool delete(int id)
        {
            try
            {
                this.conn = new Conn();
                this.Sql = "delete from acesso_colaborador where id = @id";
                this.conn.conection_parameter(this.sql);
                this.conn.Cmd.Parameters.AddWithValue("@id", id);
                this.conn.Cmd.ExecuteNonQuery();
                this.conn.desconectar();
                return true;
            }
            catch (Exception e) { return false; }

        }
    }
}
