﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    class AcessoColaboradorController
    {

        AcessoColaboradroDAO acessoColaboradorDAO;
        Message message;
        private bool flag;
        private DataTable acesso;

        public DataTable Acesso
        {
            get { return acesso; }
            set { acesso = value; }
        }

        public bool Flag
        {
            get { return flag; }
            set { flag = value; }
        }
     
        private DataTable grid;

        public DataTable Grid
        {
            get { return grid; }
            set { grid = value; }
        }

        public bool insert(ClassAcessoColaborador acessoColaborador)
        {
            this.acessoColaboradorDAO = new AcessoColaboradroDAO();
            return this.acessoColaboradorDAO.insert(acessoColaborador);
        }

        public bool update(ClassAcessoColaborador acessoColaborador)
        {
            this.acessoColaboradorDAO = new AcessoColaboradroDAO();
            return this.acessoColaboradorDAO.update(acessoColaborador);
        }

        public void select(String dateEnd)
        {
            this.acessoColaboradorDAO = new AcessoColaboradroDAO();
            this.flag = false;
            this.acesso = new DataTable();
            acessoColaboradorDAO.select(dateEnd);
            acessoColaboradorDAO.SqlAdapter.Fill(acesso);
            this.grid = acesso;
            this.acessoColaboradorDAO = new AcessoColaboradroDAO();
        }

        public Dictionary<string, Dictionary<string, int>> selectAutoComplete()
        {
            this.acessoColaboradorDAO = new AcessoColaboradroDAO();
            List<ClassColaborador> colaboradores = new List<ClassColaborador>();
            ColaboradroDAO colaboradorDAO = new ColaboradroDAO();
            ClassColaborador colaborador = new ClassColaborador();
            Dictionary<string, int> dictionaryMatricula = new Dictionary<string, int>();
            Dictionary<string, int> dictionaryNome = new Dictionary<string, int>();
            Dictionary<string, Dictionary<string, int>> dictionarys = new Dictionary<string, Dictionary<string, int>>();
            
            colaboradorDAO.select();
            while (colaboradorDAO.Reader.Read())
            {
                dictionaryMatricula.Add(colaboradorDAO.Reader["matricula"].ToString(), Convert.ToInt16(colaboradorDAO.Reader["id"]));
                dictionaryNome.Add(colaboradorDAO.Reader["nome"].ToString(), Convert.ToInt16(colaboradorDAO.Reader["id"]));
            }
            dictionarys.Add("matricula", dictionaryMatricula);
            dictionarys.Add("nome", dictionaryNome);

            return dictionarys;
        }

        public bool deleteForList(List<int> idAcesso)
        {
            foreach (var id in idAcesso)
            {
                AcessoColaboradroDAO acessoDAO = new AcessoColaboradroDAO();
                acessoDAO.delete(id);
            }
            return true;
        }
    }
}
