﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace WindowsFormsApplication1
{
    class EmpresaDAO
    {
        public Conn conn;
        public ClassEmpresa classEmpresa;
        private SQLiteDataReader reader;

        public SQLiteDataReader Reader
        {
            get { return reader; }
            set { reader = value; }
        }

        public void buscar()
        {
            this.classEmpresa = new ClassEmpresa();
            this.conn = new Conn();
            string sql = "select * from empresa_visitada order by nome";
            this.conn.connection(sql);
            this.reader = this.conn.Cmd.ExecuteReader();
 
        }
    }
}
