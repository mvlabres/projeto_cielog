﻿namespace WindowsFormsApplication1
{
    partial class Historico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("X");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Id");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Turno");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CPF");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Motorista");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Empresa");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Data/hora de entrada");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Data/hora de saída");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn9 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Placa");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn10 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Placa carreta");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Veículo");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Cor");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NF entrada");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NF saída");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Usuário entrada");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn16 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("usuario saída");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn17 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Senha");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn18 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Empresa visitada");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn19 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Tipo de caminhão");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn1 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("X");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn2 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Id");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn3 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Turno");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn4 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("CPF");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn5 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Motorista");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn6 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Empresa");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn7 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Data/hora de entrada");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn8 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Data/hora de saída");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn9 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Placa");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn10 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Placa carreta");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn11 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Veículo");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn12 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Cor");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn13 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("NF entrada");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn14 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("NF saída");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn15 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Usuário entrada");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn16 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("usuario saída");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn17 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Senha");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn18 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Empresa visitada");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn19 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Tipo de caminhão");
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab1 = new Infragistics.Win.UltraWinToolbars.RibbonTab("Histórico");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup1 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("Ferramentas");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool5 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool10");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool1 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Salvar");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool3 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exportar");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool5 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Deletar");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool7 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Marcar Todos");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool15 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Desmarcar Todos");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup2 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("Filtros");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool1 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool1");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool3 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool2");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool2 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Salvar");
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool4 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exportar");
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool6 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Deletar");
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool8 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ButtonTool1");
            Infragistics.Win.UltraWinToolbars.PopupControlContainerTool popupControlContainerTool2 = new Infragistics.Win.UltraWinToolbars.PopupControlContainerTool("PopupControlContainerTool1");
            Infragistics.Win.UltraWinToolbars.PopupControlContainerTool popupControlContainerTool4 = new Infragistics.Win.UltraWinToolbars.PopupControlContainerTool("PopupControlContainerTool2");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool9 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ButtonTool2");
            Infragistics.Win.UltraWinToolbars.PopupControlContainerTool popupControlContainerTool3 = new Infragistics.Win.UltraWinToolbars.PopupControlContainerTool("ControlContainerTool3");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool10 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ButtonTool3");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool2 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool1");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool4 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool2");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool6 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool4");
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool12 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ButtonTool4");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool13 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Imprimir");
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool14 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Marcar Todos");
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool16 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Desmarcar Todos");
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool8 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool5");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool13 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool6");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool14 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool7");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool15 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool8");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool16 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool9");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool11 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("ControlContainerTool10");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Historico));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pbAtualizar = new System.Windows.Forms.PictureBox();
            this.cbEmpresas = new System.Windows.Forms.CheckBox();
            this.cbcpfs = new System.Windows.Forms.CheckBox();
            this.cbEmpresa = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbcpf = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtFim = new System.Windows.Forms.DateTimePicker();
            this.dtInicio = new System.Windows.Forms.DateTimePicker();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridHistorico = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.dshistoricos = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.Historico_Fill_Panel = new Infragistics.Win.Misc.UltraPanel();
            this.label18 = new System.Windows.Forms.Label();
            this.cbTipo_caminhao = new System.Windows.Forms.ComboBox();
            this.cbusuarioSaida = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLimparCampos = new System.Windows.Forms.Button();
            this.cbAlteraCpf = new System.Windows.Forms.ComboBox();
            this.lbCpf = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cbEmpresaVisitada = new System.Windows.Forms.ComboBox();
            this.txtPlacaCarreta = new System.Windows.Forms.TextBox();
            this.txtPlacaCavalo = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCor = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtVeiculo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.picAnularFinal = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lbSaida = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.DTPHoraSaida = new System.Windows.Forms.DateTimePicker();
            this.dtpDataEntrada = new System.Windows.Forms.DateTimePicker();
            this.txtNfSaida = new System.Windows.Forms.TextBox();
            this.txtNfEntrada = new System.Windows.Forms.TextBox();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this._Historico_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.ultraToolbarsManager1 = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
            this._Historico_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._Historico_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._Historico_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.excelHistorico = new Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter(this.components);
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAtualizar)).BeginInit();
            this.panel1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridHistorico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dshistoricos)).BeginInit();
            this.Historico_Fill_Panel.ClientArea.SuspendLayout();
            this.Historico_Fill_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAnularFinal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.pictureBox2);
            this.ultraTabPageControl1.Controls.Add(this.pictureBox1);
            this.ultraTabPageControl1.Controls.Add(this.panel2);
            this.ultraTabPageControl1.Controls.Add(this.panel1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1264, 283);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Image = global::WindowsFormsApplication1.Properties.Resources.proteg1;
            this.pictureBox2.Location = new System.Drawing.Point(623, 87);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(117, 73);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = global::WindowsFormsApplication1.Properties.Resources.novo_cielog1;
            this.pictureBox1.Location = new System.Drawing.Point(37, 313);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(91, 73);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.pbAtualizar);
            this.panel2.Controls.Add(this.cbEmpresas);
            this.panel2.Controls.Add(this.cbcpfs);
            this.panel2.Controls.Add(this.cbEmpresa);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.cbcpf);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(13, 71);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(564, 73);
            this.panel2.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(504, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Atualizar";
            // 
            // pbAtualizar
            // 
            this.pbAtualizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbAtualizar.Image = global::WindowsFormsApplication1.Properties.Resources.atualizar;
            this.pbAtualizar.Location = new System.Drawing.Point(512, 10);
            this.pbAtualizar.Name = "pbAtualizar";
            this.pbAtualizar.Size = new System.Drawing.Size(36, 33);
            this.pbAtualizar.TabIndex = 5;
            this.pbAtualizar.TabStop = false;
            this.pbAtualizar.Click += new System.EventHandler(this.pbAtualizar_Click);
            // 
            // cbEmpresas
            // 
            this.cbEmpresas.AutoSize = true;
            this.cbEmpresas.Location = new System.Drawing.Point(366, 37);
            this.cbEmpresas.Name = "cbEmpresas";
            this.cbEmpresas.Size = new System.Drawing.Size(119, 17);
            this.cbEmpresas.TabIndex = 5;
            this.cbEmpresas.Text = "Todas as Empresas";
            this.cbEmpresas.UseVisualStyleBackColor = true;
            this.cbEmpresas.CheckedChanged += new System.EventHandler(this.cbnomes_CheckedChanged);
            this.cbEmpresas.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbEmpresas_MouseClick);
            // 
            // cbcpfs
            // 
            this.cbcpfs.AutoSize = true;
            this.cbcpfs.Location = new System.Drawing.Point(366, 14);
            this.cbcpfs.Name = "cbcpfs";
            this.cbcpfs.Size = new System.Drawing.Size(100, 17);
            this.cbcpfs.TabIndex = 4;
            this.cbcpfs.Text = "Todos os CPF\'s";
            this.cbcpfs.UseVisualStyleBackColor = true;
            this.cbcpfs.CheckedChanged += new System.EventHandler(this.cbnomes_CheckedChanged);
            this.cbcpfs.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbcpfs_MouseClick);
            // 
            // cbEmpresa
            // 
            this.cbEmpresa.FormattingEnabled = true;
            this.cbEmpresa.Location = new System.Drawing.Point(65, 38);
            this.cbEmpresa.Name = "cbEmpresa";
            this.cbEmpresa.Size = new System.Drawing.Size(266, 21);
            this.cbEmpresa.TabIndex = 3;
            this.cbEmpresa.Text = "selecione";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Empresa";
            // 
            // cbcpf
            // 
            this.cbcpf.FormattingEnabled = true;
            this.cbcpf.Location = new System.Drawing.Point(65, 11);
            this.cbcpf.Name = "cbcpf";
            this.cbcpf.Size = new System.Drawing.Size(266, 21);
            this.cbcpf.TabIndex = 1;
            this.cbcpf.Text = "selecione";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "CPF";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.dtFim);
            this.panel1.Controls.Add(this.dtInicio);
            this.panel1.Location = new System.Drawing.Point(50, 203);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 73);
            this.panel1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Fim";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Início";
            // 
            // dtFim
            // 
            this.dtFim.Location = new System.Drawing.Point(66, 43);
            this.dtFim.Name = "dtFim";
            this.dtFim.Size = new System.Drawing.Size(121, 20);
            this.dtFim.TabIndex = 3;
            // 
            // dtInicio
            // 
            this.dtInicio.Location = new System.Drawing.Point(66, 13);
            this.dtInicio.Name = "dtInicio";
            this.dtInicio.Size = new System.Drawing.Size(121, 20);
            this.dtInicio.TabIndex = 2;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.gridHistorico);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1264, 283);
            // 
            // gridHistorico
            // 
            this.gridHistorico.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridHistorico.DataMember = null;
            this.gridHistorico.DataSource = this.dshistoricos;
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn2.Header.VisiblePosition = 1;
            ultraGridColumn3.Header.VisiblePosition = 3;
            ultraGridColumn4.Header.VisiblePosition = 2;
            ultraGridColumn4.Width = 117;
            ultraGridColumn5.Header.VisiblePosition = 5;
            ultraGridColumn6.Header.VisiblePosition = 4;
            ultraGridColumn6.Width = 184;
            ultraGridColumn7.Header.VisiblePosition = 7;
            ultraGridColumn8.Header.VisiblePosition = 8;
            ultraGridColumn9.Header.VisiblePosition = 9;
            ultraGridColumn10.Header.VisiblePosition = 6;
            ultraGridColumn10.Width = 142;
            ultraGridColumn11.Header.VisiblePosition = 12;
            ultraGridColumn12.Header.VisiblePosition = 14;
            ultraGridColumn13.Header.VisiblePosition = 15;
            ultraGridColumn14.Header.VisiblePosition = 16;
            ultraGridColumn15.Header.VisiblePosition = 10;
            ultraGridColumn16.Header.VisiblePosition = 11;
            ultraGridColumn17.Header.VisiblePosition = 17;
            ultraGridColumn18.Header.VisiblePosition = 18;
            ultraGridColumn19.Header.VisiblePosition = 13;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5,
            ultraGridColumn6,
            ultraGridColumn7,
            ultraGridColumn8,
            ultraGridColumn9,
            ultraGridColumn10,
            ultraGridColumn11,
            ultraGridColumn12,
            ultraGridColumn13,
            ultraGridColumn14,
            ultraGridColumn15,
            ultraGridColumn16,
            ultraGridColumn17,
            ultraGridColumn18,
            ultraGridColumn19});
            this.gridHistorico.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.gridHistorico.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridHistorico.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.gridHistorico.ExitEditModeOnLeave = false;
            this.gridHistorico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridHistorico.Location = new System.Drawing.Point(-4, 0);
            this.gridHistorico.Name = "gridHistorico";
            this.gridHistorico.Size = new System.Drawing.Size(1271, 280);
            this.gridHistorico.TabIndex = 0;
            this.gridHistorico.Text = "Histórico";
            this.gridHistorico.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.gridHistorico_CellChange);
            this.gridHistorico.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.gridHistorico_ClickCell);
            // 
            // dshistoricos
            // 
            ultraDataColumn1.DataType = typeof(bool);
            this.dshistoricos.Band.Columns.AddRange(new object[] {
            ultraDataColumn1,
            ultraDataColumn2,
            ultraDataColumn3,
            ultraDataColumn4,
            ultraDataColumn5,
            ultraDataColumn6,
            ultraDataColumn7,
            ultraDataColumn8,
            ultraDataColumn9,
            ultraDataColumn10,
            ultraDataColumn11,
            ultraDataColumn12,
            ultraDataColumn13,
            ultraDataColumn14,
            ultraDataColumn15,
            ultraDataColumn16,
            ultraDataColumn17,
            ultraDataColumn18,
            ultraDataColumn19});
            // 
            // Historico_Fill_Panel
            // 
            // 
            // Historico_Fill_Panel.ClientArea
            // 
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.label18);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.cbTipo_caminhao);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.cbusuarioSaida);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.label16);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.txtLimparCampos);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.cbAlteraCpf);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.lbCpf);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.label17);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.cbEmpresaVisitada);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.txtPlacaCarreta);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.txtPlacaCavalo);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.label14);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.label15);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.label12);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.txtCor);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.label6);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.txtVeiculo);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.label10);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.label13);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.txtNome);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.picAnularFinal);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.label11);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.lbSaida);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.label9);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.label8);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.label7);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.txtSenha);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.DTPHoraSaida);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.dtpDataEntrada);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.txtNfSaida);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.txtNfEntrada);
            this.Historico_Fill_Panel.ClientArea.Controls.Add(this.ultraTabControl1);
            this.Historico_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default;
            this.Historico_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Historico_Fill_Panel.Location = new System.Drawing.Point(8, 158);
            this.Historico_Fill_Panel.Name = "Historico_Fill_Panel";
            this.Historico_Fill_Panel.Size = new System.Drawing.Size(1280, 495);
            this.Historico_Fill_Panel.TabIndex = 0;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(326, 39);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 13);
            this.label18.TabIndex = 52;
            this.label18.Text = "Tipo de caminhão";
            // 
            // cbTipo_caminhao
            // 
            this.cbTipo_caminhao.FormattingEnabled = true;
            this.cbTipo_caminhao.Location = new System.Drawing.Point(428, 32);
            this.cbTipo_caminhao.Name = "cbTipo_caminhao";
            this.cbTipo_caminhao.Size = new System.Drawing.Size(165, 21);
            this.cbTipo_caminhao.TabIndex = 51;
            // 
            // cbusuarioSaida
            // 
            this.cbusuarioSaida.FormattingEnabled = true;
            this.cbusuarioSaida.Location = new System.Drawing.Point(1071, 36);
            this.cbusuarioSaida.Name = "cbusuarioSaida";
            this.cbusuarioSaida.Size = new System.Drawing.Size(176, 21);
            this.cbusuarioSaida.TabIndex = 50;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(968, 42);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 13);
            this.label16.TabIndex = 49;
            this.label16.Text = "Usuário saída";
            // 
            // txtLimparCampos
            // 
            this.txtLimparCampos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLimparCampos.Location = new System.Drawing.Point(1110, 7);
            this.txtLimparCampos.Name = "txtLimparCampos";
            this.txtLimparCampos.Size = new System.Drawing.Size(161, 23);
            this.txtLimparCampos.TabIndex = 48;
            this.txtLimparCampos.Text = "Limpar Campos";
            this.txtLimparCampos.UseVisualStyleBackColor = true;
            this.txtLimparCampos.Click += new System.EventHandler(this.txtLimparCampos_Click);
            // 
            // cbAlteraCpf
            // 
            this.cbAlteraCpf.FormattingEnabled = true;
            this.cbAlteraCpf.Location = new System.Drawing.Point(86, 61);
            this.cbAlteraCpf.Name = "cbAlteraCpf";
            this.cbAlteraCpf.Size = new System.Drawing.Size(198, 21);
            this.cbAlteraCpf.TabIndex = 47;
            // 
            // lbCpf
            // 
            this.lbCpf.AutoSize = true;
            this.lbCpf.Location = new System.Drawing.Point(15, 12);
            this.lbCpf.Name = "lbCpf";
            this.lbCpf.Size = new System.Drawing.Size(0, 13);
            this.lbCpf.TabIndex = 46;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(616, 64);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 13);
            this.label17.TabIndex = 45;
            this.label17.Text = "Empresa Visitada";
            // 
            // cbEmpresaVisitada
            // 
            this.cbEmpresaVisitada.FormattingEnabled = true;
            this.cbEmpresaVisitada.Location = new System.Drawing.Point(719, 59);
            this.cbEmpresaVisitada.Name = "cbEmpresaVisitada";
            this.cbEmpresaVisitada.Size = new System.Drawing.Size(176, 21);
            this.cbEmpresaVisitada.TabIndex = 43;
            // 
            // txtPlacaCarreta
            // 
            this.txtPlacaCarreta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlacaCarreta.Location = new System.Drawing.Point(719, 32);
            this.txtPlacaCarreta.MaxLength = 20;
            this.txtPlacaCarreta.Multiline = true;
            this.txtPlacaCarreta.Name = "txtPlacaCarreta";
            this.txtPlacaCarreta.Size = new System.Drawing.Size(165, 23);
            this.txtPlacaCarreta.TabIndex = 40;
            // 
            // txtPlacaCavalo
            // 
            this.txtPlacaCavalo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlacaCavalo.Location = new System.Drawing.Point(428, 114);
            this.txtPlacaCavalo.MaxLength = 20;
            this.txtPlacaCavalo.Multiline = true;
            this.txtPlacaCavalo.Name = "txtPlacaCavalo";
            this.txtPlacaCavalo.Size = new System.Drawing.Size(165, 23);
            this.txtPlacaCavalo.TabIndex = 39;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(617, 39);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 16);
            this.label14.TabIndex = 42;
            this.label14.Text = "Placa carreta";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(326, 117);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 16);
            this.label15.TabIndex = 41;
            this.label15.Text = "Placa";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 122);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 16);
            this.label12.TabIndex = 38;
            this.label12.Text = "Cor";
            // 
            // txtCor
            // 
            this.txtCor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCor.Location = new System.Drawing.Point(86, 116);
            this.txtCor.MaxLength = 50;
            this.txtCor.Multiline = true;
            this.txtCor.Name = "txtCor";
            this.txtCor.Size = new System.Drawing.Size(198, 22);
            this.txtCor.TabIndex = 34;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 16);
            this.label6.TabIndex = 37;
            this.label6.Text = "Veículo";
            // 
            // txtVeiculo
            // 
            this.txtVeiculo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVeiculo.Location = new System.Drawing.Point(86, 88);
            this.txtVeiculo.MaxLength = 50;
            this.txtVeiculo.Multiline = true;
            this.txtVeiculo.Name = "txtVeiculo";
            this.txtVeiculo.Size = new System.Drawing.Size(198, 22);
            this.txtVeiculo.TabIndex = 32;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(15, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 16);
            this.label10.TabIndex = 36;
            this.label10.Text = "Empresa";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(15, 37);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 16);
            this.label13.TabIndex = 33;
            this.label13.Text = "Nome";
            // 
            // txtNome
            // 
            this.txtNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.Location = new System.Drawing.Point(86, 34);
            this.txtNome.Multiline = true;
            this.txtNome.Name = "txtNome";
            this.txtNome.ReadOnly = true;
            this.txtNome.Size = new System.Drawing.Size(223, 21);
            this.txtNome.TabIndex = 31;
            // 
            // picAnularFinal
            // 
            this.picAnularFinal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picAnularFinal.Image = global::WindowsFormsApplication1.Properties.Resources.block_16;
            this.picAnularFinal.Location = new System.Drawing.Point(907, 120);
            this.picAnularFinal.Name = "picAnularFinal";
            this.picAnularFinal.Size = new System.Drawing.Size(23, 17);
            this.picAnularFinal.TabIndex = 29;
            this.picAnularFinal.TabStop = false;
            this.picAnularFinal.Click += new System.EventHandler(this.picAnularFinal_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1027, 63);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Senha";
            // 
            // lbSaida
            // 
            this.lbSaida.AutoSize = true;
            this.lbSaida.Location = new System.Drawing.Point(616, 120);
            this.lbSaida.Name = "lbSaida";
            this.lbSaida.Size = new System.Drawing.Size(88, 13);
            this.lbSaida.TabIndex = 15;
            this.lbSaida.Text = "Data/Hora saída";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(616, 89);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Data/Hora entrada";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(326, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "NF Saída";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(326, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "NF Entrada";
            // 
            // txtSenha
            // 
            this.txtSenha.Location = new System.Drawing.Point(1071, 60);
            this.txtSenha.Multiline = true;
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(68, 22);
            this.txtSenha.TabIndex = 11;
            // 
            // DTPHoraSaida
            // 
            this.DTPHoraSaida.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPHoraSaida.Location = new System.Drawing.Point(719, 115);
            this.DTPHoraSaida.Name = "DTPHoraSaida";
            this.DTPHoraSaida.Size = new System.Drawing.Size(176, 22);
            this.DTPHoraSaida.TabIndex = 10;
            // 
            // dtpDataEntrada
            // 
            this.dtpDataEntrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDataEntrada.Location = new System.Drawing.Point(719, 86);
            this.dtpDataEntrada.Name = "dtpDataEntrada";
            this.dtpDataEntrada.Size = new System.Drawing.Size(176, 22);
            this.dtpDataEntrada.TabIndex = 9;
            // 
            // txtNfSaida
            // 
            this.txtNfSaida.Location = new System.Drawing.Point(428, 88);
            this.txtNfSaida.Multiline = true;
            this.txtNfSaida.Name = "txtNfSaida";
            this.txtNfSaida.Size = new System.Drawing.Size(165, 22);
            this.txtNfSaida.TabIndex = 5;
            // 
            // txtNfEntrada
            // 
            this.txtNfEntrada.Location = new System.Drawing.Point(428, 61);
            this.txtNfEntrada.Multiline = true;
            this.txtNfEntrada.Name = "txtNfEntrada";
            this.txtNfEntrada.Size = new System.Drawing.Size(165, 22);
            this.txtNfEntrada.TabIndex = 4;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Location = new System.Drawing.Point(6, 164);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(1268, 309);
            this.ultraTabControl1.TabIndex = 0;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "tab1";
            ultraTab1.Visible = false;
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Histórico";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1264, 283);
            // 
            // _Historico_Toolbars_Dock_Area_Left
            // 
            this._Historico_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Historico_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Historico_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
            this._Historico_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Historico_Toolbars_Dock_Area_Left.InitialResizeAreaExtent = 8;
            this._Historico_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 158);
            this._Historico_Toolbars_Dock_Area_Left.Name = "_Historico_Toolbars_Dock_Area_Left";
            this._Historico_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(8, 495);
            this._Historico_Toolbars_Dock_Area_Left.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // ultraToolbarsManager1
            // 
            appearance14.BackColor = System.Drawing.Color.White;
            appearance14.BackColor2 = System.Drawing.Color.White;
            this.ultraToolbarsManager1.Appearance = appearance14;
            this.ultraToolbarsManager1.DesignerFlags = 1;
            this.ultraToolbarsManager1.DockWithinContainer = this;
            this.ultraToolbarsManager1.DockWithinContainerBaseType = typeof(System.Windows.Forms.Form);
            this.ultraToolbarsManager1.ImageTransparentColor = System.Drawing.Color.White;
            ribbonTab1.Caption = "Histórico";
            ribbonGroup1.Caption = "Ferramentas";
            controlContainerTool5.ControlName = "pictureBox2";
            controlContainerTool5.InstanceProps.Width = 117;
            buttonTool1.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool3.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool5.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            controlContainerTool5,
            buttonTool1,
            buttonTool3,
            buttonTool5,
            buttonTool7,
            buttonTool15});
            ribbonGroup2.Caption = "Filtros";
            controlContainerTool1.ControlName = "panel1";
            controlContainerTool1.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            controlContainerTool3.ControlName = "panel2";
            controlContainerTool3.InstanceProps.Width = 637;
            ribbonGroup2.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            controlContainerTool1,
            controlContainerTool3});
            ribbonTab1.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup1,
            ribbonGroup2});
            this.ultraToolbarsManager1.Ribbon.NonInheritedRibbonTabs.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonTab[] {
            ribbonTab1});
            this.ultraToolbarsManager1.Ribbon.Visible = true;
            appearance11.Image = global::WindowsFormsApplication1.Properties.Resources.save_32;
            buttonTool2.SharedPropsInternal.AppearancesSmall.Appearance = appearance11;
            buttonTool2.SharedPropsInternal.Caption = "Salvar";
            appearance12.Image = global::WindowsFormsApplication1.Properties.Resources.Table_Excel;
            buttonTool4.SharedPropsInternal.AppearancesSmall.Appearance = appearance12;
            buttonTool4.SharedPropsInternal.Caption = "Exportar";
            appearance13.Image = global::WindowsFormsApplication1.Properties.Resources.delete_32;
            buttonTool6.SharedPropsInternal.AppearancesSmall.Appearance = appearance13;
            buttonTool6.SharedPropsInternal.Caption = "Deletar";
            buttonTool8.SharedPropsInternal.Caption = "ButtonTool1";
            popupControlContainerTool4.SharedPropsInternal.Caption = "PopupControlContainerTool2";
            popupControlContainerTool4.SharedPropsInternal.Category = "panel";
            buttonTool9.SharedPropsInternal.Category = "panel";
            popupControlContainerTool3.SharedPropsInternal.Caption = "ControlContainerTool3";
            controlContainerTool2.ControlName = "panel1";
            controlContainerTool2.SharedPropsInternal.Caption = "Data";
            controlContainerTool2.SharedPropsInternal.Category = "panel";
            controlContainerTool4.ControlName = "panel2";
            controlContainerTool4.SharedPropsInternal.Caption = "Funcionário";
            controlContainerTool4.SharedPropsInternal.Width = 637;
            controlContainerTool6.ControlName = "pictureBox1";
            appearance18.Image = global::WindowsFormsApplication1.Properties.Resources.proteg1;
            controlContainerTool6.SharedPropsInternal.AppearancesLarge.Appearance = appearance18;
            controlContainerTool6.SharedPropsInternal.Width = 127;
            buttonTool12.SharedPropsInternal.Caption = "ButtonTool4";
            appearance15.Image = global::WindowsFormsApplication1.Properties.Resources.Printer1;
            buttonTool13.SharedPropsInternal.AppearancesSmall.Appearance = appearance15;
            buttonTool13.SharedPropsInternal.Caption = "Imprimir";
            appearance16.Image = global::WindowsFormsApplication1.Properties.Resources.tick_322;
            buttonTool14.SharedPropsInternal.AppearancesSmall.Appearance = appearance16;
            buttonTool14.SharedPropsInternal.Caption = "Marcar Todos";
            appearance17.Image = global::WindowsFormsApplication1.Properties.Resources.trash_32;
            buttonTool16.SharedPropsInternal.AppearancesSmall.Appearance = appearance17;
            buttonTool16.SharedPropsInternal.Caption = "Desmarcar Todos";
            controlContainerTool8.SharedPropsInternal.Caption = "ControlContainerTool5";
            controlContainerTool8.SharedPropsInternal.Width = 115;
            controlContainerTool13.ControlName = "label9";
            controlContainerTool13.SharedPropsInternal.Caption = "ControlContainerTool6";
            controlContainerTool14.SharedPropsInternal.Caption = "ControlContainerTool7";
            controlContainerTool15.SharedPropsInternal.Caption = "ControlContainerTool8";
            controlContainerTool16.SharedPropsInternal.Caption = "ControlContainerTool9";
            controlContainerTool11.ControlName = "pictureBox2";
            this.ultraToolbarsManager1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool2,
            buttonTool4,
            buttonTool6,
            buttonTool8,
            popupControlContainerTool2,
            popupControlContainerTool4,
            buttonTool9,
            popupControlContainerTool3,
            buttonTool10,
            controlContainerTool2,
            controlContainerTool4,
            controlContainerTool6,
            buttonTool12,
            buttonTool13,
            buttonTool14,
            buttonTool16,
            controlContainerTool8,
            controlContainerTool13,
            controlContainerTool14,
            controlContainerTool15,
            controlContainerTool16,
            controlContainerTool11});
            this.ultraToolbarsManager1.ToolClick += new Infragistics.Win.UltraWinToolbars.ToolClickEventHandler(this.ultraToolbarsManager1_ToolClick);
            // 
            // _Historico_Toolbars_Dock_Area_Right
            // 
            this._Historico_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Historico_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Historico_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
            this._Historico_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Historico_Toolbars_Dock_Area_Right.InitialResizeAreaExtent = 8;
            this._Historico_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(1288, 158);
            this._Historico_Toolbars_Dock_Area_Right.Name = "_Historico_Toolbars_Dock_Area_Right";
            this._Historico_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(8, 495);
            this._Historico_Toolbars_Dock_Area_Right.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _Historico_Toolbars_Dock_Area_Top
            // 
            this._Historico_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Historico_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Historico_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
            this._Historico_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Historico_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._Historico_Toolbars_Dock_Area_Top.Name = "_Historico_Toolbars_Dock_Area_Top";
            this._Historico_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(1296, 158);
            this._Historico_Toolbars_Dock_Area_Top.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _Historico_Toolbars_Dock_Area_Bottom
            // 
            this._Historico_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Historico_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Historico_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
            this._Historico_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Historico_Toolbars_Dock_Area_Bottom.InitialResizeAreaExtent = 8;
            this._Historico_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 653);
            this._Historico_Toolbars_Dock_Area_Bottom.Name = "_Historico_Toolbars_Dock_Area_Bottom";
            this._Historico_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(1296, 8);
            this._Historico_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // Historico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1296, 661);
            this.Controls.Add(this.Historico_Fill_Panel);
            this.Controls.Add(this._Historico_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._Historico_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._Historico_Toolbars_Dock_Area_Top);
            this.Controls.Add(this._Historico_Toolbars_Dock_Area_Bottom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Historico";
            this.Text = "Histórico";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAtualizar)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridHistorico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dshistoricos)).EndInit();
            this.Historico_Fill_Panel.ClientArea.ResumeLayout(false);
            this.Historico_Fill_Panel.ClientArea.PerformLayout();
            this.Historico_Fill_Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picAnularFinal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinToolbars.UltraToolbarsManager ultraToolbarsManager1;
        private Infragistics.Win.Misc.UltraPanel Historico_Fill_Panel;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Historico_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Historico_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Historico_Toolbars_Dock_Area_Top;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Historico_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtFim;
        private System.Windows.Forms.DateTimePicker dtInicio;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cbEmpresa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbcpf;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox cbEmpresas;
        private System.Windows.Forms.CheckBox cbcpfs;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid gridHistorico;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource dshistoricos;
        private System.Windows.Forms.PictureBox pbAtualizar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNfSaida;
        private System.Windows.Forms.TextBox txtNfEntrada;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbSaida;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.DateTimePicker DTPHoraSaida;
        private System.Windows.Forms.DateTimePicker dtpDataEntrada;
        private System.Windows.Forms.PictureBox picAnularFinal;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtVeiculo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtPlacaCarreta;
        private System.Windows.Forms.TextBox txtPlacaCavalo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cbEmpresaVisitada;
        private System.Windows.Forms.Label lbCpf;
        private System.Windows.Forms.ComboBox cbAlteraCpf;
        private System.Windows.Forms.Button txtLimparCampos;
        private System.Windows.Forms.ComboBox cbusuarioSaida;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbTipo_caminhao;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter excelHistorico;
    }
}