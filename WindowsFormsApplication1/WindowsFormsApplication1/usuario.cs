﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using System.Data.SQLite;

namespace WindowsFormsApplication1
{
    public partial class Usuario : Form
    {
        public Usuario()
        {
            InitializeComponent();
            carrega_grid();
        }
        string id = "";
        String strConn = @"Data Source=C:\cielog\cielog.s3db";//string  para conexão com o bano de dados, declaração global

        private void carrega_grid()
        {

            dsUsuario.Rows.Clear();
            DataTable usuario = new DataTable();
            SQLiteConnection con = new SQLiteConnection(strConn);
            con.Open();
            SQLiteCommand cmd = new SQLiteCommand(con);
            cmd.CommandText = "select * from usuario";
            SQLiteDataAdapter envia = new SQLiteDataAdapter(cmd);
            envia.Fill(usuario);
            DataTable grid = usuario;
            foreach (DataRow grd in grid.Rows)
            {
                object[] novoDados = new object[]{
                    grd[0].ToString(),//id
                    grd[1].ToString(),//nome
                }; dsUsuario.Rows.Add(novoDados);
            }
        } 
        private void pbSalvar_usuario_Click(object sender, EventArgs e)
        {
            if (txtusuario.Text != "")
            {
                int resultado = -1;
                try
                {
                    using (SQLiteConnection conn = new SQLiteConnection(strConn))
                    {
                        conn.Open();
                        using (SQLiteCommand cmd = new SQLiteCommand(conn))
                        {
                            cmd.CommandText = "INSERT INTO usuario(nome) VALUES (@nome)";
                            cmd.Prepare();
                            cmd.Parameters.AddWithValue("@nome", txtusuario.Text);
                            try
                            {
                                resultado = cmd.ExecuteNonQuery();
                            }
                            catch (SQLiteException ex)
                            {
                                throw ex;
                            }
                            finally
                            {
                                conn.Close();
                            }
                            carrega_grid();
                            DialogResult dr = System.Windows.Forms.DialogResult.Yes;
                            dr = MessageBox.Show(
                                      this,
                                      "Usuário salvo com sucesso",
                                      "Informação",
                                      MessageBoxButtons.OK,
                                      MessageBoxIcon.Information);
                        }
                    }
                    txtusuario.Clear();
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
            else { MessageBox.Show("Favor informar um nome!"); txtusuario.Focus(); }
        }
        private void gridUsuario_MouseEnterElement(object sender, Infragistics.Win.UIElementEventArgs e)
        {
            UltraGridCell cell = e.Element.GetContext(typeof(UltraGridCell)) as UltraGridCell;

            if (cell != null)
            {
                cell.Row.Activate();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            DialogResult dr = System.Windows.Forms.DialogResult.Yes;
            dr = MessageBox.Show(
                      this,
                      "Tem certeza que deseja excluir o usuário ID: "+id+"?",
                      "Confirmação",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Question);
            switch (dr)
            {
                case DialogResult.Yes:
                    {
                        int resultado = -1;
                        using (SQLiteConnection conn = new SQLiteConnection(strConn))
                        {
                            conn.Open();
                            using (SQLiteCommand cmd = new SQLiteCommand(conn))
                            {
                                cmd.CommandText = "delete from usuario where id_usuario = @id";
                                cmd.Prepare();
                                cmd.Parameters.AddWithValue("@id", id);
                                resultado = cmd.ExecuteNonQuery();
                                conn.Close();
                                txtusuario.Text = "";
                                carrega_grid();
                            }
                        }
                    }
                    break;
                case DialogResult.No: break;
            }
        }

        private void gridUsuario_ClickCell(object sender, ClickCellEventArgs e)
        {
            id = e.Cell.Row.Cells["Id"].Text;
            txtusuario.Text = e.Cell.Row.Cells["Nome"].Text;
        }
    }
}
